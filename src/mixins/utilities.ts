import * as moment from "moment";

export function dateParser(dateFormat: Date) {
    let dayLabel = moment(dateFormat, "d").format("ddd");
    let dateLabel = (dateFormat.getDate().toString().length == 1) ? "0" + dateFormat.getDate() : dateFormat.getDate();
    let monthLabel = moment(dateFormat.getMonth() + 1, "M").format("MMM");
    let yearLabel = dateFormat.getFullYear().toString().slice(-2);

    return `${dateLabel} ${monthLabel} ${yearLabel} (${dayLabel})`;
}

export function toDateObject(dateFormat: Date, isCompleteFormat: boolean = false) {
    let dayLabel = moment(dateFormat, "d").format("ddd");
    let dateLabel = (dateFormat.getDate().toString().length == 1) ? "0" + dateFormat.getDate() : dateFormat.getDate();
    let monthLabel = moment(dateFormat.getMonth() + 1, "M").format((isCompleteFormat ? "MMMM" : "MMM"));
    let yearLabel = dateFormat.getFullYear().toString();

    return {
        day: dayLabel,
        date: dateLabel,
        month: monthLabel,
        year: yearLabel
    }
}
