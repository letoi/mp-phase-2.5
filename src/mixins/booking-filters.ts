import { CIBAirportData, ORBAirportData } from "../models/airport";
declare var _: any;

export const LIMIT: number = 500;

export function filterCIBOrigin(airports: Array<CIBAirportData>) {
    return _.chain(airports)
        .filter((airport: CIBAirportData) => {
            return airport.isCIBOrigin == "true";
        })
        .map((airport: CIBAirportData) => {
            airport = parseAirportData(airport);
            return airport;
        })
        .sortBy(["value"])
        .value()
        .splice(0, LIMIT);
}

export function filterORBOrigin(airports: Array<ORBAirportData>) {
    return _.chain(airports)
        .filter((airport: ORBAirportData) => {
            return airport.isORBOrigin == "true";
        })
        .map((airport: ORBAirportData) => {
            airport = parseAirportData(airport);
            return airport;
        })
        .sortBy(["value"])
        .value()
        .splice(0, LIMIT);
}

export function filterCIBDestination(airports: Array<CIBAirportData>) {
    return _.chain(airports)
        .filter((airport: CIBAirportData) => {
            return airport.isCIBDestination == "true";
        })
        .map((airport: CIBAirportData) => {
            airport = parseAirportData(airport);
            return airport;
        })
        .sortBy(["value"])
        .value()
        .splice(0, LIMIT);
}

export function filterORBDestination(airports: Array<ORBAirportData>) {
    return _.chain(airports)
        .filter((airport: ORBAirportData) => {
            return airport.isORBDestination == "true";
        })
        .map((airport: ORBAirportData) => {
            airport = parseAirportData(airport);
            return airport;
        })
        .sortBy(["value"])
        .value()
        .splice(0, LIMIT);
}

export function parseAirportData(airport: any) {
    airport.value = `${airport.cityName} - ${airport.airportCode}`;
    airport.label = `${airport.cityName}, ${airport.countryName} (${airport.airportName} - ${airport.airportCode})`;
    return airport;
}

export function parseFlightStatusAirportListData(airport: any) {
	airport.value = `${airport.cityName} - ${airport.airportCode}`;
    airport.label = `${airport.cityName}, ${airport.countryName} (${airport.airportName} - ${airport.airportCode})`;
    return airport;
}
