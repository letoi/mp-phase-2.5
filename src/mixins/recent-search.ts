import { Component, Vue } from 'vue-property-decorator';
import { FlightData } from '../models/airport';
import * as moment from 'moment';
import { Constants } from '../models/constants';
import { toDateObject } from './utilities';
import storageAvailable from 'storage-available';

declare var _: any;

@Component
export default class RecentSearch extends Vue {
    localStorageKey: string = 'turbo';
    cookiePrefix = 'CIB_SEARCH_HISTORY';
    expiryDays: number = 31;
    fallBackToCookie: boolean = false;

    mounted() {
        this.fallBackToCookie = !storageAvailable('localStorage');
    }

    saveToLocalStorage(type: string, data: any) {
        let dataStore = this.parseToStorageData(data);
        if (this.fallBackToCookie) {
            let cookie = this.parseToCookie(dataStore);
            this.saveToCookie(cookie);
            return;
        }
        
        let localStorageData: any = JSON.parse(localStorage.getItem(this.localStorageKey)!) || {};
        localStorageData[type] = (localStorageData[type] || {});
        localStorageData[type].search_history = (localStorageData[type].search_history || []);
        dataStore = this.checkDuplicates(localStorageData[type].search_history, dataStore);
        localStorageData[type].search_history.push(dataStore);
        localStorage.setItem(this.localStorageKey, JSON.stringify(localStorageData));
    }

    getFromLocalStorage(type: string) {
        let localStorageData: any = JSON.parse(localStorage.getItem(this.localStorageKey)!) || {};
        localStorageData[type] = (localStorageData[type] || {});
        localStorageData[type].search_history = (localStorageData[type].search_history || []);
        this.removeExpired(type, localStorageData);
        this.checkDisplayableSearch(type, localStorageData);

        if (this.fallBackToCookie) {
            return {
                search_history: this.getAllCookies()
            }
        }

        return localStorageData[type];
    }

    
    parseToFlightData(tripCountries: any, data: any) {
        let flightData: FlightData = new FlightData();
        flightData.from = _.find(tripCountries.origin, {airportCode: data.origin}) || {};
        flightData.to = _.find(tripCountries.destination, {airportCode: data.destination}) || {};
        flightData.schedule = {
            start: moment(data.departure).toDate(),
            end: data.arrival ? moment(data.arrival).toDate() : undefined,
            oneWay: data.tripType == 'oneWay' ? true : false
        }
        flightData.cabinDescription = _.find(Constants.cabinClass, {cabinId: data.class});
        flightData.passengerCount.adult = data.adults;
        flightData.passengerCount.children = data.children;
        flightData.passengerCount.infant = data.infants;
        return flightData;
    }

    removeRecentSearch(type: string, createdAt: number, idx: number) {
        if (this.fallBackToCookie) {
            this.removeCookie(idx);
        }
        let localStorageData: any = JSON.parse(localStorage.getItem(this.localStorageKey)!) || {};
        localStorageData[type] = (localStorageData[type] || {});
        localStorageData[type].search_history = (localStorageData[type].search_history || []);
        localStorageData[type].search_history = _.filter(localStorageData[type].search_history, (history: any) => {
            return history.created_at != createdAt;
        });

        localStorage.setItem(this.localStorageKey, JSON.stringify(localStorageData));
    }
  
    private checkDuplicates(searchHistory: any, data: any) {
        let result, idx;
        let query: any = {
            origin: data.origin,
            destination: data.destination,
            departure: data.departure
        };

        if (data.tripType == 'return') {
            query.arrival = data.arrival;
        }
        
        result = _.find(searchHistory, query);
        if (data.tripType == 'oneWay' && result.arrival) result = null;
        if (result) {
            idx = _.findIndex(searchHistory, {created_at: result.created_at});
            data.updated_at = +moment();
            searchHistory.splice(idx, 1);
        }

        return data;
    }

    private parseToStorageData(data: any) {
        let dataStore: any = {};
        dataStore.origin  = data.from.airportCode;
        dataStore.destination = data.to.airportCode;
        dataStore.tripType = data.schedule.oneWay ? 'oneWay' : 'return';
        dataStore.departure = +moment(data.schedule.start);
        dataStore.arrival = (!data.schedule.oneWay) ? +moment(data.schedule.end) : undefined;
        dataStore.adults = data.passengerCount.adult;
        dataStore.children = data.passengerCount.children;
        dataStore.infants = data.passengerCount.infant;
        dataStore.class = data.cabinDescription.cabinId;
        dataStore.created_at = +moment();
        dataStore.updated_at = +moment();
        return dataStore;
    }

    private checkDisplayableSearch(type: string, localStorageData: any) {
        localStorageData[type].search_history = _.filter(localStorageData[type].search_history, (history: any) => {
            let startDate = moment(history.departure);
            let currentDate = toDateObject(moment().toDate());
            return moment(`${currentDate.year}-${currentDate.month}-${currentDate.date}`).isSameOrBefore(startDate);
        });
    }

    private removeExpired(type: string, localStorageData: any) {
        localStorageData[type].search_history = _.filter(localStorageData[type].search_history, (history: any) => {
            let currentDate = toDateObject(moment().toDate());
            let expiryDate = moment(history.created_at).add(this.expiryDays, 'days');
            return moment(`${currentDate.year}-${currentDate.month}-${currentDate.date}`).isBefore(expiryDate);
        });

        localStorage.setItem(this.localStorageKey, JSON.stringify(localStorageData));
    }

    private parseToCookie(data: any) {
        let cookieData = [];
        let departReturn = [];

        departReturn.push(moment(data.departure).format("DD/MM/YYYY"));
        if (data.arrival) departReturn.push(moment(data.arrival).format("DD/MM/YYYY"));

        cookieData.push(data.origin);
        cookieData.push(data.destination);
        cookieData.push(data.tripType == 'oneWay' ? 'O' : 'R');
        cookieData.push(departReturn.join("-"));
        cookieData.push(data.class);
        cookieData.push(data.adults);
        cookieData.push(data.children);
        cookieData.push(data.infants);
        cookieData.push(moment().format("DD/MM/YYYY hh:mm:ss"));

        return cookieData.join("|");
    }

    private parseCookieToFlightData(cookieData: any) {
        let dataStore: any = {};
        let cookie = cookieData.split("|");
        let departReturn = [];

        departReturn = cookie[3].split("-");
        dataStore.departure = +moment(departReturn[0], 'DD/MM/YYYY');
        if (cookie[2] == 'R') dataStore.arrival = +moment(departReturn[1], 'DD/MM/YYYY');

        dataStore.origin = cookie[0];
        dataStore.destination = cookie[1];
        dataStore.tripType = cookie[2] == 'O' ? 'oneWay' : 'return';
        dataStore.class = cookie[4];
        dataStore.adults = parseInt(cookie[5]);
        dataStore.children = parseInt(cookie[6]);
        dataStore.infants = parseInt(cookie[7]);
        dataStore.created_at = moment(cookie[8], 'DD/MM/YYYY hh:mm:ss').toDate();

        return dataStore;
    }

    private getAllCookies() {
        let cookies = document.cookie.split(";");

        if (!cookies[0]) {
            return [];
        }

        cookies = _.map(cookies, (cookie: string, cookieIdx: number) => {
            cookie = cookie.trim();
            let data = cookie.replace(`${this.cookiePrefix}_${cookieIdx + 1}=`, ``);
            return this.parseCookieToFlightData(data);
        });
        
        return cookies;
    }

    private saveToCookie(cookieData: string) {
        let cookies = document.cookie.split(";");
        cookies = (cookies[0]) ? cookies : [];
        let cookieName = `${this.cookiePrefix}_${cookies.length + 1}`;
        let expiry = moment().add(this.expiryDays, 'days').toDate();
        document.cookie = `${cookieName}=${cookieData}; ${expiry}; path=/;`;
    }

    private removeCookie(index: number) {
        let cookieName = `${this.cookiePrefix}_${index + 1}`;
        document.cookie = `${cookieName}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
        this.arrangeCookies();
    }

    private arrangeCookies() {
        let cookies = document.cookie.split(";");

        if (!cookies[0]) {
            return;
        }

        cookies = _.map(cookies, (cookie: string, idx: number) => {
            cookie = cookie.trim();
            let cookieData = cookie.split("=");
            let dataStore = this.parseCookieToFlightData(cookieData[1]);

            if (`${this.cookiePrefix}_${idx + 1}` == cookieData[0]) return;

            document.cookie = `${cookieData[0]}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
            document.cookie = `${this.cookiePrefix}_${idx + 1}=${cookieData[1]}; ${dataStore.created_at}; path=/;`;
        });
    }
    
}