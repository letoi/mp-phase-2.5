declare var webpackHotUpdate: any;

export namespace StyleLoader {
    export function attachLoader() {
        if (typeof webpackHotUpdate !== "undefined") {
            require("../assets/styles/main.scss");
        }
    }
}