export class RecentSearchData {
    origin: string = "";
    destination: string = "";
    trip_type: string = "";
    departure: number = 0;
    arrival: number = 0;
    class: string = "";
    adults: number = 0;
    children: number = 0;
    infants: number = 0;
    created_at: number = 0;
    updated_at: number = 0;
}

export class RecentSearchTokenData {
    search_history_bookflights: Array<any> = [];
    search_history_redeemflights: Array<any> = [];
    search_history_bookflights_hotel: Array<any> = [];
}