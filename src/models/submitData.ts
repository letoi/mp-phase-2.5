export default class SubmitData {
    _eventId: string;
	arrivalAirportCode: string;
	departureAirportCode: string;
	departureDate: string;
	flightID: number;
    flightNo: string;
    passenger: Array<PassengerData>

    constructor(){
        this._eventId = "updateSeatMap";
        this.arrivalAirportCode = "";
        this.departureAirportCode = "";
        this.departureDate = "";
        this.flightID = 0;
        this.flightNo = "";
        this.passenger = [];
    }
}

export class PassengerData {
    id: number;
    newSeat: string;
    newSeatType: string;
    newSeatPrice: string;
    newCurrency: "";
    oldSeat: string;
    oldSeatType: string;
    oldSeatPrice: string;
    oldCurrency: string;
    seatAddon: object;

    constructor(){
        this.id = 0;
        this.newSeat = "";
        this.newSeatType = "";
        this.newSeatPrice = "";
        this.newCurrency = "";
        this.oldSeat = "";
        this.oldSeatType = "";
        this.oldSeatPrice = "";
        this.oldCurrency = "";
        this.seatAddon = {};
    }
}

