export default class SeatSelectionData {
    flight: SeatSelectionFlightData;
    selectedId: number;
    seat: any; // {}
    selectedSeats: Array<any>; // []
    mainDeckCount: number;
    upperDeckCount: number;
    activeDeck: string;
    addonDetails: any; // {}
    allFlightSegmentDetails: any; // []

    constructor() {
        this.flight = new SeatSelectionFlightData();
        this.flight.flightInfo = {};
        this.flight.updatedPassengerData = [];

        this.selectedId = 0;
        this.seat = {};
        this.selectedSeats = [];
        this.selectedSeats = [];
        this.upperDeckCount = 0;
        this.mainDeckCount = 0;
        this.activeDeck = '';
        this.addonDetails = {};
        this.allFlightSegmentDetails = [];
    }
}

class SeatSelectionFlightData {
    flightInfo: any; // {}
    updatedPassengerData: Array<any>; // []

    constructor(){
        this.flightInfo = {};
        this.updatedPassengerData = [];
    }
}
