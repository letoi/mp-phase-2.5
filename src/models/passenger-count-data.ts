export class PassengerCountData {
    adult: number = 1;
    children: number = 0;
    infant: number = 0;
}

export class PassengerCountHotelData {
    adult: number = 1;
    children: Array<ChildrenCountData> = [];
    infant: number = 0;
}

export class ChildrenCountData {
    childAge: number = 0;
}

export class RoomCountData {
    room: Array<PassengerCountHotelData> = []
}