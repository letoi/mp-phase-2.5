export default class UserSummaryPanel {
    isLogin: boolean;
    userSummaryPanelText: string;
    userData:any;
    country:string;
    userType:string;

    constructor() {
        this.isLogin = false;
        this.userSummaryPanelText = "";
        this.userData = {};
        this.country = 'SG';
        this.userType = 'guest';
    }
}