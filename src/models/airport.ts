import { PassengerCountData, PassengerCountHotelData, RoomCountData } from "./passenger-count-data";

export class FlightStatusAirportRootData {
	defaultSelectedAirport: string = "";
	airportList: FlightStatusAirportData[] = [];
}

export class FlightStatusAirportData {
    airportCode: string = "";
    airportName: string = "";
    cityCode: string = "";
    cityName: string = "";
    countryName: string = "";
    countryCode: string = "";
    isCIBOrigin: string = "";
    isCIBDestination: string = "";
    isORBOrigin: string = "";
    isORBDestination: string = "";
	excludedCountries: string = "";

	// UI
	label: string = "";
	value: string = "";
}

export class CIBAirportData {
    airportCode: string = "";
    airportName: string = "";
    cityCode: string = "";
    cityName: string = "";
    countryName: string = "";
    countryCode: string = "";
    isCIBOrigin: string = "";
    isCIBDestination: string = "";
    excludedCountries: string = "";
    label: string = "";
    value: string = "";
}

export class ORBAirportData {
    airportCode: string = "";
    airportName: string = "";
    cityCode: string = "";
    cityName: string = "";
    countryName: string = "";
    countryCode: string = "";
    isORBOrigin: string = "";
    isORBDestination: string = "";
    excludedCountries: string = "";
}

export class FlightData {
    from: CIBAirportData = new CIBAirportData();
    to: CIBAirportData = new CIBAirportData();
    cabinDescription: any = {};
    schedule: any = {};
    passengerCount: PassengerCountData = new PassengerCountData();
}

export class FlightHotelData {
    from: CIBAirportData = new CIBAirportData();
    to: CIBAirportData = new CIBAirportData();
    cabinDescription: any = {};
    schedule: any = {};
    passengerCount: RoomCountData = new RoomCountData();
}

export class FlightStatusQuery {
	from: FlightStatusAirportData = new FlightStatusAirportData();
	to: FlightStatusAirportData = new FlightStatusAirportData();
    schedule: any = {};
}
