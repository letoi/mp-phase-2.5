export namespace Constants {
    export const cabinClass: Array<any> = [
        {
            cabinId: "ECO",
            cabinDescription: "Economy"
        },
        {
            cabinId: "PEY",
            cabinDescription: "Premium Economy"
        },
        {
            cabinId: "BSN",
            cabinDescription: "Business"
        },
        {
            cabinId: "SUT",
            cabinDescription: "First / Suites"
        }
    ];
    export const passengerCountDictionary: any = {
        "ECO": 9,
        "PEY": 6,
        "BSN": 4,
        "SUT": 4
    }
}