import { toDateObject } from "../mixins/utilities";
import * as moment from 'moment';

export function dateMonthYearFormat(value: any) {
    let objectDate = toDateObject(new Date(value));
    return `${objectDate.date} ${objectDate.month} ${objectDate.year}`;
}

export function dateCompleteMonthYearFormat(value: any) {
    let objectDate = toDateObject(new Date(value), true);
    return `${objectDate.date} ${objectDate.month} ${objectDate.year}`;
}

export function recentSearchDate(value: any) {
    let startDate = toDateObject(moment(value.departure).toDate()),
        endDate;
    if (value.tripType == 'return') {
        endDate = toDateObject(moment(value.arrival).toDate());
        return `${startDate.date} ${startDate.month} - ${endDate.date} ${endDate.month} ${endDate.year}`;
    }

    return `${startDate.date} ${startDate.month} ${startDate.year}`;
}