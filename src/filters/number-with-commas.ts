export default function numberWithCommas(value: any) {
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}