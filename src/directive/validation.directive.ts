import { VNode } from "vue";

var requiredElements: Array<any> = [];
var modelRequire: string = '';
var errorRequire: string = '';
var requiredBlurEvent = new Event('blur');

function vModelValue(vnode: any): string {
    return vnode.data.directives.find((directive: any) => {
        return directive.name === 'model';
    }).value;
}

function removeError(el: Element) {
    var errorElement = getErrorMessageElement(el);
    var parentElement: Element = <Element>el.parentElement;
    if (errorElement) errorElement.classList.add('hidden');
    parentElement.classList.remove('validation_container_error');
    if (el.nextElementSibling) el.nextElementSibling.classList.remove('validation_form_label_error');
    parentElement.parentElement!.parentElement!.classList.remove('has-error');
}

function addError(el: Element) {
    var errorElement = getErrorMessageElement(el);
    var parentElement: Element = <Element>el.parentElement;
    if (errorElement) errorElement.classList.remove('hidden');
    parentElement.classList.add('validation_container_error');
    if (el.nextElementSibling) el.nextElementSibling.classList.add('validation_form_label_error');
    parentElement.parentElement!.parentElement!.classList.add('has-error');
}

function checkRequired(el: Element): boolean {
    if (modelRequire == "") {
        addError(el);
        return false;
    } else {
        removeError(el);
        return true;
    }
}

function getErrorMessageElement(el: Element): Element {
    var parentElement: Element = <Element>el.parentElement;
    var errorMessage = parentElement.getElementsByClassName('validation_container');
    return errorMessage[0];
}

export const RequiredModel = {
    bind(el: Element, binding: any, vnode: VNode) {
        el.addEventListener('focus', () => {
            if (el.classList.contains('pristine') && !binding.modifiers.calendar) {
                el.classList.remove('pristine');
            }
            removeError(el);
        });
    },

    inserted(el: Element, binding: any, vnode: VNode) {
        var requiredElement = {
            el: el,
            binding: binding
        }
        requiredElements.push(requiredElement);
    },

    update(el: Element, binding: any, vnode: VNode) {
        modelRequire = binding.value;
        if (!el.classList.contains('pristine')) {
            checkRequired(el);
        }
    }
}

export function validate(el: any) {
    modelRequire = el.value;
    if (!el.classList.contains('pristine')) {
        checkRequired(el);
    }
}

export function validateAll(event: any): boolean {
    var inputElements = event.target.getElementsByTagName('input')
    var isRequiredNoError: boolean = true;
    for (let input of inputElements) {
        if (input.classList.contains('validate')) {
            modelRequire = input.value;
            var isChecked = checkRequired(input);
            isRequiredNoError = isRequiredNoError && isChecked;
        }
    }
    return isRequiredNoError;
}