import Vue from "vue";
import Component from "vue-class-component";
import "./multi-tab.component.scss";

declare var _: any;

const $MultiTabComponent = Vue.extend({
    props: {
        value: Object
    }
})

@Component({
    template: require("./multi-tab.component.html"),
    props: ['value']
})

export default class MultiTabComponent extends $MultiTabComponent {
    windowWidth: number = 0;
    created() {
        let self = this;
        self.windowWidth = document.documentElement.clientWidth; // On Load Initial
    }
    mounted() { }
}