import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./pax-packs.component.scss";
import LightBoxComponent from "../../components/light-box/light-box.component";
import TabViewComponent from "../../components/tabview/tab-view.component";
import VTooltip from 'v-tooltip';

const PaxPacksData = Vue.extend({
    props: {
        paxinfo: Object
    },
    
    data() {
        return {
        }
    }
})

@Component({
    template: require("./pax-packs.component.html"),
    components: {
    	LightBoxComponent,
        TabViewComponent
    }
})

export default class PaxPacksComponent extends PaxPacksData {

	showExtraBaggage: string = '0';
    

    mounted() {
    }

    openModal(id: string){
        this.showExtraBaggage = id;
    }

    closeModal() {
        this.showExtraBaggage = '0';
    }
}

