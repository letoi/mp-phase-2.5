import Vue from "vue";
import Component from "vue-class-component";
import LightBoxComponent from "../light-box/light-box.component";
import "./footer.component.scss";
import axios from "axios";



const $FooterComponent = Vue.extend({
    props: {
        footerdata: Object
    },
    data(){
        return {
            
        }
    }
})

@Component({
    template: require("./footer.component.html"),
    components:{
        LightBoxComponent                                                                                                                                                                                                                                                                                                   
    }
})

export default class FooterComponent extends $FooterComponent {
   
    mounted(){
        
    }

    scrollToTop(){
        var elementTop: Element = <Element>document.querySelector('.container-center.header-main_wrap');
        elementTop.scrollIntoView();
    }
}