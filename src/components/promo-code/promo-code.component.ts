import Vue from "vue";
import Component from "vue-class-component";
import "./promo-code.component.scss";
import { RequiredModel, validate, validateAll } from "../../directive/validation.directive";

declare const _: any;

const $PromoCodeComponent = Vue.extend({
    props: {
        promocodedata: Object
    },
    directives: {
        requiredModel: RequiredModel
    },
    data() {
        return {
            promoCodeReference: ""
        }
    }
})

@Component({
    template: require("./promo-code.component.html"),
})

export default class PromoCodeComponent extends $PromoCodeComponent {
    mounted() {
    }

    checkForm(event: any) {
        let clearElement = <HTMLElement>document.querySelector('.ico-clear');
        if (this.promoCodeReference.length > 0) {
            //return true;
            clearElement.style.display = 'block';
        } else {
            clearElement.style.display = 'none';
        }
    }

    onPromoCodeReferenceFocus() {
        let promoCodeReferenceRef = <Element>this.$refs.promoCodeReferenceRef;
        promoCodeReferenceRef.removeClass('has-error');
        let promoCodeErrorRef = <Element>this.$refs.promoCodeErrorRef;
        promoCodeErrorRef.addClass('hidden');
    }

    onPromoCodeReferenceBlur() {
        if (!this.promoCodeReference) {
            let promoCodeReferenceRef = <Element>this.$refs.promoCodeReferenceRef;
            promoCodeReferenceRef.addClass('has-error');
            let promoCodeErrorRef = <Element>this.$refs.promoCodeErrorRef;
            promoCodeErrorRef.removeClass('hidden');
            let clearElement = <HTMLElement>document.querySelector('.ico-clear');
            clearElement.style.display = 'none';
        }
    }

    resetForm() {
        var self = this; //you need this because *this* will refer to Object.keys below`

        //Iterate through each object field, key is name of the object field`
        Object.keys(this.promoCodeReference).forEach(function(key,index) {
          self.promoCodeReference = '';
        });

        this.onPromoCodeReferenceBlur();
    }

    onValidationBlur(event: any) {
        validate(event.target);
    }
}