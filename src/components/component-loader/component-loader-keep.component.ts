import Vue from "vue";
import Component from "vue-class-component";
import ComponentLoader from "./component-loader.component";

@Component({
    template: require("./component-loader-keep.component.html")
})
export default class ComponentLoaderKeep extends ComponentLoader {

}