import Vue from 'vue';
import Component from 'vue-class-component';

@Component({
    template: `<div hidden></div>`
})
class DefaultComponent extends Vue {}

const $ComponentLoader = Vue.extend({
    props: {
        components: {
            type: [Object, Array],
            default: []
        }
    },
    data() {
        return {
            windowWidth: 0,
            dynamicComponent: DefaultComponent,
            dynamicProps: { }
        }
    },
    watch: {
        windowWidth: function(newWindowWidth) {
            let loadDefault = true;
            for(var i = 0; i < this.components.length; i ++) {
                let loader = this.components[i];
                
                if (newWindowWidth > loader.minWidth || newWindowWidth < loader.maxWidth) {
                    this.dynamicComponent = loader.component;
                    this.dynamicProps = (loader.props || {});
                    loadDefault = false;
                    break;
                }
            }
            if (loadDefault) {
                this.dynamicComponent = DefaultComponent;
                this.dynamicProps = {};
            }
        }
    }
});

@Component({
    template: require("./component-loader.component.html")
})
export default class ComponentLoader extends $ComponentLoader {
    created() {
        let self = this;
        self.windowWidth = document.documentElement.clientWidth; // On Load Initial
        for(var i = 0; i < this.components.length; i ++) {
            let loader = this.components[i];
            if (self.windowWidth > loader.minWidth) {
                this.dynamicComponent = loader.component;
                this.dynamicProps = (loader.props || {});
                break;
            }
        }
		this.$nextTick(function () {
			window.addEventListener('resize', function() {
				self.windowWidth = document.documentElement.clientWidth;
			});
		});
    }
}