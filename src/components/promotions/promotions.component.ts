import Vue from 'vue';
import Component from 'vue-class-component';
import axios from 'axios';

const slick = require('slick-carousel');
const $ = require('jquery');

import './promotions.component.scss';
import '../../../node_modules/slick-carousel/slick/slick.scss';
// import '../../../node_modules/slick-carousel/slick/slick-theme.scss';

const $PromotionsComponent = Vue.extend({
  data() {
    return {
      promotions: ''
    }
  }
})

@Component({
  template: require('./promotions.component.html'),
})

export default class PromotionsComponent extends $PromotionsComponent {
  mounted() {
  }

  created() {
    this.getData();
  }

  updated() {
    var self = this;
    $(document).ready(function () {
      self.initializeSlick();
    });
  }

  async getData() {
    const response = await axios.get('assets/JSONS/promotions.json');
    this.promotions = response.data;
    return response.data;
  }

  initializeSlick() {
    const $slick = $('.js-promotions-slider');

    if ($slick.hasClass('slick-initialized')) {
      $slick.slick('unslick');
    }
    const option = {
      infinite: true,
      slidesToShow: 1,
      slideToScroll: 1,
      arrows: false,
      centerMode: true,
      mobileFirst: true,
      responsive: [{
        breakpoint: 767,
        settings: 'unslick',
      }]
    };

    $(window).on('load resize orientationchange', function () {
      if ($slick.hasClass('slick-initialized')) {
        return;
      }

      $slick.slick(option);
    });
  }
}
