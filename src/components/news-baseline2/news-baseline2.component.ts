import Vue from 'vue';
import Component from 'vue-class-component';
import "./news-baseline2.component.scss";

import axios from "axios";

const $NewsBaseline2Component = Vue.extend({
  data() {
    return {
      data: ''
    }
  }
})

@Component({
  template: require('./news-baseline2.component.html'),
  components: {

  }
})

export default class NewsBaseline2Component extends $NewsBaseline2Component {
  mounted() {
    this.getData();
  }

  async getData() {
    const response = await axios.get('assets/JSONS/news.json');

    this.data = response.data;

    return response.data;
  }
}
