import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./payment-method.component.scss";
import LightBoxComponent from "../../components/light-box/light-box.component";
import PaymentTabComponent from "../../components/payment-tab/payment-tab.component";
import { directive as onClickaway } from 'vue-clickaway';

declare var pageData: any;
declare var selectedCardIndex: any;
declare var selectedCard: any;
const PaymentMethodData = Vue.extend({
    directives: {
        onClickaway: onClickaway
    },
    props: {
        paymentinfo: Object,
        bookinginfo: Object,
        cardinfo: Object
    },
    data() {
        return {
            pageData,
            collapsed: true,
            disabled: true,
            selectedCardIndex: 0,
            selectedCard: pageData.cardData.digitalWalletVO.digitalWalletCardVO[0]
        }
    }
})

@Component({
    template: require("./payment-method.component.html"),
    components: {
        LightBoxComponent,
        PaymentTabComponent
    }
})
export default class PaymentMethodComponent extends PaymentMethodData {
    
    showCVV: string = '0';   
    state: boolean = false; 
    month: boolean = false; 
    year: boolean = false;
    country: boolean = false;
    currency: boolean = false;

    mounted() {
        
    }

    created(){
        let digitalWallet = pageData.cardData.digitalWalletVO.digitalWalletCardVO;

        var defaultCard = '';
        for (let i in digitalWallet){         
            if (digitalWallet[i].defaultCard == "true"){
                defaultCard = i;
            }
        }
        
        this.selectedCardIndex = parseInt(defaultCard);        
        this.selectedCard = pageData.cardData.digitalWalletVO.digitalWalletCardVO[this.selectedCardIndex];

    }

    beforeMount(){
        this.changeSelectedCard();
    }


    changeSelectedCard(){        
        this.selectedCard = pageData.cardData.digitalWalletVO.digitalWalletCardVO[this.selectedCardIndex];
        var expiry = this.selectedCard.expiryDate.split("-");
        this.selectedCard.expiryMonth = expiry[0];
        this.selectedCard.expiryYear = expiry[1];
    }

    openModal(id: string){
        this.showCVV = id;
    }

    closeModal() {
        this.showCVV = '0';
    }

    openState() {
        this.state = true;
    }

    closeState() {
        this.state = false;
    }

    openMonth() {
        this.month = true;
    }

    closeMonth() {
        this.month = false;
    }

    openYear() {
        this.year = true;
    }

    closeYear() {
        this.year = false;
    }

    openCountry() {
        this.country = true;
    }

    closeCountry() {
        this.country = false;
    }

    openCurrency() {
        this.currency = true;
    }

    closeCurrency() {
        this.currency = false;
    }


}
