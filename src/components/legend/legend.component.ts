import Vue from "vue";
import Component from "vue-class-component";
import "./legend.component.scss";
import SeatSelectionData from "../../models/seat-selection-data";
import LightBoxComponent from "../light-box/light-box.component";
import VTooltip from 'v-tooltip';

import "./legend.component.scss";

Vue.use(VTooltip);

declare const _: any;

const $LegendComponent = Vue.extend({
    props: {
        value: SeatSelectionData,
        flight: Object,
        dictionary: Object,
        allFlightSegmentDetails: Array
    },
    components: {
        LightBoxComponent
    }
})

@Component({
    template: require("./legend.component.html"),
})

export default class LegendComponent extends $LegendComponent {
    showCabinHighlights: string = "0";
    isShowLegend: boolean = false;

    mounted() {
    }

    showLegend() {
        this.isShowLegend = !this.isShowLegend;
    }

    openModal(id: string) {
        // this.$forceUpdate();
        this.showCabinHighlights = id;
    }

    closeModal() {
        this.showCabinHighlights = '0';
    }

    getSeatType(seatType: string): string {
        return this.dictionary.seatType[seatType];
    }

    getCabinClass(flightNumber: string) {
        let result = _.find(this.allFlightSegmentDetails, (flight: any) => {
            return flight.flightNumber == flightNumber;
        });
        return result ? result.cabinClass.toLowerCase() : "";
    }

    getSeatData(key: string) {
        return _.find(this.value.flight.updatedPassengerData[this.value.selectedId] ? this.value.flight.updatedPassengerData[this.value.selectedId].seatPricing : [], (seatPrice: any) => {
            if (seatPrice.type == key) {
                return seatPrice;
            }
        })
    }

    displayLegend(seatType: string) {
        let result = true;
        if (this.getCabinClass(this.value.flight.flightInfo.flightNumber) == 'business' && (seatType == 'P' || seatType == 'F')) {
            result = false;
        } else if (this.getCabinClass(this.value.flight.flightInfo.flightNumber) == 'premiumeconomy' && seatType == 'F') {
            result = false;
        }
        return result;
    }
}
