import Vue from 'vue';
import Component from 'vue-class-component';
import axios from "axios";

const slick = require('slick-carousel');
const $ = require('jquery');

import './experience-baseline1.component.scss';
import '../../../node_modules/slick-carousel/slick/slick.scss';

const $ExperienceBaselineOneComponent = Vue.extend({
  data() {
    return {
      experienceBaselineOne: ''
    }
  }
})

@Component({
  template: require('./experience-baseline1.component.html')
})

export default class ExperienceBaselineOneComponent extends $ExperienceBaselineOneComponent {
  created() {
    this.getData();
  }

  updated() {
    var self = this;
    $(document).ready(function () {
      self.initializeSlick();
    });
  }

  async getData() {
    const response = await axios.get('assets/JSONS/experience-baseline1.json');

    this.experienceBaselineOne = response.data;

    return response.data;
  }

  initializeSlick() {
    const $slick = $('.js-experience-baseline-one-slider');
    const option = {
      dots: true,
      infinite: false,
      slidesToShow: 1,
      slideToScroll: 1,
      arrows: true
    };

    $(window).on('load resize orientationchange', function () {
      if ($slick.hasClass('slick-initialized')) {
        return;
      }

      $slick.slick(option);
    });
  }
}
