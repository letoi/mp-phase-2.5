import Vue from "vue";
import Component from "vue-class-component";
import "./sticky-pax-tabs.component.scss";

declare const _: any;

const $StickyPaxTabsComponent = Vue.extend({
    props: {
        value: Object
    }
})

@Component({
    template: require("./sticky-pax-tabs.component.html"),
})

export default class StickyPaxTabsComponent extends $StickyPaxTabsComponent {
   
    created(){
        console.log(this.value);
    }

    changeActivePassenger(index: string){
        this.value.activePassengerIndex = index;
        this.$emit("input", this.value);
    }
}