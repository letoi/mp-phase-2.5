import Vue from "vue";
import Component from "vue-class-component";
import "./inline-kf-login.component.scss";
import LightBoxComponent from "../../light-box/light-box.component";

declare const _: any;

const $InlineKfLoginComponent = Vue.extend({
    props: {
        inlineKfLoginData: Object
    }
})

@Component({
    template: require("./inline-kf-login.component.html"),
    components: {
		LightBoxComponent
	}
})

export default class InlineKfLoginComponent extends $InlineKfLoginComponent {
    showLogin:string = "";
    mounted(){
    }

    openModal(){
        this.showLogin = '1';
    }

    closeModal(){
        this.showLogin = '';
    }
}