import Vue from "vue";
import Component from "vue-class-component";
import "./apis-information.component.scss";
import { StyleLoader } from "../../../mixins/style-loader";
StyleLoader.attachLoader();
declare const _: any;

const $ApisInformationComponent = Vue.extend({
    props: {
        apisInformationData: Object,
        value: Object
    }
})

@Component({
    template: require("./apis-information.component.html"),
})

export default class ApisInformationComponent extends $ApisInformationComponent {
    selectedCitizenStatus: string = "";
    mounted(){
        console.log(this.value);
    }
}