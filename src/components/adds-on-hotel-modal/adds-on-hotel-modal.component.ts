import Vue from "vue";
import Component from "vue-class-component";
import AutocompleteComponent from "../autocomplete/autocomplete.component";
import VDatepickerComponent from "../v-datepicker/v-datepicker.component";
import PassengerCountDesktopComponent from "../passenger-count-base/desktop/passenger-count/passenger-count.desktop.component";
import { RequiredModel, validate, validateAll } from "../../directive/validation.directive";
import { FlightData, CIBAirportData, ORBAirportData, FlightHotelData } from "../../models/airport";

import "./adds-on-hotel-modal.component.scss";

declare const _: any;

const $HotelModalComponent = Vue.extend({
  props: {
		isShowing: {
			type: Boolean,
			default: false
		}
	},
  data() {
    return {
      isShowing: false
    }
  },
  directives: {
    requiredModel: RequiredModel
  }
})
@Component
class BookingAutocompleteComponent extends AutocompleteComponent {
  mounted() {
    console.log("mounted autocomplete custom");
  }
}
@Component
class DatePickerBook extends VDatepickerComponent { }
@Component
class DatePickerRedeem extends VDatepickerComponent { }
@Component
class DatePickerBookHotel extends VDatepickerComponent { }
@Component
class BookPassengerCountDesktopComponent extends PassengerCountDesktopComponent { }

@Component({
  template: require("./adds-on-hotel-modal.component.html"),
  components: {
    'v-autocomplete-booking': BookingAutocompleteComponent,
    'v-autocomplete': AutocompleteComponent,
    'v-datepicker-book': DatePickerBook,
    'v-datepicker-redeem': DatePickerRedeem,
    'v-datepicker-bookhotel': DatePickerBookHotel,
    'v-passengercount-book': BookPassengerCountDesktopComponent
  }
})

export default class HotelModalComponent extends $HotelModalComponent {
  bookFlightHotelData: FlightHotelData = new FlightHotelData();
  showHotel: string = '0';

  mounted() {
		this.$root.$on('hotelModal', (id: string) => {
			this.openModal(id);
		})
  }

  //custom methods
	openModal(id: string) {
		// this.$forceUpdate();
		this.showHotel = id;
	}

	closeModal() {
		this.showHotel = '0';
	}

  checkForm(event: any) {
    event.preventDefault();

    if (validateAll(event)) {
      console.log('save data search here!');

    }

    return false;
  }
}