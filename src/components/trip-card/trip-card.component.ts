import Vue from 'vue';
import Component from 'vue-class-component';

const slick = require('slick-carousel');
const $ = require('jquery');

import "./trip-card.component.scss";
import '../../../node_modules/slick-carousel/slick/slick.scss';

import axios from "axios";

const $TripCardComponent = Vue.extend({
  data() {
    return {
      tripCard: Object
    }
  }
})

@Component({
  template: require('./trip-card.component.html'),
  components: {

  }
})

export default class TripCardComponent extends $TripCardComponent {
  tripCard: any;

  mounted() {
    var self = this;
    $(document).ready(function () {
      self.initializeSlick();
    });

  }

  created() {
    this.getData();
  }

  async getData() {
    const response = await axios.get('assets/JSONS/trip-card.json');

    this.tripCard = response.data;

    return response.data;
  }

  initializeSlick() {
    const $slick = $('.js-card-slider');

    if ($slick.hasClass('slick-initialized')) {
      $slick.slick('unslick');
    }
    const option = {
      dots: true,
      slidesToShow: 1,
      slideToScroll: 1,
      infinite: true,
      arrows: false,
      centerMode: true,
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 767,
          settings: 'unslick',
        }
      ]
    };

    $(window).on('load resize orientationchange', function () {
      if ($slick.hasClass('slick-initialized')) {
        return;
      }

      $slick.slick(option);
    });
  }
}
