import Vue from 'vue';
import Component from 'vue-class-component';
import axios from "axios";

const slick = require('slick-carousel');
const $ = require('jquery');

import './experience-baseline2.component.scss';
import '../../../node_modules/slick-carousel/slick/slick.scss';

const $ExperienceComponent = Vue.extend({
  data() {
    return {
      experience: ''
    }
  }
})

@Component({
    template: require('./experience-baseline2.component.html')
})

export default class ExperienceComponent extends $ExperienceComponent{
  experience: any;

  mounted() {

  }

  created() {
    this.getData();
  }

  updated() {
    var self = this;
    $(document).ready(function () {
      self.initializeSlick();
    });
  }

  async getData() {
    const response = await axios.get('assets/JSONS/experience.json');

    this.experience = response.data;

    return response.data;
  }

  initializeSlick() {
    const $slick = $('.js-experience-slider');
    const option = {
      infinite: true,
      slidesToShow: 1,
      slideToScroll: 1,
      arrows: false,
      mobileFirst: true,
      centerMode: true,
      responsive: [{
        breakpoint: 767,
        settings: 'unslick',
      }]
    };

    $(window).on('load resize orientationchange', function() {
      if ($slick.hasClass('slick-initialized')) {
        return;
      }

      $slick.slick(option);
    });
  }
}
