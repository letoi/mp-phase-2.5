import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./payment-accordion.component.scss";
import BookingInfoComponent from "../../components/booking-info/booking-info.component";
import FlightInfoComponent from "../../components/flight-info/flight-info.component";
import FlightAddonComponent from "../../components/flight-addon/flight-addon.component";
import PaxPacksComponent from "../../components/pax-packs/pax-packs.component";
import TripAddonsComponent from "../../components/trip-addons/trip-addons.component";
import CostBreakdownComponent from "../../components/cost-breakdown/cost-breakdown.component";
import VueCollapse from 'vue2-collapse';
import VTooltip from 'v-tooltip';
import axios from "axios";

Vue.use(VueCollapse);
Vue.use(VTooltip, {
    popover: {
        defaultPopperOptions: {
            modifiers: {
                preventOverflow: { padding: 0 },
                defaultPlacement: 'bottom'
            },
        },
    }
});

declare var pageData: any;


const PaymentAccordionData = Vue.extend({
    props: {
        bookingsummarydata: Object
    },
    
    data() {
        return {
            pageData,
            isActive: false,
            collapsed: true
        }
    }
})

@Component({
    template: require("./payment-accordion.component.html"),
    components: {
        BookingInfoComponent,
        FlightInfoComponent,
        FlightAddonComponent,
        PaxPacksComponent,
        TripAddonsComponent,
        CostBreakdownComponent
    }
})
export default class PaymentAccordionComponent extends PaymentAccordionData {

    mounted() {
        
    }

    activeClass(e: any) {
        if(true == e.status){
            return e.vm.nodes.toggle.classList.add("active");
        } else{
            return e.vm.nodes.toggle.classList.remove("active");
        }
    }

    toCurrency(string: any) {
            return string.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
      
    }

}
