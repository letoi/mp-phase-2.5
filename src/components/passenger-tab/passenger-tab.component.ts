import Vue from "vue";
import Component from "vue-class-component";
import "./passenger-tab.component.scss";
import SeatSelectionData from "../../models/seat-selection-data";
import VTooltip from 'v-tooltip';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import LightBoxComponent from "../light-box/light-box.component";


declare var _: any;
declare var swiperOption: any;

Vue.use(VueAwesomeSwiper);
Vue.use(VTooltip, {
    popover: {
        defaultPopperOptions: {
            modifiers: {
                preventOverflow: { padding: 0 },
                defaultPlacement: 'bottom'
            },
        },
    }
});

const $PassengerTabComponent = Vue.extend({
    props: {
        value: SeatSelectionData
    },
    components: {
        LightBoxComponent
    }
})

@Component({
    template: require("./passenger-tab.component.html")
})

export default class PassengerTabComponent extends $PassengerTabComponent {
    kfTooltipTriggered: string = "";
    moreTooltipTriggered: string = "";
    swiper: any = {};
    addonToRemove: object = {};
    showRemoveModal: string = "";
    passengerIndex: number = 0;
    swiperOption: object = {
        slidesPerView: 'auto',
        spaceBetween: 0,
        freeMode: true,
        centeredSlides: false,
        keyboard: {
            enabled: true,
        },
        navigation: {
            nextEl: '.passenger-tab_btn--next',
            prevEl: '.passenger-tab_btn--prev'
        }
    }
    windowWidth: number = 0;
    mounted() {

        let self: any = this;
        self.windowWidth = document.documentElement.clientWidth;
        self.$watch(() => { return self.value.flight.updatedPassengerData }, (passengerData: any) => {
            if (self.windowWidth >= 768 && self.windowWidth < 992) {
                self.$refs.swiperRef ? self.$refs.swiperRef.swiper.allowTouchMove = passengerData.length >= 2 : null;
            } else {
                self.$refs.swiperRef ? self.$refs.swiperRef.swiper.allowTouchMove = passengerData.length > 2 : null;
            }
        });

        self.$watch(() => { return self.value.selectedId }, () => {
            self.$refs.swiperRef.swiper.slideTo(self.value.selectedId)
        });
    }

    openModal(id: string) {
        // this.$forceUpdate();
        this.showRemoveModal = id;
    }

    closeModal() {
        this.resetData();
    }

    isEmptyAddon(addon: object) {
        return _.isEmpty(addon);
    }

    selectPassenger(index: number) {
        let self: any = this;
        this.value.selectedId = index;
        this.$emit('input', this.value);
    }

    openTooltip(passId: string, tooltip: string) {
        tooltip == 'kf' ? (this.kfTooltipTriggered = this.kfTooltipTriggered == passId ? "" : passId) : (this.moreTooltipTriggered = this.moreTooltipTriggered == passId ? "" : passId);
    }

    changeActivePassenger(move: String) {
        let self: any = this;
        move == 'next' ? self.$refs.swiperRef.swiper.slideNext() : self.$refs.swiperRef.swiper.slidePrev();
        move == 'next' ? this.value.selectedId++ : this.value.selectedId--;
        this.$emit('input', this.value);
    }

    removeAddOfPassenger(passengerId: number) {
        this.passengerIndex = passengerId;
        this.openModal('5')
        this.addonToRemove = this.value.flight.updatedPassengerData[passengerId];
    }

    removeAddons(passengerId: number) {
        this.openModal("");
        this.value.flight.updatedPassengerData[passengerId].seat.selectedAddon = {};
        this.$emit('input', this.value);
    }

    isObjectEmpty(objectToTest: any): boolean {
        return _.isEmpty(objectToTest);
    }

    resetData() {
        this.showRemoveModal = "";
    }
}
