import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./terms-conditions.component.scss";

const TermsConditionsData = Vue.extend({
    data() {
        return {
            checked: false
        }
    }
})

@Component({
    template: require("./terms-conditions.component.html"),
})

export default class TermsConditionsComponent extends TermsConditionsData {
    mounted() {
    }
}
