import Vue from "vue";
import Component from "vue-class-component";
import "./light-box.component.scss";
import "../../extenders/element-extender";

const $LightBoxComponent = Vue.extend({
	props: {
		active: String,
		id: String,
		lightBoxWidth: String
	}
})

@Component({
	template: require("./light-box.component.html"),
})

export default class LightBoxComponent extends $LightBoxComponent {
	MODAL_BODY_CLASS_NAME: string = "modal-body";
	ON_LIGHT_BOX_CLOSED_EMIT_NAME: string = "lightbox-closed";

	mounted() {
		this.$watch("active", function (activeValue) {
			if (activeValue == this.id) {
				// console.log("without Scroller");
				// let modalBodyHeight = window.outerHeight;

				if (this.lightBoxWidth) {
					this.$el.getElementsByClassName("modal-body")[0]!.attr("style", `width: ${this.lightBoxWidth}`);
				}

				// document.querySelector("html")!.addClass("no-scroller");
				// this.$el.getElementsByClassName("modal-container")[0]!.attr("style", `overflow-y: scroll`);

				// Height Centering
				let modalBodyStyle = window.getComputedStyle(this.$el.getElementsByClassName("modal-body")[0]);
				if (modalBodyStyle && parseFloat(modalBodyStyle.height!) < window.innerHeight) {
					// console.log(modalBodyStyle.height);
					// modalBodyHeight = parseFloat(modalBodyStyle.height!);
					// console.log("Style: ", modalBodyStyle);

					// this.$el.getElementsByClassName(this.MODAL_BODY_CLASS_NAME)[0]!.attr("style", `top: calc(50% - ${modalBodyHeight/2}px);`);
					this.$el.getElementsByClassName("modal-container")[0]!.removeClass("full-height");
					this.$el.getElementsByClassName("modal-container")[0]!.addClass("no-scroller");
					document.querySelector("html")!.addClass("no-scroller-without-padding");
				} else {
					this.$el.getElementsByClassName("modal-container")[0]!.addClass("full-height");
					this.$el.getElementsByClassName("modal-container")[0]!.removeClass("no-scroller");
					document.querySelector("html")!.addClass("no-scroller");
				}

				// console.log("Window: ", window.innerHeight);
				// console.log("Modal: ", modalBodyHeight);
				// console.log(window.outerHeight);
			} else if (activeValue == '0' || activeValue == "") {
				// console.log("with Scroller");
				this.$el.getElementsByClassName("modal-container")[0] ? this.$el.getElementsByClassName("modal-container")[0]!.attr("style", `overflow-y: hidden`) : null;
				document.querySelector("html")!.removeClass("no-scroller");
				document.querySelector("html")!.removeClass("no-scroller-without-padding");
			}
		})
	}

	onLightboxOutClick(event: MouseEvent) {
		let modalBodyElement = document.querySelector(`.${this.MODAL_BODY_CLASS_NAME}`);
		let clickedElement = event.srcElement as Element;
		if (!(modalBodyElement!.contains(clickedElement) || clickedElement!.attr('class') == this.MODAL_BODY_CLASS_NAME)) {
			// this.active = '0';

			this.$emit(this.ON_LIGHT_BOX_CLOSED_EMIT_NAME);
		}
	}
}
