import Vue from "vue";
import Component from "vue-class-component";
import LightBoxComponent from "../light-box/light-box.component";
// import "./add-on-table.component.scss";
import axios from "axios";



const $AddOnTableComponent = Vue.extend({
    props: {
        addontabledata: Object
    },
    data(){
        return {

        }
    }
})

@Component({
    template: require("./add-on-table.component.html"),
    components:{
        LightBoxComponent
    }
})

export default class AddOnTableComponent extends $AddOnTableComponent {

    mounted(){
    }
}