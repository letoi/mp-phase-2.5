import Vue from "vue";
import Component from "vue-class-component";
import "./manage-booking.component.scss";
import { RequiredModel, validate, validateAll } from "../../directive/validation.directive";
import UserSummaryPanel from "../../models/user-data";
import { toDateObject } from "../../mixins/utilities";

declare const _: any;

const $ManageBookingComponent = Vue.extend({
    directives: {
        requiredModel: RequiredModel
    },
    props: {
        value: UserSummaryPanel,
        pnrList: Array
    },
    data() {
        return {
            detailSelection: "bookReference",
            bookingReferenceBR: "",
            familyNameBR: "",
            bookingReferenceET: "",
            familyNameET: ""
        }
    }
})

@Component({
    template: require("./manage-booking.component.html"),
})

export default class ManageBookingComponent extends $ManageBookingComponent {
    showManualBooking: boolean = false;
    mounted() {
        this.value.userSummaryPanelText = "Log in to easily access and manage existing bookings.";
    }

    toDateObject(dateString: string) {
		let date = new Date(dateString);
		return toDateObject(date);
	}

    checkForm(event: any) {
        if (validateAll(event)) {
            //return true;
            event.target.submit();
        }
        return false;
    }

    onValidationBlur(event: any) {
        validate(event.target);
    }
}