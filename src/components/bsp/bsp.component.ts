import Vue from "vue";
import Component from "vue-class-component";
import "./bsp.component.scss";

declare const _: any;

const $BspComponent = Vue.extend({
    props: {
        bspData: Object
    }
})

@Component({
    template: require("./bsp.component.html"),
})

export default class BspComponent extends $BspComponent {

    showAll: boolean = false;
    isBSPCollapse: boolean = true;
    isBSPCollapseContainer: boolean = false;

    // toOpenCollapse: string = "";
    // toOpenCollapseContainer: string = "";

    mounted() { }

    // toggleDetails(toOpen: string){
    //     if (this.toOpenCollapse == toOpen) {
    // 		this.toOpenCollapse = "";
    // 		setTimeout(() => {
    // 			this.toOpenCollapseContainer = "";
    // 		}, 500);
    // 	} else {
    // 		this.toOpenCollapse = toOpen;
    // 		this.toOpenCollapseContainer = toOpen;
    // 	}
    // }

    toggleDetails() {
        this.isBSPCollapse = !this.isBSPCollapse;
        this.isBSPCollapseContainer = !this.isBSPCollapseContainer;
    }
}