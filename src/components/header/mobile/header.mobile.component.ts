// import Vue from "vue";
import Component from "vue-class-component";
import HeaderComponent from "../header.component";

declare var _: any;

@Component({
	template: require("./header.mobile.component.html")
})
export default class HeaderMobileComponent extends HeaderComponent {
	isNavActive: boolean = false;
	body: any = document.getElementsByTagName("BODY")[0];
	curLabel: string = '';
	
	mounted() {
	}

	toggleMainNav() {
		this.isNavActive = !this.isNavActive;

		this.isNavActive ? this.body.classList.add('disable-scroll') : this.body.classList.remove('disable-scroll');
	}

	toggleSubNav(nameLabel:string) {
		this.curLabel = nameLabel === this.curLabel ? '' : this.curLabel = nameLabel;
		console.log(this.curLabel);
	}
}
