import Vue from "vue";
import Component from "vue-class-component";
import LightBoxComponent from "../light-box/light-box.component";
import { focus } from 'vue-focus';

declare var _: any;

const $HeaderComponent = Vue.extend({
	props: {
		headerdata: Array,
		country: String,
		loggedin: Boolean,
		checkedInPax: Number,
		logoLink: String,
		userdata: {
			type: Object,
			default: {}
		}
	}
})

@Component({
	components: {
		LightBoxComponent
	},
	directives: {
		focus: focus
	}
})

export default class HeaderComponent extends $HeaderComponent {
	//variable with strict typing
	showLogin: string = '0';
	languages: object = {};
	userType: string = 'guest';
	userCountry: string = 'SG';
	
	// Popover booleans
	loginPopoverShown: boolean = false;

	//on page load method
	mounted() {
		this.$root.$on('loginModal', (id: string) => {
			this.openModal(id);
		})
	}

	//custom methods
	openModal(id: string) {
		// this.$forceUpdate();
		this.showLogin = id;
	}

	closeModal() {
		this.showLogin = '0';
	}

	// check if link should be displayed
	showLink(link: any): boolean {
		// in Teamsite user can have both allowedCountry and restrictedCountry as long as the Country would only exist in either one not both
		// allowedCountry and restrictedCountry can exist together on single link

		let show = false;

		// allowedCountry is to only display this link only on the countries listed in the array
		if (link.allowedCountry && link.allowedCountry.indexOf(this.userCountry) > -1) show = true
		// restrictedCountry is to display this link in ALL countries excluding the countries listed in the array
		if (link.restrictedCountry && link.restrictedCountry.indexOf(this.userCountry) == -1) show = true

		// it is fine if the front-end condition would check allowedCountry then next is restrictedCountry

		if (!link.restrictedCountry && !link.allowedCountry) show = true

		return show;
	}

	// filter secondlevel links
	getSecondLevelLinks(links: Array<object>, userType: String): Array<object> {
		let filtered: Array<object> = _.filter(links, function (o: any) { return (o.userDisplay.indexOf(userType) > -1); });

		return filtered;
	}

	// filter third level links
	getThirdLevelLinks(links: any, userType: String): Array<object> {
		let spliced = links.thirdLevelNavigationContainer.slice(0, 5);
		let _self = this;

		let filtered: Array<object> = _.filter(spliced, function (o: any) {
			return !o.userDisplay || (o.userDisplay && o.userDisplay.indexOf(userType) != -1) && _self.showLink(o)
		});

		return filtered;
	}

	// Usability tabbing
	popOverQueryToFocusNext(parentId: string, toFocusNextQuery: string) {
		let toFocus = document.querySelector(`#${parentId}`)!.querySelector(toFocusNextQuery);
		if (toFocus) {
			let checkExist = setInterval(() => {
				if (document.body.contains(toFocus!)) {
					clearInterval(checkExist);
					toFocus!.focus();
				}
			}, 100); // check every 100ms
		}
	}
}
