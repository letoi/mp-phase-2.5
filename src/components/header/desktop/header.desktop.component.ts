import Vue from "vue";
import Component from "vue-class-component";
import HeaderComponent from "../header.component";
import "./header.desktop.component.scss";

@Component({
    template: require("./header.desktop.component.html")
})
export default class HeaderDesktopComponent extends HeaderComponent {

}