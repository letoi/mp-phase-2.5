import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./tab-view.component.scss";

const TabsComponentData = Vue.extend({
    props: {
        tabs: Number,
        active: Number,
        id: String
    }
})

@Component({
    template: require("./tab-view.component.html"),
    props: ['tabs', 'active', 'id']
})
export default class TabViewComponent extends TabsComponentData {
    activeTab: number = this.active;
    mounted() {
        for (let tab = 1; tab < this.tabs + 1; tab++) {
            var tabElm: Element = <Element>this.$slots['title-' + tab][0].elm;
            tabElm.parentElement!.addEventListener('click', () => {
                this.activeTab = tab;
            });
        }
    }

    getActiveTab(id: number) {
        return this.activeTab == id ? true : false;
    }
}
