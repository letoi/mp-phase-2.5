import Vue from "vue";
import Component from "vue-class-component";
import "./masthead.component.scss";
import axios from "axios";

const $MastheadComponent = Vue.extend({
    props: {
        mastheaddata: Array
    },
    data(){
        return {
            
        }
    }
})

@Component({
    template: require("./masthead.component.html")
})


export default class MastheadComponent extends $MastheadComponent{
   
    mounted(){
        
    }
}