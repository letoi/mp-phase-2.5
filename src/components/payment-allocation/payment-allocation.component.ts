import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./payment-allocation.component.scss";
import LightBoxComponent from "../../components/light-box/light-box.component";


const PaymentAllocationData = Vue.extend({
    data() {
        return {
        }
    }
})

@Component({
    template: require("./payment-allocation.component.html")
})

export default class PaymentAllocationComponent extends PaymentAllocationData {    

    mounted() {
    }

}
