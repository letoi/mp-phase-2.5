import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import RangeSlider from 'vue-range-slider';
import "./pay-miles.component.scss";
import LightBoxComponent from "../../components/light-box/light-box.component";
import PaymentAllocationComponent from "../../components/payment-allocation/payment-allocation.component";
import '../../../node_modules/vue-range-slider/dist/vue-range-slider.css'

let document:any;

const PayMilesData = Vue.extend({
    data() {
        return {
            collapsed: true,
            sliderValue: 29988,
            min: 5000,
            max: 37000,
            step: 98,
            disabled: false,
            name: 'payMiles'
        }
    }
})

@Component({
    template: require("./pay-miles.component.html"),
    components: {
        LightBoxComponent,
        PaymentAllocationComponent,
        RangeSlider
    }
})

export default class PayMilesComponent extends PayMilesData {    
    
    showPaymentAllocation: string = '0';
    showPayWithKrisFlyer: string = '0';

    mounted() {
    }

    openModal(id: string){
        this.showPaymentAllocation = id;
        this.showPayWithKrisFlyer = id;
    }

    closeModal() {
        this.showPaymentAllocation = '0';
        this.showPayWithKrisFlyer = '0';
    }

    incrementRange() {
        var sliderStep = this.sliderValue + this.step;
        if(sliderStep > this.max){
            return this.sliderValue = this.max;
        }else {
            return this.sliderValue += this.step;
        }
    }

    decrementRange() {
        var sliderStep = this.sliderValue - this.step;
        if(sliderStep < this.min){
            return this.sliderValue = this.min;
        }else {
            return this.sliderValue -= this.step;
        }
    }

    showTooltip() {
        let tooltipMin = <Element>this.$refs.tooltipMin;
        let tooltipMax = <Element>this.$refs.tooltipMax;
        if(this.sliderValue === this.min) {
            tooltipMin.addClass('show');
            tooltipMax.removeClass('show');
        }else if(this.sliderValue === this.max){
            tooltipMax.addClass('show');
            tooltipMin.removeClass('show');
        }else {
            tooltipMin.removeClass('show');
            tooltipMax.removeClass('show');
        }
    }

}
