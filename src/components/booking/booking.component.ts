import Vue from "vue";
import Component from "vue-class-component";
import "./booking.component.scss";
import AutocompleteComponent from "../autocomplete/autocomplete.component";
import VDatepickerComponent from "../v-datepicker/v-datepicker.component";
import PassengerCountDesktopComponent from "../passenger-count-base/desktop/passenger-count/passenger-count.desktop.component";
import { RequiredModel, validate, validateAll } from "../../directive/validation.directive";
import { FlightData, CIBAirportData, ORBAirportData, FlightHotelData } from "../../models/airport";
import { RecentSearchData, RecentSearchTokenData } from "../../models/recent-search";
import { filterCIBOrigin, filterCIBDestination, parseAirportData, filterORBOrigin, filterORBDestination } from "../../mixins/booking-filters";
import { Constants } from "../../models/constants";
import axios from "axios";
import VTooltip from 'v-tooltip';
import UserSummaryPanel from '../../models/user-data';
import * as moment from "moment";
import storageAvailable from 'storage-available';
import PassengerCountHotelDesktopComponent from "../passenger-count-base/desktop/passenger-count-hotel/passenger-count-hotel.desktop.component";
import { PassengerCountHotelData, RoomCountData } from "../../models/passenger-count-data";

Vue.use(VTooltip, {
    popover: {
        defaultPopperOptions: {
            modifiers: {
                preventOverflow: { padding: 0 },
                defaultPlacement: 'bottom'
            },
        },
    }
});

declare var _: any;
const $BookingComponent = Vue.extend({
    directives: {
        requiredModel: RequiredModel
    },
    props: {
        bookingData: Array,
        value: UserSummaryPanel,
        userData: Object
    },
    filters: {
        formatRecentSearchDay: function (unix_timestamp: string) {
            return moment(unix_timestamp).date();
        },
        formatRecentSearchMonth: function (unix_timestamp: string) {
            return moment(unix_timestamp).format('MMM');
        },
        formatRecentSearchYear: function (unix_timestamp: string) {
            return moment(unix_timestamp).format('YYYY');
        }
    },
    data() {
        return {
            flightSelection: "bookFlight"
        }
    }
})

@Component
class BookingAutocompleteComponent extends AutocompleteComponent {
    mounted() {
        console.log("mounted autocomplete custom");
    }
}

@Component
class DatePickerBook extends VDatepickerComponent {}
@Component
class DatePickerRedeem extends VDatepickerComponent {}
@Component
class DatePickerBookHotel extends VDatepickerComponent {}
@Component
class BookPassengerCountDesktopComponent extends PassengerCountDesktopComponent {}
@Component
class RedeemPassengerCountDesktopComponent extends PassengerCountDesktopComponent {}


@Component({
    template: require("./booking.component.html"),
    components: {
        'v-autocomplete-booking': BookingAutocompleteComponent,
        'v-autocomplete': AutocompleteComponent,
        'v-datepicker-book': DatePickerBook,
        'v-datepicker-redeem': DatePickerRedeem,
        'v-datepicker-bookhotel': DatePickerBookHotel,
        'v-passengercount-book': BookPassengerCountDesktopComponent,
        'v-passengercount-redeem': RedeemPassengerCountDesktopComponent,
        'v-passengerhotelcount': PassengerCountHotelDesktopComponent
    }
})

export default class BookingComponent extends $BookingComponent {
    cabin: any = Constants.cabinClass;
    bookFlightData: FlightData = new FlightData();
    redeemFlightData: FlightData = new FlightData();
    bookFlightHotelData: FlightHotelData = new FlightHotelData();
    originCountriesBook: CIBAirportData[] = [];
    destinationCountriesBook: CIBAirportData[] = [];
    originCountriesRedeem: ORBAirportData[] = [];
    destinationCountriesRedeem: ORBAirportData[] = [];
    originCountriesBookHotel: ORBAirportData[] = [];
    destinationCountriesBookHotel: ORBAirportData[] = [];
    bookFlightJSON: any;
    redeemFlightJSON: any;
    bookFlightJSONURL: string = "/assets/JSONS/CIBAirportListJSON.json";
    redeemFlightJSONURL: string = "/assets/JSONS/ORBAirportListJSON.json";
    bookFlightSelection: string = 'bookFlight';
    redeemFlightSelection: string = 'redeemFlight';
    bookFlightHotelSelection: string = 'bookFlightsHotel';

    RECENT_SEARCH_LOCAL_STORAGE_KEY_NAME: string = "turbo";

    recentSearchLocalStorage: RecentSearchTokenData = new RecentSearchTokenData();

    mounted() {
        // this.value.userSummaryPanelText = "Log in to enjoy reward flights, upgrades, exclusive benefits and a better booking experience.";
        this.$watch("flightSelection", function (flightSelectionValue) {
            if (flightSelectionValue == this.redeemFlightSelection) {
                if (this.redeemFlightJSON) {
                    this.duplicateFromBookFlight(this.redeemFlightData);
                    return;
                }
                this.onRequest(this.redeemFlightJSONURL, "ORBAirportList")
                    .then((response: any) => {
                        this.redeemFlightJSON = response;
                        this.renderORB(response);
                        this.duplicateFromBookFlight(this.redeemFlightData);
                    });
            } else if (flightSelectionValue == this.bookFlightSelection) {
                this.duplicateFromRedeemFlight(this.bookFlightData);
            }
        });

        this.bookFlightData.cabinDescription = this.redeemFlightData.cabinDescription = this.bookFlightHotelData.cabinDescription = {
            cabinId: "ECO",
            cabinDescription: "Economy"
        };
        this.onRequest(this.bookFlightJSONURL, "CIBAirportList")
            .then((response: any) => {
                this.bookFlightJSON = response;
                this.renderToFrom(response, "CIBAirportList");
                this.renderCIB(response);

                if (storageAvailable('localStorage')) {
                    this.recentSearchLocalStorage = JSON.parse(localStorage.getItem(this.RECENT_SEARCH_LOCAL_STORAGE_KEY_NAME) ? localStorage.getItem(this.RECENT_SEARCH_LOCAL_STORAGE_KEY_NAME)! : `{ "search_history_bookflights": [], "search_history_redeemflights": [], "search_history_bookflights_hotel": [] }`);
                } else {
                    // Use cookies instead
                    this.populateFromCookies();
                }
            });
    }

    async onRequest(url: string, key: string) {
        let response = await axios.get(url);
        return response.data;
    }

    duplicateFromRedeemFlight(data: any) {
        data.from = this.redeemFlightData.from;
        data.schedule = this.redeemFlightData.schedule;
        data.cabinDescription = this.redeemFlightData.cabinDescription;
        data.passengerCount = this.redeemFlightData.passengerCount;
    }

    duplicateFromBookFlight(data: any) {
        data.from = this.bookFlightData.from;
        data.schedule = this.bookFlightData.schedule;
        data.cabinDescription = this.bookFlightData.cabinDescription;
        data.passengerCount = this.bookFlightData.passengerCount;
    }

    renderToFrom(response: any, key: string) {
        let findDefaultSelected = _.find(response[key], { airportCode: response.defaultSelectedAirport });
        this.bookFlightData.from = parseAirportData(findDefaultSelected);
    }

    renderCIB(response: any) {
        this.originCountriesBook = filterCIBOrigin(response.CIBAirportList);
        this.destinationCountriesBook = filterCIBDestination(response.CIBAirportList);
        this.originCountriesBookHotel = filterCIBOrigin(response.CIBAirportList);
        this.destinationCountriesBookHotel = filterCIBDestination(response.CIBAirportList);
    }

    renderORB(response: any) {
        this.originCountriesRedeem = filterORBOrigin(response.ORBAirportList);
        this.destinationCountriesRedeem = filterORBDestination(response.ORBAirportList);
    }

    removeSelectedSearch(index: number, recentSearch: Array<any>) {
        recentSearch.splice(index, 1);
        localStorage.setItem(this.RECENT_SEARCH_LOCAL_STORAGE_KEY_NAME, JSON.stringify(this.recentSearchLocalStorage));
        this.removeCookie("CIB_SEARCH_HISTORY_" + (index + 1));
    }

    checklocalStorageExpiry(datasArray: Array<any>) {
        datasArray.filter((dataArray, index) => {
            if (!moment(dataArray.created_at).add(31, 'days').isAfter(moment().toDate())) this.removeSelectedSearch(index, datasArray);
        })
    }

    getRecentSearchData(): Array<any> {
        switch (this.flightSelection) {
            case this.bookFlightSelection: {
                this.checklocalStorageExpiry(this.recentSearchLocalStorage.search_history_bookflights);
                return this.recentSearchLocalStorage.search_history_bookflights;
            }
            case this.redeemFlightSelection: {
                this.checklocalStorageExpiry(this.recentSearchLocalStorage.search_history_redeemflights);
                return this.recentSearchLocalStorage.search_history_redeemflights;
            }
            case this.bookFlightHotelSelection: {
                this.checklocalStorageExpiry(this.recentSearchLocalStorage.search_history_bookflights_hotel);
                return this.recentSearchLocalStorage.search_history_bookflights_hotel;
            }
            default: {
                return [];
            }
        }
    }

    getFromArrayOfCountries(countryCode: string): Array<any> {
        return this.originCountriesBook.filter((countryData) => {
            if (countryData.airportCode == countryCode) {
                return countryData;
            }
        });
    }

    onClickRecentSearch(data: Array<any>, index: number) {
        switch (this.flightSelection) {
            case this.bookFlightSelection: {
                this.bookFlightData.from = this.getFromArrayOfCountries(data[index].origin)[0];
                this.bookFlightData.to = this.getFromArrayOfCountries(data[index].destination)[0];
                this.bookFlightData.schedule = {
                    start: moment(data[index].departure).toDate(),
                    end: moment(data[index].arrival).toDate()
                };
                this.bookFlightData.cabinDescription.cabinDescription = data[index].class;
                this.bookFlightData.passengerCount.adult = data[index].adults;
                this.bookFlightData.passengerCount.children = data[index].children;
                this.bookFlightData.passengerCount.infant = data[index].infants;
                break;
            }
            case this.redeemFlightSelection: {
                this.redeemFlightData.from = this.getFromArrayOfCountries(data[index].origin)[0];
                this.redeemFlightData.to = this.getFromArrayOfCountries(data[index].destination)[0];
                this.redeemFlightData.schedule = {
                    start: moment(data[index].departure).toDate(),
                    end: moment(data[index].arrival).toDate()
                };
                this.redeemFlightData.cabinDescription.cabinDescription = data[index].class;
                this.redeemFlightData.passengerCount.adult = data[index].adults;
                this.redeemFlightData.passengerCount.children = data[index].children;
                this.redeemFlightData.passengerCount.infant = data[index].infants;
                break;
            }
            case this.bookFlightHotelSelection: {
                this.bookFlightHotelData.from = this.getFromArrayOfCountries(data[index].origin)[0];
                this.bookFlightHotelData.to = this.getFromArrayOfCountries(data[index].destination)[0];
                this.bookFlightHotelData.schedule = {
                    start: moment(data[index].departure).toDate(),
                    end: moment(data[index].arrival).toDate()
                };
                this.bookFlightHotelData.cabinDescription.cabinDescription = data[index].class;
                // this.bookFlightHotelData.passengerCount.adult = data[index].adult
                // this.bookFlightHotelData.passengerCount.children = data[index].children;
                // this.bookFlightHotelData.passengerCount.infant = data[index].infants;
                break;
            }
        }
    }

    checkPairODDate(storage: Array<any>, currentSearch: RecentSearchData): boolean {
        var data = storage.filter((history) => {
            if (history.origin == currentSearch.origin && history.destination == currentSearch.destination && history.departure == currentSearch.departure && history.arrival == currentSearch.arrival) {
                return history
            }
        })
        return data.length > 0 ? true : false;
    }

    updatePaxAndTimeStamp(storage: Array<any>, currentSearch: RecentSearchData, adults: number, children: number, infants: number) {
        storage.filter((history, index) => {
            if (history.origin == currentSearch.origin && history.destination == currentSearch.destination && history.departure == currentSearch.departure && history.arrival == currentSearch.arrival) {
                history.adults = adults;
                history.children = children;
                history.infants = infants;
                history.updated_at = +moment().unix();

                storage.push(history);
                storage.splice(index, 1);
                return;
            }
        })
    }

    onCalendarOpen(openCalendar: any, date: any, event: any) {
        openCalendar(date, event);
        var ODField = document.querySelectorAll('.control_group')[0];
        ODField.classList.add('faded');
    }

    onCalendarClose(blurFocus: any, event: any) {
        blurFocus(event);
        var ODField = document.querySelectorAll('.control_group')[0];
        ODField.classList.remove('faded');
    }

    onValidationBlur(event: any) {
        validate(event.target);
    }

    onSearchHistory(flightData: any): RecentSearchData {
        var search_history: RecentSearchData = {
            origin: flightData.from.airportCode,
            destination: flightData.to.airportCode,
            trip_type: flightData.schedule.oneWay ? "oneWay" : "return",
            departure: +moment(flightData.schedule.start),
            arrival: +moment(flightData.schedule.end),
            class: flightData.cabinDescription.cabinDescription,
            adults: flightData.passengerCount.adult,
            children: flightData.passengerCount.children,
            infants: flightData.passengerCount.infant,
            created_at: +moment().toDate(),
            updated_at: +moment().toDate()
        } 
        return search_history;
    }

    getTotalAdults(flight: RoomCountData): number {
        return flight.room.reduce((prev: number, current: PassengerCountHotelData) => {
            return current.adult + prev
        }, 0);
    }

    getTotalChildren(flight: RoomCountData): number {
        return flight.room.reduce((prev: number, current: PassengerCountHotelData) => {
            return current.children.length + prev
        }, 0);
    }

    getTotalInfants(flight: RoomCountData): number {
        return flight.room.reduce((prev: number, current: PassengerCountHotelData) => {
            return current.infant + prev
        }, 0);
    }

    saveRecentSearch() {
        var flightData = this.flightSelection == this.bookFlightSelection ? this.bookFlightData : (this.flightSelection == this.redeemFlightSelection ? this.redeemFlightData : this.bookFlightHotelData);

        if (storageAvailable('localStorage')) {
            switch (this.flightSelection) {
                case this.bookFlightSelection: {
                    if (!this.checkPairODDate(this.recentSearchLocalStorage.search_history_bookflights, this.onSearchHistory(this.bookFlightSelection))) {
                        if (this.recentSearchLocalStorage.search_history_bookflights.length >= 8) this.recentSearchLocalStorage.search_history_bookflights.splice(0, 1);
                        this.recentSearchLocalStorage.search_history_bookflights.push(this.onSearchHistory(this.bookFlightSelection));
                    } else {
                        this.updatePaxAndTimeStamp(this.recentSearchLocalStorage.search_history_bookflights, this.onSearchHistory(this.bookFlightSelection), this.bookFlightData.passengerCount.adult, this.bookFlightData.passengerCount.children, this.bookFlightData.passengerCount.infant)
                    }
                    break;
                }
                case this.redeemFlightSelection: {
                    if (!this.checkPairODDate(this.recentSearchLocalStorage.search_history_redeemflights, this.onSearchHistory(this.redeemFlightData))) {
                        if (this.recentSearchLocalStorage.search_history_redeemflights.length >= 8) this.recentSearchLocalStorage.search_history_redeemflights.splice(0, 1);
                        this.recentSearchLocalStorage.search_history_redeemflights.push(this.onSearchHistory(this.redeemFlightData));
                    } else {
                        this.updatePaxAndTimeStamp(this.recentSearchLocalStorage.search_history_redeemflights, this.onSearchHistory(this.redeemFlightData), this.redeemFlightData.passengerCount.adult, this.redeemFlightData.passengerCount.children, this.redeemFlightData.passengerCount.infant)
                    }
                    break;
                }
                case this.bookFlightHotelSelection: {

                    var search_history: RecentSearchData = {
                        origin: this.bookFlightHotelData.from.airportCode,
                        destination: this.bookFlightHotelData.to.airportCode,
                        trip_type: this.bookFlightHotelData.schedule.oneWay ? "oneWay" : "return",
                        departure: +moment(this.bookFlightHotelData.schedule.start),
                        arrival: +moment(this.bookFlightHotelData.schedule.end),
                        class: this.bookFlightHotelData.cabinDescription.cabinDescription,
                        adults: this.getTotalAdults(this.bookFlightHotelData.passengerCount),
                        children: this.getTotalChildren(this.bookFlightHotelData.passengerCount),
                        infants: this.getTotalInfants(this.bookFlightHotelData.passengerCount),
                        created_at: +moment().toDate(),
                        updated_at: +moment().toDate()
                    } 

                    if (!this.checkPairODDate(this.recentSearchLocalStorage.search_history_bookflights_hotel, search_history)) {
                        if (this.recentSearchLocalStorage.search_history_bookflights_hotel.length >= 8) this.recentSearchLocalStorage.search_history_bookflights_hotel.splice(0, 1);
                        this.recentSearchLocalStorage.search_history_bookflights_hotel.push(search_history);
                    } else {
                        this.updatePaxAndTimeStamp(this.recentSearchLocalStorage.search_history_bookflights_hotel, search_history, this.getTotalAdults(this.bookFlightHotelData.passengerCount), this.getTotalChildren(this.bookFlightHotelData.passengerCount), this.getTotalInfants(this.bookFlightHotelData.passengerCount))
                    }
                    break;
                }
            }
            localStorage.setItem(this.RECENT_SEARCH_LOCAL_STORAGE_KEY_NAME, JSON.stringify(this.recentSearchLocalStorage));
        }
        else {
            // Use cookies instead
            if (this.flightSelection == this.bookFlightSelection) {
                var recentSearch = `${flightData.from.airportCode}|${flightData.to.airportCode}|R|${moment(flightData.schedule.start).format("DD/MM/YYYY")}-${moment(flightData.schedule.end).format("DD/MM/YYYY")}|${flightData.cabinDescription.cabinDescription.toLowerCase()}|${this.bookFlightData.passengerCount.adult}|${this.bookFlightData.passengerCount.children}|${this.bookFlightData.passengerCount.infant}|${moment().format("DD/MM/YY hh:mm:ss")}|${flightData.from.airportCode}`
            }
            else if (this.flightSelection == this.redeemFlightSelection) {
                var recentSearch = `${flightData.from.airportCode}|${flightData.to.airportCode}|R|${moment(flightData.schedule.start).format("DD/MM/YYYY")}-${moment(flightData.schedule.end).format("DD/MM/YYYY")}|${flightData.cabinDescription.cabinDescription.toLowerCase()}|${this.redeemFlightData.passengerCount.adult}|${this.redeemFlightData.passengerCount.children}|${this.redeemFlightData.passengerCount.infant}|${moment().format("DD/MM/YY hh:mm:ss")}|${flightData.from.airportCode}` 
            }
            else {
                var recentSearch = `${flightData.from.airportCode}|${flightData.to.airportCode}|R|${moment(flightData.schedule.start).format("DD/MM/YYYY")}-${moment(flightData.schedule.end).format("DD/MM/YYYY")}|${flightData.cabinDescription.cabinDescription.toLowerCase()}|${this.getTotalAdults(this.bookFlightHotelData.passengerCount)}|${this.getTotalChildren(this.bookFlightHotelData.passengerCount)}|${this.getTotalInfants(this.bookFlightHotelData.passengerCount)}|${moment().format("DD/MM/YY hh:mm:ss")}|${flightData.from.airportCode}`;
            }
            this.setCookie(<string>this.getAvailableRecentSearchCookieName(), recentSearch, 31);
        }
    }

    getAvailableRecentSearchCookieName() {
        for (var i = 1; i < 9; i++) {
            if (this.getCookie("CIB_SEARCH_HISTORY_" + i) == "") {
                return "CIB_SEARCH_HISTORY_" + i;
            }
        }
    }

    setCookie(cname: string, cvalue: string, exdays: number) {
        if (cname == undefined) {
            return;
        }
        var expires = "expires=" + moment().add(31, 'days').toDate();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    removeCookie(cname: string) {
        document.cookie = cname + "=; " + "expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    }

    removeAllCookie() {
        for (var i = 1; i < 9; i++) {
            document.cookie = "CIB_SEARCH_HISTORY_" + i + "=; " + "expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        }
    }

    getCookie(cname: string) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    checkCookie(cname: string): boolean {
        return this.getCookie(cname) != '' ? true : false
    }

    populateFromCookies() {
        for (var i = 1; i < 9; i++) {
            var currentCookie = this.getCookie("CIB_SEARCH_HISTORY_" + i);
            if (currentCookie != "") {
                var search_history: RecentSearchData = {
                    origin: currentCookie.split("|")[0].toString(),
                    destination: currentCookie.split("|")[1].toString(),
                    trip_type: currentCookie.split("|")[2],
                    departure: +moment(currentCookie.split("|")[3].split("-")[0].split("/")[1] + "/" + currentCookie.split("|")[3].split("-")[0].split("/")[0] + "/" + currentCookie.split("|")[3].split("-")[0].split("/")[2]),
                    arrival: +moment(currentCookie.split("|")[3].split("-")[1].split("/")[1] + "/" + currentCookie.split("|")[3].split("-")[1].split("/")[0] + "/" + currentCookie.split("|")[3].split("-")[1].split("/")[2]),
                    class: currentCookie.split("|")[4],
                    adults: parseInt(currentCookie.split("|")[5]),
                    children: parseInt(currentCookie.split("|")[6]),
                    infants: parseInt(currentCookie.split("|")[7]),
                    created_at: +moment(currentCookie.split("|")[8]),
                    updated_at: +moment(currentCookie.split("|")[8])
                }

                this.recentSearchLocalStorage.search_history_bookflights.push(search_history);
                this.recentSearchLocalStorage.search_history_redeemflights.push(search_history);
                this.recentSearchLocalStorage.search_history_bookflights_hotel.push(search_history);
            }
        }
    }

    checkForm(event: any) {
        console.log(this.bookFlightHotelData.passengerCount);
        if (validateAll(event)) {
            this.saveRecentSearch();
        }
        return false;
        event.preventDefault();
    }
}
