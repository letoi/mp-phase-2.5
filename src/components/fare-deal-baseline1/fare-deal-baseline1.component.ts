import Vue from 'vue';
import Component from 'vue-class-component';
import "./fare-deal-baseline1.component.scss";

import axios from "axios";

const $FareDealComponent = Vue.extend({
  data() {
    return {
      cities: '',
      viewMore: Object,
      isActive: false,
      city: 'SIN-Singapore',
      dropdownActiveItem: 0,
      inputCity: '',
      countries: Array,
      originCountries: Array,
      maxItem: 8,
      loaded: false
    }
  },

  computed: {
    splitCityName: function () {
      return this.city.split('-')[1];
    }
  },

  filters: {
    formatCityName: function(cityName:string) {
      return cityName.split('-')[1];
    }
  }
})

@Component({
  template: require('./fare-deal-baseline1.component.html'),
})

export default class FareDealComponent extends $FareDealComponent {
  mounted() {
    this.getData();
  }

  async getData() {
    var self = this;
    const response = await axios.get('assets/JSONS/fareDealsHomePage.json');

    setTimeout(function() {
      self.loaded = true;
    }, 100);

    self.originCountries = response.data.promoVO;
    self.countries = JSON.parse(JSON.stringify(self.originCountries));

    self.viewMore = response.data.viewMore;

    self.cities = self.cutOffArray(response.data.promoVO[0].cityVO);
    self.city = response.data.promoVO[0].city;

    return response.data;
  }

  toggleDropdown() {
    this.isActive = !this.isActive;
  }

  changeCity(city: string, index: number, country: any) {
    this.city = city;
    this.dropdownActiveItem = index;
    this.cities = this.cutOffArray(country[index].cityVO);

    this.toggleDropdown();
  }

  cutOffArray(array: any) {
    if(array.length < this.maxItem) {
      return array;
    }

    array.length = this.maxItem;
    return array;
  }

  filterCities(inputCity: string, countries: any) {
    this.countries = countries.filter((country: any) => country.city.toLowerCase().split('-')[1].indexOf(inputCity) > -1);
  }
}
