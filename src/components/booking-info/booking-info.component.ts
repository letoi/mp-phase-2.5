import Vue from "vue";
import Component from "vue-class-component";
import LightBoxComponent from "../light-box/light-box.component";
// import "./add-on-table.component.scss";
import axios from "axios";



const $BookingInfoComponent = Vue.extend({
    props: {
        bookinginfodata: Object
    },
    data(){
        return {

        }
    }
})

@Component({
    template: require("./booking-info.component.html"),
    components:{
        LightBoxComponent
    }
})

export default class BookingInfoComponent extends $BookingInfoComponent {

    mounted(){
    }
}