import Vue from "vue";
import Component from "vue-class-component";
import LightBoxComponent from "../light-box/light-box.component";
// import "./add-on-table.component.scss";
import axios from "axios";



const $FlightInfoComponent = Vue.extend({
    props: {
        flightinfodata: Object
    },
    data(){
        return {

        }
    }
})

@Component({
    template: require("./flight-info.component.html"),
    components:{
        LightBoxComponent
    }
})

export default class FlightInfoComponent extends $FlightInfoComponent {

    mounted(){
    }
}