import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./payment-tab.component.scss";

const PaymentTabData = Vue.extend({
    props: {
        tabs: Number,
        active: Number,
        id: String,
        bookinginfo: Object
    }
})

@Component({
    template: require("./payment-tab.component.html"),
    props: ['tabs', 'active', 'id']
})
export default class PaymentTabComponent extends PaymentTabData {
    activeTab: number = 1;
    mounted() {
        this.activeTab= this.active;
        for (let tab = 1; tab < this.tabs + 1; tab++) {
            var tabElm: Element = <Element>this.$slots['title-' + tab][0].elm;
            tabElm.addEventListener('click', () => {
                this.activeTab = tab;
            });
        }
    }

    toCurrency(string: any) {
            return string.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
      
    }

    getActiveTab(id: number) {
        return this.activeTab == id ? true : false;
    }
}
