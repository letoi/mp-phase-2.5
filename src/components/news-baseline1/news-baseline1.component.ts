import Vue from 'vue';
import Component from 'vue-class-component';
import "./news-baseline1.component.scss";

import axios from "axios";

const $NewsBaseline1Component = Vue.extend({
  data() {
    return {
      data: ''
    }
  }
})

@Component({
  template: require('./news-baseline1.component.html'),
  components: {

  }
})

export default class NewsBaseline1Component extends $NewsBaseline1Component {
  mounted() {
    this.getData();
  }

  async getData() {
    const response = await axios.get('assets/JSONS/news.json');

    this.data = response.data;

    return response.data;
  }
}
