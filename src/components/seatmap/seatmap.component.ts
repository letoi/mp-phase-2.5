import Vue from "vue";
import Component from "vue-class-component";
import "./seatmap.component.scss";
import SeatmapRowComponent from "./seatmap-row.component";
import SeatmapColumnComponent from "./seatmap-column.component";
import SeatComponent from "./seat.component";
import SeatSelectionData from "../../models/seat-selection-data";

const $SeatmapComponent = Vue.extend({
    props: {
        seatmapdata: Object,
        dictionary: Object,
        value: SeatSelectionData,
    }
})

@Component({
    template: require("./seatmap.component.html"),
    props: ['seatmapdata', 'dictionary'],
    components: {
        SeatmapRowComponent,
        SeatmapColumnComponent,
        SeatComponent
    }
})

export default class SeatmapComponent extends $SeatmapComponent {
    mounted() {
        let ref: any = this.$refs.seatmapWrapper;
        console.log();
        let scrollCenterPosition = (this.$el.querySelector('.seatmap_compartment--control')!.scrollWidth - this.$el.querySelector('.seatmap_compartment--control')!.clientWidth) / 2;
        setTimeout(() => {
            this.$el.querySelector('.seatmap_compartment--control')!.scrollTo(scrollCenterPosition, 0);
        }, 800);
        // ref.style.transform = 'translateX(-28%)';
    }

}
