import Vue from "vue";
import Component from "vue-class-component";

const $SeatmapColumnComponent = Vue.extend({
    props: {
        columndata: Object,
        rowdata: Object,
        dictionary: Object,
        rowid: Number,
        colid: Number
    }
})

@Component({
    template: require("./seatmap-column.component.html"),
    props: ['columndata', 'rowdata', 'dictionary', 'rowid', 'colid'],
    components: {
    }
})

export default class SeatmapColumnComponent extends $SeatmapColumnComponent {
    mounted() { 
        // console.log(this.rowdata);
     }
}