import Vue from "vue";
import Component from "vue-class-component";

const $SeatmapRowComponent = Vue.extend({
    props: {
        rowdata: Object,
        compartment: Object,
        dictionary: Object,
        rowid: Number
    }
})

@Component({
    template: require("./seatmap-row.component.html"),
    props: ['rowdata', 'compartment', 'dictionary', 'rowid'],
    components: {
    }
})

export default class SeatmapRowComponent extends $SeatmapRowComponent {
    mounted() { 
        
    }
}