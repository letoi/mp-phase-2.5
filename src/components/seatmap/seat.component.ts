import Vue from "vue";
import Component from "vue-class-component";
import VTooltip from 'v-tooltip';
import "./seatmap.component.scss";
import SeatSelectionData from "../../models/seat-selection-data";
import LightBoxComponent from "../light-box/light-box.component";
// import { directive as onClickaway } from 'vue-clickaway';


declare var _: any;
Vue.use(VTooltip, {
    popover: {
        defaultPopperOptions: {
            modifiers: {
                preventOverflow: {
                    padding: 0
                },
                flip: {
                    enabled: false
                },
                defaultPlacement: 'bottom'
            }
        },
    }
});



const $SeatComponent = Vue.extend({
    // directives: {
    //     onClickaway: onClickaway
    // },
    props: {
        seatdata: Object,
        seatno: Number,
        value: SeatSelectionData,
        dictionary: Object,
        column: Object,
        columnLength: Number,
        columnIndex: Number,
        rowIndex: Number
    },
    // data() {
    //     return {
    //         closeTooltipBoolean: true
    //     }
    // }
})

@Component({
    template: require("./seat.component.html"),
    props: ['seatdata', 'seatno', 'selectedPassenger'],
    components: {
        VTooltip,
        LightBoxComponent
    }
})

export default class SeatComponent extends $SeatComponent {
    openPopover: string = "";
    showEmergencyModal: string = "";
    tempAddon:object = {};
    addonIds: Array<string> = [];
    detailsStatus: boolean = false;
    proceed: boolean = false;
    seatDetailsFromPricing: object = {};
    windowWidth : number = 0;

    mounted() {
        VTooltip.options.defaultContainer = "seatmap_compartment";
    }
    
    created() {
        let self = this;
        self.windowWidth = document.documentElement.clientWidth; // On Load Initial

		this.$nextTick(function () {
			window.addEventListener('resize', function() {
                self.windowWidth = document.documentElement.clientWidth;
			});
		});
    }

    openModal(id: string){
		this.$forceUpdate();
        this.showEmergencyModal = id;
	}

	closeModal(seatno: string) {
        this.openPopover = seatno;
		this.resetData();
	}

    showDetails(){
        this.detailsStatus = !this.detailsStatus;
    }

    isPassengerSeat(seat: string): boolean{
        return _.find(this.value.flight.updatedPassengerData, (passenger: any) => {
            return (this.value.selectedSeats && passenger.seat.seatNo == seat && passenger.seat.isSelected && this.value.flight.updatedPassengerData[this.value.selectedId].seat.seatNo != seat)
        }) ? true : false;
    }

    isSelected(seat: string): boolean {
        return _.find(this.value.flight.updatedPassengerData, (passenger: any) => {
            return (this.value.selectedSeats && passenger.seat.seatNo == seat && passenger.seat.isSelected)
        }) ? true : false;
    }

    getPassengerId(seat: string) {
        return _.find(this.value.flight.updatedPassengerData, (passenger: any) => {
            return (this.value.selectedSeats && passenger.seat.seatNo == seat && passenger.seat.isSelected)
        });
    }

    isAddonSelected(addon: object, seat: string): boolean{
        return this.value.flight.updatedPassengerData[this.value.selectedId].seat.selectedAddon == addon && this.value.flight.updatedPassengerData[this.value.selectedId].seat.seatNo == seat ? true: false;
    }

    unselectSeat(seat: string) {
        if (this.value.flight.updatedPassengerData[this.value.selectedId].seat.seatNo == seat) {
            this.value.flight.updatedPassengerData[this.value.selectedId].seat = {};
            this.value.flight.updatedPassengerData[this.value.selectedId].seatSelected = "";
            this.value.flight.updatedPassengerData[this.value.selectedId].seatPrice = "";
            this.value.flight.updatedPassengerData[this.value.selectedId].seatType = "";
            this.value.flight.updatedPassengerData[this.value.selectedId].seatTypeWord = "";
            this.value.selectedSeats.pop();
            this.$emit("input", this.value);
            console.log("im here");
        }
        this.resetData();
    }

    selectSelectedSeat(index: number){
        this.value.selectedId = index;
        this.$emit("input", this.value);
    }

    getSeatCharacteristicsType(seatCharacteristic: string):object{
        let returnObj = {
            state:false,
            strongText:"",
            normalText:""
        }

        if(seatCharacteristic.indexOf('E') != -1 && seatCharacteristic.indexOf('W') != -1){
            returnObj.state = true;
            returnObj.strongText = "This is a windowless emergency exit seat (cabin door protrudes).";
            returnObj.normalText = "Not available for these passengers: Pregnant, under 15 years, infants, and/or require special assistance."
        }else if(seatCharacteristic.indexOf('E') != -1){
            returnObj.state = true;
            returnObj.strongText = "This is an emergency exit seat."; 
            returnObj.normalText = "Not available for these passengers: Pregnant, under 15 years, infants, and/or require special assistance."
        }else if(seatCharacteristic.indexOf('W') != -1){
            returnObj.state = true;
            returnObj.strongText = "This is a windowless seat.";
        }

        return returnObj;
    }

    selectSeat(seatNumber: string,addAddon: object = {}) {
        
        // if (this.seatdata.characteristics.toUpperCase().indexOf('E') != -1) {
        //     this.tempAddon = addAddon;
        //     this.showEmergencyModal = "3";
        // } else {
            this.saveSeat(seatNumber, addAddon);
        // }
    }

    saveSeat(seatNumber: string, addAddon: object = {}) {
        this.showEmergencyModal = "";
        this.value.flight.updatedPassengerData[this.value.selectedId].seat = this.seatdata;
        this.value.flight.updatedPassengerData[this.value.selectedId].seat.isSelected = true;
        this.value.flight.updatedPassengerData[this.value.selectedId].seat.seatNo = seatNumber;
        this.value.flight.updatedPassengerData[this.value.selectedId].seatSelected = seatNumber;
        this.value.flight.updatedPassengerData[this.value.selectedId].seatPrice = this.getSeatDetails() ? this.getSeatDetails().amount : "";
        this.value.flight.updatedPassengerData[this.value.selectedId].seatType = this.seatdata.zone;
        this.value.flight.updatedPassengerData[this.value.selectedId].seatTypeWord = this.getSeatType();
        if(!_.isEmpty(addAddon)){
            this.value.flight.updatedPassengerData[this.value.selectedId].seat.selectedAddon = addAddon;
        }else{
            this.value.flight.updatedPassengerData[this.value.selectedId].seat.selectedAddon = {};
        }
        this.value.selectedSeats.push(seatNumber);
        this.$root.$emit("input", this.value);
        this.changeActivePassenger(this.value.selectedId);
        // let position = window.pageYOffset
        // window.scroll(0,1);
        // window.scroll({
        //     top: position,
        //     behavior: "smooth"
        //   });
        // this.$root.$el.querySelector('.seatmap_submit--mobile');
    }

    changeActivePassenger(id: number) {
        this.value.flight.updatedPassengerData.length - 1 == id ? this.value.selectedId = 0 : this.value.selectedId++;
        this.$parent.$emit("input", this.value);
    }

    getSeatType(): string {
        var seat = "Standard";
        if (this.seatdata.zone) {
            seat = this.dictionary.seatType[this.seatdata.zone].toLowerCase() == "preferred" ? "Extra Legroom" : this.dictionary.seatType[this.seatdata.zone];
        }
        return seat;
    }

    getSeatDetails( index: number = this.value.selectedId): any {
       return  this.seatDetailsFromPricing = _.find(this.value.flight.updatedPassengerData[index].seatPricing, (pricing: any) => {
            if(pricing.type == this.seatdata.zone){
                if(pricing.amount > 0){
                    pricing.amountString = pricing.currency+" "+pricing.amount;
                }else{
                    pricing = {};
                }
                return pricing;
            }
        }) || {};
    }

    getAddOnIds(zone: string): any {
        return _.find(this.value.flight.updatedPassengerData[this.value.selectedId].eligibleAddons, (addOnType: any) => {
            if (addOnType.addonType == zone) {
                return addOnType;
            }
        });
    }

    getAddOnDetails(): Array<any> {
        let addonDetails: Array<any> = new Array<any>();
        _.each(this.getAddOnIds(this.seatdata.zone).addonIds || [], (addonIdObject: any) => {
            _.find(this.value.addonDetails, (addons: any) => {
                if (addons.addonId == addonIdObject.addonId) {
                    addons.price = addonIdObject.price;
                    addons.originalPrice = addonIdObject.originalPrice;
                    addons.currency = addonIdObject.currency;
                    addons.percentageSaved = addonIdObject.percentageSaved;
                    addonDetails.push(addons);
                    return;
                }
            });
        });
        return addonDetails;
    }

    removeAddons(){
        this.value.flight.updatedPassengerData[this.value.selectedId].seat.selectedAddon = {};
        this.value.selectedId++;
        this.value.selectedId--;
        this.$parent.$emit("input", this.value);
    }

    getFlightSegmentDetails(boundId: string): any {
        return this.value.allFlightSegmentDetails[parseInt(boundId)];
    }

    resetData(){
        this.tempAddon = {};
        this.showEmergencyModal = "";
        this.detailsStatus = false;
        this.proceed = false;
    }

    selectPassenger(index: number) {
        let self: any = this;
        this.value.selectedId = index;
        this.$emit('input', this.value);
        this.openModal("6");
    }

    isEmptyAddon(addon:object){
        return _.isEmpty(addon);
    }
}
