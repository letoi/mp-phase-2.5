import { Component, Vue, Prop } from "vue-property-decorator";

declare var _: any;

@Component
export default class PassengerCountBaseComponent extends Vue {
    @Prop(Object) value!: any;
    @Prop(Object) cabin!: any;
    limit: number = 9;
    childrenRatio: number = 5;
    lastCabinSelected: any = {
        cabinId: ""
    };

    state: boolean = false;

    openState() {
        this.state = true;
    }

    passengerAdd(passenger: any, key: string) {
        passenger[key] ++;
        this.$emit("input", this.value);
    }

    passengerRemove(passenger: any, key: string) {
        if (key == "adult" && passenger.adult == 1) return;
        if (!passenger[key]) return;

        passenger[key] --;
        this.$emit("input", this.value);
    }

    isDisabled(key: string) {
        let passengerTotal = this.getPassengerTotal();
        if (passengerTotal == this.limit) return true;

        if (key == "children" &&
            (this.value.children + 1) > (this.value.adult * this.childrenRatio)
        ) {
            return true;
        }

        if (key == "infant" &&
            (this.value.infant + 1) > (this.value.adult)
        ) {
            return true;
        }

        return false;
    }

    isDisabledMinus(key: string) {
        if (key == "adult" && this.value.adult == 1) {
            return true;
        }

        if (key == "children") {
            return !this.value.children;
        }

        if (key == "infant") {
            return !this.value.infant;
        }
    }

    getPassengerTotal() {
        return this.value.adult + this.value.children + this.value.infant;
    }

    get getPassengerCountLabel() {
        let label = [];
        let adult = this.value.adult,
            children = this.value.children,
            infant = this.value.infant;
        if (adult)  {
            label.push(`${adult} ${adult > 1 ? 'Adults' : 'Adult'}`);
        }

        if (children) {
            label.push(`${children} ${children > 1 ? 'Children' : 'Child'}`);
        }

        if (infant) {
            label.push(`${infant} ${infant > 1 ? 'Infants' : 'Infant'}`);
        }

        return label.join(", ");
    }

    getRange(rangeLength: number, startAt: number = 0) {
        return _.range(rangeLength).map((i: number) => i + startAt);
    }

    closeState() {
        this.state = false;
    }
}