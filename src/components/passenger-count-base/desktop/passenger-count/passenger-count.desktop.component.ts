import { Component, Watch } from 'vue-property-decorator';
import { directive as onClickaway } from 'vue-clickaway';
import './passenger-count.desktop.component.scss';
import {PassengerCountData} from '../../../../models/passenger-count-data';
import AutocompleteComponent from '../../../autocomplete/autocomplete.component';
import { Constants } from '../../../../models/constants';
import PassengerCountBaseComponent from '../../base/passenger-count.base.component';
import { Prop } from 'vue-property-decorator';


@Component({
    template: require('./passenger-count.desktop.component.html'),
    components: {
        'v-autocomplete': AutocompleteComponent
    },
    directives: {
        onClickaway
    }
})
export default class PassengerCountDesktopComponent extends PassengerCountBaseComponent {
    @Prop(PassengerCountData) value!: PassengerCountData;

    @Watch('cabin')
    onCabinChange(cabinValue: any) {
        this.limit = Constants.passengerCountDictionary[cabinValue.cabinId];
            if (this.lastCabinSelected.cabinId != cabinValue.cabinId) {
                this.lastCabinSelected = this.cabin;
                this.$emit("input", new PassengerCountData());
            }
    }
}