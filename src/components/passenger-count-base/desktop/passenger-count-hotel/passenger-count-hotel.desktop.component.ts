import { Component, Watch } from 'vue-property-decorator';
import { directive as onClickaway } from 'vue-clickaway';
import './passenger-count-hotel.desktop.component.scss';
import { PassengerCountHotelData, ChildrenCountData, RoomCountData } from '../../../../models/passenger-count-data';
import AutocompleteComponent from '../../../autocomplete/autocomplete.component';
import { Constants } from '../../../../models/constants';
import PassengerCountBaseComponent from '../../base/passenger-count.base.component';
import { Prop } from 'vue-property-decorator';
import { RequiredModel } from '../../../../directive/validation.directive';

@Component({
    template: require('./passenger-count-hotel.desktop.component.html'),
    components: {
        'v-autocomplete': AutocompleteComponent
    },
    directives: {
        onClickaway,
        requiredModel: RequiredModel
    }
})
export default class PassengerCountHotelDesktopComponent extends PassengerCountBaseComponent {
    @Prop([RoomCountData, Object]) value!: RoomCountData;

    @Watch('cabin')
    onCabinChange(cabinValue: any) {
        this.limit = Constants.passengerCountDictionary[cabinValue.cabinId];
        if (this.lastCabinSelected.cabinId != cabinValue.cabinId) {
            this.lastCabinSelected = this.cabin;
            this.$emit("input", new PassengerCountHotelData());
        }
    }

    roomCount: RoomCountData = new RoomCountData();
    expandedRoomIndex: number = 0;

    mounted() {
        this.addRoom();
    }

    passengerAddChildren(passenger: PassengerCountHotelData) {
        let newChild = new ChildrenCountData();
        passenger.children.push(newChild);
        this.$emit("input", this.roomCount);
    }

    passengerRemoveChildren(passenger: PassengerCountHotelData) {
        passenger.children.pop();
        this.$emit("input", this.roomCount);
    }

    addRoom() {
        let newRoom = new PassengerCountHotelData();
        if (this.roomCount.room.length < 3) this.roomCount.room.push(newRoom);
        this.expandedRoomIndex = this.roomCount.room.length - 1;
        this.$emit("input", this.roomCount);
    }

    removeRoom(roomIndex: number) {
        this.roomCount.room.splice(roomIndex, 1);
        this.$emit("input", this.roomCount);
    }

    getTotalAdults(): number {
        return this.roomCount.room.reduce((prev: number, current: PassengerCountHotelData) => {
            return current.adult + prev
        }, 0);
    }

    getTotalChildren(): number {
        return this.roomCount.room.reduce((prev: number, current: PassengerCountHotelData) => {
            return current.children.length + prev
        }, 0);
    }

    getTotalInfants(): number {
        return this.roomCount.room.reduce((prev: number, current: PassengerCountHotelData) => {
            return current.infant + prev
        }, 0);
    }

    getIfRoomIsExpanded(roomIndex: number): boolean {
        return roomIndex == this.expandedRoomIndex ? true : false;
    }

    expandRoomDetails(roomIndex: number) {
        this.expandedRoomIndex = roomIndex;
    }

    doneAddingBookHotelDetails() {
        this.expandedRoomIndex = this.roomCount.room.length - 1;
        this.closeState();
        this.$emit("input", this.roomCount);
    }

    isDisabledHotelMinus(key: string, room: PassengerCountHotelData) {
        if (key == "adult" && room.adult == 1) {
            return true;
        }

        if (key == "children") {
            return !room.children;
        }

        if (key == "infant") {
            return !room.infant;
        }
    }

    get getPassengerCountLabel() {
        let label = [];
        let adult = this.getTotalAdults(),
            children = this.getTotalChildren(),
            infant = this.getTotalInfants(),
            room = this.roomCount.room.length;
        if (adult)  {
            label.push(`${adult} ${adult > 1 ? 'Adults' : 'Adult'}`);
        }

        if (children) {
            label.push(`${children} ${children > 1 ? 'Children' : 'Child'}`);
        }

        if (infant) {
            label.push(`${infant} ${infant > 1 ? 'Infants' : 'Infant'}`);
        }

        if (room) {
            label.push(`${room} ${room > 1 ? 'Rooms' : 'Room'}`);
        }

        return label.join(", ");
    }

}