import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./addon-upsell.component.scss";

declare var pageData: any;
const AddonUpsellData = Vue.extend({
	props: {
		upsellinfo: Object
	},
    data() {
        return {
        	pageData
        }
    }
})

@Component({
    template: require("./addon-upsell.component.html"),
})

export default class AddonUpsellComponent extends AddonUpsellData {
    mounted() {
    }
}
