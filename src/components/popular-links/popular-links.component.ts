import Vue from "vue";
import Component from "vue-class-component";
import axios from "axios";

import "./popular-links.component.scss";

const $PopularLinksComponent = Vue.extend({
	data() {
		return {
			popularLinks: Array
		};
	}
});

@Component({
	template: require("./popular-links.component.html"),
	components: {}
})
export default class PopularLinksComponent extends $PopularLinksComponent {
	mounted() {
		this.getData();
	}

	async getData() {
		const response = await axios.get("assets/JSONS/popular-links.json");

		this.popularLinks = response.data;

		return response.data;
	}
}
