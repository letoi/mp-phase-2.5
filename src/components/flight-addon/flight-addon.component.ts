import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./flight-addon.component.scss";
import LightBoxComponent from "../../components/light-box/light-box.component";

const FlightAddonData = Vue.extend({
    props: {
        flightaddoninfo: Object
    },
    data() {
        return {
        }
    }
})

@Component({
    template: require("./flight-addon.component.html"),
    components: {
    	LightBoxComponent
    }
})

export default class FlightAddonComponent extends FlightAddonData {

	showRemoveDeal: string = '0';
    

    mounted() {
    }

    openModal(id: string){
        this.showRemoveDeal = id;
    }

    closeModal() {
        this.showRemoveDeal = '0';
    }
}
