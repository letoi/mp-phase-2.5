import Vue from 'vue';
import Component from 'vue-class-component';
import axios from 'axios';

import './sign-up.component.scss';

const $SignUpComponent = Vue.extend({
  data() {
    return {
      signUp: ''
    }
  }
})

@Component({
  template: require('./sign-up.component.html')
})

export default class SignUpComponent extends $SignUpComponent {
  mounted() {
    this.getData();
  }

  async getData() {
    const response = await axios.get('assets/JSONS/signUp.json');

    this.signUp = response.data;

    return response.data;
  }
}
