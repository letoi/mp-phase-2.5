import Vue from "vue";
import Component from "vue-class-component";
import "./progress-bar.component.scss";

declare const _: any;

const $ProgressBarComponent = Vue.extend({
    props: {
        items: {
            type: [Object, Array],
            default: []
        }
    }
})

@Component({
    template: require("./progress-bar.component.html")
})

export default class ProgressBarComponent extends $ProgressBarComponent {
    mounted() { /* console.log(this.items) */ }
}