import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./trip-addons.component.scss";
import LightBoxComponent from "../../components/light-box/light-box.component";

const TripAddonsData = Vue.extend({
    props: {
        tripaddoninfo: Object
    },
    data() {
        return {
        }
    }
})

@Component({
    template: require("./trip-addons.component.html"),
    components: {
    	LightBoxComponent
    }
})

export default class TripAddonsComponent extends TripAddonsData {

	showRemoveAddOns: string = '0';
    

    mounted() {
    }

    openModal(id: string){
        this.showRemoveAddOns = id;
    }

    closeModal() {
        this.showRemoveAddOns = '0';
    }
}
