import Vue from 'vue';
import Component from 'vue-class-component';
import VueSimpleSuggest from 'vue-simple-suggest';
import './autocomplete.component.scss';
import '../../extenders/element-extender';

declare var _: any;
const $AutocompleteComponent = Vue.extend({
    props: {
        minLength: {
            type: Number,
            default: 0
        },
        disabled: {
            type: Boolean,
            default: false
        },
        max: {
            type: Number,
            default: 0
        },
        items: {
            type: [Object, Array],
            default: () => {}
        },
        value: {
            type: [String, Object, Number, Date, Array],
            default: () => []
        },
        mode: {
            type: String,
            default: "input"
        },
        valueAttribute: {
            type: String,
            default: "id"
        },
        displayAttribute: {
            type: String,
            default: "title"
        },
        debounce: {
            type: Number,
            default: 100
        }
    },
    data() {
        return {
            item: ""
        }
    },
    watch: {
        value: function (value) {
            const autocomplete: any = this.$refs.autocomplete;
            this.item = value;
            autocomplete.selected = value;
        }
    }
});

@Component({
    template: require('./autocomplete.component.html'),
    components: {
        VueSimpleSuggest
    }
})
export default class AutocompleteComponent extends $AutocompleteComponent {
    searchInput: any;
    hadSelected: boolean = true;

    mounted() {
        let inputWrapper = this.$el.querySelectorAll(".input-wrapper")[0];
        inputWrapper.removeClass("input-wrapper");
        inputWrapper.addClass("input_wrapper");
        this.searchInput = this.$el.querySelectorAll(".ref--searchinput")[0];
		this.item = this.value;

		this.setWriteables();
    }

    customFiltering(list: any, search: string) {
        let regEx = new RegExp(search, "i");
        if (typeof list == "object") {
            return regEx.test(list[this.displayAttribute]);
        } else {
            return regEx.test(list);
        }
	}

	setWriteables() {
		let writeables = this.$el.querySelectorAll(".writeable");

		if (writeables) {
			for (let i = 0; i < writeables.length; i++) {
				writeables[i].removeAttribute("readonly");
			}
		}
	}

    onFocus(evt: any) {
        const autocomplete: any = this.$refs.autocomplete;
        autocomplete.suggestions = this.items;
		this.$el.querySelectorAll(".input_wrapper")[0].addClass("open");

		this.setWriteables();
    }

    onBlur(evt: any) {
		this.$el.querySelectorAll(".input_wrapper")[0].removeClass("open");
    }

    onSelect() {
		this.setData();
    }

    onHideSelect() {
        this.$nextTick(() => {
			this.searchInput.blur();
		});
    }

    setData() {
        const autocomplete: any = this.$refs.autocomplete;
        this.item = (_.cloneDeep(autocomplete.selected) || this.value);
        this.hadSelected = (autocomplete.selected) ? true : false;
        this.$emit("input", this.item);
        this.$nextTick(() => {
            this.$emit('select-done');
        });
    }
}
