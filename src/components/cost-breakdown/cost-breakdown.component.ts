import Vue, { VNode } from "vue";
import Component from "vue-class-component";
import "./cost-breakdown.component.scss";
import VTooltip from 'v-tooltip';

Vue.use(VTooltip, {
    popover: {
        defaultPopperOptions: {
            modifiers: {
                preventOverflow: { padding: 0 },
                defaultPlacement: 'bottom'
            },
        },
    }
});

const CostBreakdownData = Vue.extend({
    props: {
        costinfo: Object
    },
    data() {
        return {
        }
    }
})

@Component({
    template: require("./cost-breakdown.component.html")
})

export default class CostBreakdownComponent extends CostBreakdownData {    

    mounted() {
    }

}
