import { Vue, Component, Watch, Prop } from 'vue-property-decorator';
import VueRangedatePicker from 'vue-rangedate-picker';
import { directive as onClickaway } from 'vue-clickaway';
import { dateParser } from "../../mixins/utilities";
import * as moment from 'moment';
import './v-datepicker.component.scss';
import '../../assets/styles/molecules/calendar.scss';

const VDatepickerComponentExtend = Vue.extend({
    directives: {
        onClickaway: onClickaway
    },
    data: VueRangedatePicker.data,
    props: VueRangedatePicker.props,
    computed: VueRangedatePicker.computed,
    methods: VueRangedatePicker.methods,
    created: VueRangedatePicker.created,
    watch: VueRangedatePicker.watch,
    template: require('./v-datepicker.component.html')
});

@Component({
    extends: VDatepickerComponentExtend
})
export default class VDatepickerComponent extends Vue {
    _self: any;
    startDate: string = "";
    endDate: string = "";
    oneWay: boolean = false;
    inputFocus: string = "start";
    startRef: any;
    endRef: any;
    backEnabled: boolean = false;
    @Prop(Object) value!: any;
    @Prop({ type: Boolean, default: false }) isHistoGramEnabled!: boolean;

    beforeCreate() {
        this.$options.computed!.startNextMonthDay = (() => {
            if (this._self.activeMonthStart === 11) {
                return new Date(this._self.activeYearStart + 1, this._self.startNextActiveMonth, 1).getDay();
            } else {
                return new Date(this._self.activeYearStart, this._self.startNextActiveMonth, 1).getDay();
            }
        }).bind(this);
    }

    mounted() {
        this._self = this.$vnode.componentInstance;
        this.startRef = this.$el.querySelectorAll(".ref--start")[0];
        this.endRef = this.$el.querySelectorAll(".ref--end")[0];

        if (this.value) {
            this.onRangeWatchChange(this.value);
        }
    }

    openCalendar(position: string, evt: any) {
        evt.target.closest(".input_wrapper").addClass("open");
        this.inputFocus = position;
        this.setCalendarState(true);
    }

    goPrevMonth() {
        const prevMonth = new Date(this._self.activeYearStart, this._self.activeMonthStart, 0)
        this._self.activeMonthStart = prevMonth.getMonth();
        this._self.activeYearStart = prevMonth.getFullYear();
        this._self.activeYearEnd = prevMonth.getFullYear();

        this.checkIfDisableBack();
    }

    goNextMonth() {
        const nextMonth = new Date(this._self.activeYearEnd, this._self.startNextActiveMonth, 1)
        this._self.activeMonthStart = nextMonth.getMonth()
        this._self.activeYearStart = nextMonth.getFullYear()
        this._self.activeYearEnd = nextMonth.getFullYear()

        this.checkIfDisableBack();
    }

    checkIfDisableBack() {
        if (this._self.activeMonthStart == moment().toDate().getMonth() &&
            this._self.activeYearStart == moment().toDate().getFullYear()
        ) {
            this.backEnabled = false;
        } else {
            this.backEnabled = true;
        }
    }

    blurFocus(evt: any) {
        evt.target.closest(".input_wrapper").removeClass("open");
    }

    closeCalendar() {
        this.setCalendarState(false);
    }

    setCalendarState(state: Boolean) {
        this._self.isOpen = state;
        this._self.showMonth = state;
        return
    }

    isDateDisabled(r: number, i: number, startMonthDay: number, endMonthDate: number, month: number, year: number) {
        const result = this._self.getDayIndexInMonth(r, i, startMonthDay)
        let thisDate = `${year}-${month + 1}-${result}`;
        let currentDate = new Date();
        let currentDateString = `${currentDate.getFullYear()}-${currentDate.getMonth() + 1}-${currentDate.getDate()}`;
        if (result > 0 && result <= endMonthDate) {
            let isSameDate = thisDate == currentDateString;
            let differenceResult = moment(thisDate).isBefore(currentDateString) && !isSameDate;
            return (differenceResult ? ['calendar_days--disabled', 'calendar_days--invalid'] : []);
        }

        return (!(result > 0 && result <= endMonthDate) ? ['calendar_days--disabled'] : []);
    }

    onOneWay() {
        this._self.dateRange.end = "";
        this.endDate = "";
    }

    resetDate() {
        this._self.dateRange = {
            start: "",
            end: ""
        };
        this.startDate = "";
        this.endDate = "";
        this.startRef.focus();
    }

    selectFirstItem(r: number, i: number) {
        const result = this._self.getDayIndexInMonth(r, i, this._self.startMonthDay) + 1;
        this._self.dateRange = this.getNewDateRange(result, this._self.activeMonthStart, this._self.activeYearStart);
        if (this.inputFocus == "start") {
            this.endRef.focus();
        }
        if (this._self.dateRange.start && this.oneWay) {
            this._self.isOpen = this._self.showMonth = false;
            this._self.presetActive = '';
        }
        if (this._self.dateRange.start && this._self.dateRange.end) {
            this._self.isOpen = this._self.showMonth = false;
            this._self.presetActive = '';
        }
    }

    selectSecondItem(r: number, i: number) {
        const result = this._self.getDayIndexInMonth(r, i, this._self.startNextMonthDay) + 1;
        this._self.dateRange = this.getNewDateRange(result, this._self.startNextActiveMonth, this._self.activeYearEnd);
        if (this.inputFocus == "start") {
            this.endRef.focus();
        }
        if (this._self.dateRange.start && this.oneWay) {
            this._self.isOpen = this._self.showMonth = false;
            this._self.presetActive = '';
        }
        if (this._self.dateRange.start && this._self.dateRange.end) {
            this._self.isOpen = this._self.showMonth = false;
            this._self.presetActive = ''
        }
    }

    getNewDateRange(result: number, activeMonth: number, activeYear: number) {
        const resultDate = new Date(activeYear, activeMonth, result);
        if (!this._self.dateRange.start ||
            resultDate <= this._self.dateRange.start ||
            (this.inputFocus == "start" && this._self.dateRange.end)
        ) {
            return { start: resultDate };
        } else if (!this._self.dateRange.end ||
            resultDate > this._self.dateRange.start
        ) {
            let startMoment = moment(this._self.dateRange.start);
            let endMoment = moment(resultDate);
            let daysDifference = Math.abs(startMoment.diff(endMoment, 'days'));
            if (this.oneWay) {
                return {
                    start: resultDate
                };
            }
            if (daysDifference < 4) {
                return {
                    start: this._self.dateRange.start
                };
            }
            return {
                ...this._self.dateRange,
                end: resultDate
            };
        }
    }

    @Watch('rangeWatch')
    onRangeWatchChange(value: any) {
        let {start, end} = value;
        this._self.dateRange = {
            start: start ? moment(start).add(1, "day").toDate() : "",
            end: end ? moment(end).add(1, "day").toDate() : ""
        };
        this.oneWay = value.oneWay;
    }

    @Watch('dateRange')
    onDateRangeChange(value: any) {
        let valueStart = moment(value.start).subtract(1, 'day').toDate();
        let valueEnd = moment(value.end).subtract(1, 'day').toDate();
        let inputData: any = {
            oneWay: this.oneWay
        };

        if (value.start) {
            this.startDate = dateParser(valueStart);
            inputData.start = valueStart;
        } else {
            this.startDate = "";
        }

        if (value.end) {
            this.endDate = dateParser(valueEnd);
            inputData.end = valueEnd;
        } else {
            this.endDate = "";
        }
        
        this.$emit("input", inputData);
    }
    
    @Watch('oneWay')
    onOneWayChanged(value: boolean) {
        let inputData = this.value;
        inputData.oneWay = value;
        this.$emit('input', inputData);
    }

    getDayCellOverride(getDayCellFn: Function, r: any, i: any, startNextMonthDay: any, endNextMonthDate: any, isDateDisabled: Function,  activeMonthStart: any, activeYearStart: any) {
		let disabledClasses = isDateDisabled(r, i, startNextMonthDay, endNextMonthDate, activeMonthStart, activeYearStart);
		let dayCell = getDayCellFn(r, i, startNextMonthDay, endNextMonthDate);
        return this.isHistoGramEnabled && disabledClasses!.length == 0 ? (dayCell + this.getHistogramHtml(dayCell, 1499)) : dayCell;
    }

    getHistogramHtml(daycell: any, value: number, isHighlighted: boolean = false): string {
        if (daycell == '' || daycell.toString().toLowerCase() == '&nbsp;') {
            return "";
        } else {
            return `<span class='calendar-histogram-details'>${value}</span>`;
        }
    }
}
