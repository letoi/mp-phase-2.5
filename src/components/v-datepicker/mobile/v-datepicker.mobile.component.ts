import VDatepickerComponent from "../v-datepicker.component";
import { Prop, Component } from 'vue-property-decorator';
import * as moment from "moment";
import "./v-datepicker.mobile.component.scss";

declare var _: any;

@Component({
    template: require('./v-datepicker.mobile.component.html')
})
export default class VDatepickerMobileComponent extends VDatepickerComponent {
    @Prop({ default: 5 }) monthRange!: number;
    showMonthsNumber: Array<number> = _.range(this.monthRange);

    activeMonthStart$(monthStart: number, idx: number) {
        return (monthStart + idx) % 12;
    }

    activeYearStart$(monthStart: number, yearStart: number, idx: number) {
        let calculatedMonths = monthStart + idx;
        if (calculatedMonths >= 12) yearStart += parseInt((calculatedMonths / 12).toString());
        return yearStart;
    }

    startMonthDay$(idx: number) {
        let calculatedMonths = this._self.activeMonthStart + idx;
        let activeMonth = this.activeMonthStart$(this._self.activeMonthStart, idx);
        let additionalDay = 0;
        if (calculatedMonths >= 12) additionalDay += parseInt((calculatedMonths / 12).toString());
        return new Date(this._self.activeYearStart, activeMonth, 1).getDay() + additionalDay;
    }

    endMonthDate$(idx: number) {
        return new Date(this._self.activeYearStart, this.activeMonthStart$(this._self.activeMonthStart, idx) + 1, 0).getDate();
    }

    isDateSelected(r: number, i: number, activeMonth: number, activeYear: number) {
        let calendarDay: any = this.$refs[`calendarDay_${activeMonth}_${activeYear}_${r}_${i}`];

        if (!calendarDay) return false;

        let calendarDate = parseInt(calendarDay[0].innerHTML);

        if (!this._self.dateRange.start && !this._self.dateRange.end) return false;
        if (isNaN(calendarDate)) return false;

        let currentDate = moment(`${activeYear}-${activeMonth + 1}-${calendarDate}`);
        let startDate = (this._self.dateRange.start) ? moment(this._self.dateRange.start).subtract(1, "day").toDate() : null;
        let endDate = (this._self.dateRange.end) ? moment(this._self.dateRange.end).subtract(1, "day").toDate() : null;
        if (startDate) {
            let isSame = currentDate.isSame(startDate);
            if (isSame) return true;
        }
        
        if (endDate) {
            let isSame = currentDate.isSame(endDate);
            if (isSame) return true;
        }

        return false;
    }

    isDateInRange(r: number, i: number, activeMonth: number, activeYear: number) {
        let calendarDay: any = this.$refs[`calendarDay_${activeMonth}_${activeYear}_${r}_${i}`];

        if (!calendarDay) return false;

        let calendarDate = parseInt(calendarDay[0].innerHTML);

        if (!this._self.dateRange.start || !this._self.dateRange.end) return false;
        if (isNaN(calendarDate)) return false;

        let currentDate = moment(`${activeYear}-${activeMonth + 1}-${calendarDate}`);
        let startDate = (this._self.dateRange.start) ? moment(this._self.dateRange.start).subtract(1, "day").toDate() : null;
        let endDate = (this._self.dateRange.end) ? moment(this._self.dateRange.end).subtract(1, "day").toDate() : null;

        return currentDate.isAfter(startDate!) && currentDate.isBefore(endDate!);
    }

    selectFirstItem$(r: number, i: number, startMonthDay: number, activeMonthStart: number, activeYearStart: number) {
        const result = this._self.getDayIndexInMonth(r, i, startMonthDay) + 1;
        this._self.dateRange = this.getNewDateRange(result, activeMonthStart, activeYearStart);

        if (this._self.dateRange.start && this.oneWay) {
            this._self.isOpen = this._self.showMonth = false;
            this._self.presetActive = '';
            this.onSelectEnd();
        }
        if (this._self.dateRange.start && this._self.dateRange.end) {
            this._self.isOpen = this._self.showMonth = false;
            this._self.presetActive = '';
            this.onSelectEnd();
        }   
    }

    onSelectEnd() {
        this.$nextTick(() => {
            this.$emit("select-end");
        })
    }
}