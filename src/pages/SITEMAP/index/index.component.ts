import Vue from "vue";
import Component from "vue-class-component";
import VTooltip from 'v-tooltip';

import FooterComponent from "../../../components/footer/footer.component";
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";

const modulesConfig = require("../../../../runner.config.json");

declare var pageData: any;

const $IndexComponent = Vue.extend({
	data() {
		return {
			pageData
		}
	}
})

Vue.use(VTooltip, {
	popover: {
		defaultPopperOptions: {
			modifiers: {
				preventOverflow: { padding: 0 },
				defaultPlacement: 'bottom'
			},
		},
	}
})
@Component({
    template: require("./index.component.html"),
    components: {
        FooterComponent,
        ComponentLoader
	}
})
export default class IndexComponent extends $IndexComponent {
    pageLinks: Object[] = [];
    loggedin:Boolean = false;
    country:String = 'SG';

    headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 420
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 0
		}
	]

    mounted() {
        let pageFlow = modulesConfig.pageFlow;
        let modulesInclude = modulesConfig.loadModules
            .filter((pageFlowName: String) => {
                return pageFlowName != "SITEMAP";
            }).map((pageFlowName: any) => {
                let filenames = pageFlow[pageFlowName]
                    .map((pageName: String) => {
                        let moduleJSON = require(`../../${pageFlowName}/${pageName}/module.json`);
                        return moduleJSON.filename;
                    });
                return {
                    pageFlowName: pageFlowName,
                    filenames: filenames
                }
            });
        this.pageLinks = modulesInclude;
    }

}