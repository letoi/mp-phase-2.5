import Vue from "vue";
import IndexComponent from "./index.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: IndexComponent
    },
    beforeCreate: () => { }
});