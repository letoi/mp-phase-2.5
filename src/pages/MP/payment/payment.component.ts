import Vue from "vue";
import Component from 'vue-class-component';
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";
import FooterComponent from "../../../components/footer/footer.component";
import PaymentAccordionComponent from "../../../components/payment-accordion/payment-accordion.component";
import BookingInfoComponent from "../../../components/booking-info/booking-info.component";
import PaxPacksComponent from "../../../components/pax-packs/pax-packs.component";
import FlightInfoComponent from "../../../components/flight-info/flight-info.component";
import FlightAddonComponent from "../../../components/flight-addon/flight-addon.component";
import LightBoxComponent from "../../../components/light-box/light-box.component";
import TripAddonsComponent from "../../../components/trip-addons/trip-addons.component";
import CostBreakdownComponent from "../../../components/cost-breakdown/cost-breakdown.component";

import PaymentMethodComponent from "../../../components/payment-method/payment-method.component";
import PromoCodeComponent from "../../../components/promo-code/promo-code.component";
import PayMilesComponent from "../../../components/pay-miles/pay-miles.component";
import TabViewComponent from "../../../components/tabview/tab-view.component";
import AddonUpsellComponent from "../../../components/addon-upsell/addon-upsell.component";
import TermsConditionsComponent from "../../../components/terms-conditions/terms-conditions.component";
import axios from "axios";
import "./payment.component.scss";

declare var _: any;
declare var pageData: any;

@Component({
    template: require("./payment.component.html"),
    components: {
        PaymentAccordionComponent,
        LightBoxComponent,
        PromoCodeComponent,
        PayMilesComponent,
        PaymentMethodComponent,
        AddonUpsellComponent,
        TermsConditionsComponent,
        FooterComponent,
        ComponentLoader
    }
})

export default class PaymentComponent extends Vue {

    headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 420
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 0
		}
	]

    data() {
        return {
            pageData
        }
    }

    mounted (){ }


}