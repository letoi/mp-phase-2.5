import Vue from "vue";
import PaymentComponent from "./payment.component";
import "../../../assets/styles/main.scss";

new Vue({
    el: '#app',
    components: {
        AppContent: PaymentComponent
    },
    beforeCreate: () => { }
});