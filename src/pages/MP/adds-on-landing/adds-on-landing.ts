import Vue from "vue";
import AddsOnLandingComponent from "./adds-on-landing.component";
import { StyleLoader } from "../../../mixins/style-loader";
import "../../../assets/styles/main.scss";

StyleLoader.attachLoader();

new Vue({
    el: '#app',
    components: {
        AppContent: AddsOnLandingComponent
    },
    beforeCreate: () => { }
});