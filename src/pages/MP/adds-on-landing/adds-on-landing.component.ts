import { Vue, Component } from 'vue-property-decorator';
import MastheadComponent from '../../../components/masthead/masthead.component';
import FooterComponent from '../../../components/footer/footer.component';
import LightBoxComponent from '../../../components/light-box/light-box.component';
import './adds-on-landing.component.scss';
import DisableAutocomplete from 'vue-disable-autocomplete';
import VTooltip from 'v-tooltip';
import numberWithCommas from '../../../filters/number-with-commas';
import { dateMonthYearFormat, dateCompleteMonthYearFormat } from '../../../filters/date-format';
import UserSummaryPanel from '../../../models/user-data';
import ComponentLoader from '../../../components/component-loader/component-loader.component';
import HeaderDesktopComponent from '../../../components/header/desktop/header.desktop.component';
import HeaderMobileComponent from '../../../components/header/mobile/header.mobile.component';
import AddsOnHotelModal from '../../../components/adds-on-hotel-modal/adds-on-hotel-modal.component';

declare var _: any;
declare var pageData: any;

Vue.use(DisableAutocomplete);
Vue.use(VTooltip, {
	popover: {
		defaultPopperOptions: {
			modifiers: {
				preventOverflow: { padding: 0 },
				defaultPlacement: 'bottom'
			},
		},
	}
});

@Component({
	template: require("./adds-on-landing.component.html"),
	components: {
		MastheadComponent,
		FooterComponent,
		LightBoxComponent,
		ComponentLoader,
		AddsOnHotelModal
	},
	filters: {
		numberWithCommas: numberWithCommas,
		dateFormat: dateMonthYearFormat,
		completeDateFormat: dateCompleteMonthYearFormat
	}
})

export default class AddsOnComponent extends Vue {
	userSummaryPanelData: UserSummaryPanel = new UserSummaryPanel();

	detailSelection: string = '';
	toOpenCollapse: string = '';
	toOpenCollapseContainer: string = '';
	components: any = [];
	headerComponent: any = [];
	asideComponents: any = [];

	// Popover booleans
	loginPopoverShown: boolean = false;

	data() {
		return {
			pageData
		}
	}

	showHotel() {
		this.$root.$emit('hotelModal', '1');
	}

	async created() {
		this.headerComponent = [
			{
				component: HeaderDesktopComponent,
				props: {
					headerdata: pageData.headerData.firstLevelNavigationContainer,
					logoLink: '#main'
				},
				minWidth: 768
			},
			{
				component: HeaderMobileComponent,
				props: {
					headerdata: pageData.headerData.firstLevelNavigationContainer,
					logoLink: '#main'
				},
				minWidth: 0
			}
		]
	}
}