import Vue from "vue";
import Component from "vue-class-component";
import BspComponent from "../../../components/bsp/bsp.component";
import ProgressBarComponent from "../../../components/progress-bar/progress-bar.component";
import FooterComponent from "../../../components/footer/footer.component";
import SeatmapComponent from "../../../components/seatmap/seatmap.component";
import MultiTabComponent from "../../../components/multi-tab/multi-tab.component";
import PassengerTabComponent from "../../../components/passenger-tab/passenger-tab.component";
import LegendComponent from "../../../components/legend/legend.component";
import SeatSelectionData from "../../../models/seat-selection-data";
import SubmitData, { PassengerData } from "../../../models/submitData";
import "./seatselect.base.component.scss";
import UserData from "../../../models/user-data";
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";

declare var _: any;
declare var pageData: any;
const $SeatSelectionBaseComponent = Vue.extend({
    data() {
        return {
            pageData
        }
    }
});

@Component({
    template: require('./seat-select.base.component.html'),
    components: {
        SeatmapComponent,
        FooterComponent,
        MultiTabComponent,
        PassengerTabComponent,
        LegendComponent,
        BspComponent,
        ProgressBarComponent,
        ComponentLoader
    }
})
export default class SeatSelectionBaseComponent extends $SeatSelectionBaseComponent {
    globalFilteredData: SeatSelectionData = new SeatSelectionData();
    jsonToSubmit: SubmitData = new SubmitData;
    filteredData: string = ""

    userData: UserData = new UserData();
    userType: string = 'guest';
    isSticky: boolean = true;
    windowWidth: number = 0;

    headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer,
				userdata: this.userData.userData,
				country: this.userData.country,
				loggedin: this.userData.isLogin
			},
			minWidth: 768
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer,
				userdata: this.userData.userData,
				country: this.userData.country,
				loggedin: this.userData.isLogin
			},
			minWidth: 0
		}
	]

    created() {
        this.$watch(() => { return pageData.seatmapData.seatMapDetails.data.addonDetails }, (addonDetails) => {
            this.globalFilteredData.addonDetails = addonDetails;
        });
        this.$watch(() => { return pageData.seatmapData.seatMapDetails.allFlightSegmentDetails }, (flightSegmentDetails) => {
            this.globalFilteredData.allFlightSegmentDetails = flightSegmentDetails;
        });
        this.userData.isLogin = pageData.userData.userType != 'guest';
        this.userData.userData = pageData.userData;
        this.userType = pageData.userData.userType;
        this.windowWidth = document.documentElement.clientHeight;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && this.windowWidth < 768) {
            this.isSticky = false;
        }
    }

    getFlightStatus(departureCityCode: string, arrivalCityCode: string): string {
        let flightStatus: string = "";
        _.each(pageData.seatmapData.seatMapDetails.allFlightSegmentDetails, (flight: any) => {
            if (flight.arrivalCityCode == departureCityCode && flight.departureCityCode == arrivalCityCode) {
                flightStatus = "Departing";
            } else {
                flightStatus = "Returning";
            }
        });
        return flightStatus;
    }
    

    updatePassenger(data: any) {

        _.each(data.seatMapDetails.data.passenger, (passenger: any, index: number) => {

            let passengerData: PassengerData = new PassengerData();

            passenger.seatTypeWord = this.getSeatType(passenger.seatSelected ? (passenger.seatSelected.seatType ? passenger.seatSelected.seatType : "") : "", data.seatMapDetails.dictionary);
            let seatDetails = this.getSeatDetails(passenger.seatSelected ? (passenger.seatSelected.seatType ? passenger.seatSelected.seatType : "") : "", (passenger.seatPrice ? passenger.seatPrice : []));
            passenger.seatType = passenger.seatSelected ? (passenger.seatSelected.seatType ? passenger.seatSelected.seatType : "") : "";
            passenger.seatSelected == undefined ? passenger.seatSelected = "" : passenger.seatSelected = passenger.seatSelected.seatNumber ? passenger.seatSelected.seatNumber : "";
            passenger.seat = {
                seatNo: passenger.seatSelected,
                isSelected: true,
                selectedAddon: {}
            };
            if(passenger.seatSelected != ""){
                this.globalFilteredData.selectedSeats.push(passenger.seatSelected);
            }
            passenger.index = index;
            passenger.seatPricing = passenger.seatPrice;

            passenger.seatPrice = seatDetails ? seatDetails.amount : "";
            passenger.currency = seatDetails ? seatDetails.currency : "";

            passengerData.id = passenger.passengerID;
            passengerData.oldSeat = passenger.seatSelected.seatNumber;
            passengerData.oldSeatType = passenger.seatType;
            passengerData.oldSeatPrice = passenger.seatPrice;
            passengerData.oldCurrency = passenger.currency;

            this.jsonToSubmit.passenger.push(passengerData);

            this.globalFilteredData.flight.updatedPassengerData.push(passenger);
        });

        this.globalFilteredData.flight.updatedPassengerData[0].selected = true;
    }

    getSeatType(type: string, dictionary: any): string {
        var seat = "Standard"
        if (type) {
            seat = dictionary.seatType[type].toLowerCase() == "preferred" ? "Extra Legroom" : dictionary.seatType[type];
        }
        return seat;
    }

    getSeatDetails(type: string, pricingData: any): any {
        return _.find(pricingData, (pricing: any) => {
            return pricing.type == type;
        })
    }

    changeSelectedFlight() {
        var self = this;
        _.find(pageData.seatmapData.seatMapDetails.allFlightSegmentDetails, (flight: any) => {
            if (flight.selected) {
                self.globalFilteredData.flight.flightInfo = flight;
                self.globalFilteredData.seat = {};
                return;
            }
        });
    }

    onSelectTab(flightSegment: any) {

        let flight = pageData.seatmapData.seatMapDetails.data.flight;
        flight.otherDetails = pageData.seatmapData.seatMapDetails.dictionary.flight[flight.flight];

        this.jsonToSubmit.arrivalAirportCode = flight.otherDetails.destinationAirportCode;
        this.jsonToSubmit.departureAirportCode = flight.otherDetails.originAirportCode;
        this.jsonToSubmit.departureDate = flight.otherDetails.departureDateTime;
        this.jsonToSubmit.flightID = flight.flightID;
        this.jsonToSubmit.flightNo = flight.otherDetails.flightNumber;

        _.each(this.globalFilteredData.flight.updatedPassengerData, (passenger: any, index: number) => {
            this.jsonToSubmit.passenger[index].newCurrency = passenger.currency;
            this.jsonToSubmit.passenger[index].newSeat = passenger.seatSelected;
            this.jsonToSubmit.passenger[index].newSeatPrice = passenger.seatPrice;
            this.jsonToSubmit.passenger[index].newSeatType = passenger.seatType;
            this.jsonToSubmit.passenger[index].seatAddon = passenger.seat.selectedAddon ? passenger.seat.selectedAddon : {};
        })

        this.filteredData = JSON.stringify(this.jsonToSubmit);

        let form: any = this.$refs['submitForm'];
        form.submit();

        // _.find(pageData.seatmapData.seatMapDetails.allFlightSegmentDetails, (item: any) => {
        //     item.flightNumber == flightSegment.flightNumber ? item.selected = true : item.selected = false;
        // });
        // this.changeSelectedFlight();
        // this.$emit('input', pageData.seatmapData.seatMapDetails);
    }

    getTabCount(): number {
        return pageData.seatmapData.seatMapDetails.allFlightSegmentDetails.length;
    }

    mergeFacilities(rowColumns: any, alignment: any): Object {
        for (let grp in alignment) {
            let colgrp = alignment[grp];

            let facilities = _.filter(rowColumns, (o: any) => {
                return o.type == 'facility' && colgrp.includes(o.facility.column);
            });

            // Combine new column info
            if (facilities.length > 1) {
                let col = {
                    type: 'facility',
                    colspan: facilities.length,
                    facility: facilities[0].facility,
                    column: facilities[0].column
                }

                // get index of first facility matched
                let facIndex = _.findIndex(rowColumns, (o: any) => {
                    return o.type == 'facility' && o.facility.column == facilities[0].facility.column && o.facility.type == facilities[0].facility.type;
                });

                // remove old columns
                let removed = _.remove(rowColumns, (o: any) => {
                    return o.type == 'facility' && colgrp.includes(o.facility.column);
                });

                rowColumns.splice(facIndex, 0, col);

            }
        }

        return rowColumns;
    }

    getFacility(row: any, compartment: any, column: any): any {
        let col = null;
        // iterate through the facilities
        for (let index in row.facility) {
            let facility = row.facility[index];

            if (facility.location) {
                // iterate through the alignment object
                for (let align in compartment.alignment) {
                    // Assign current location
                    let cAlign = compartment.alignment[align];

                    // Check if facility has location then check for first location value
                    if (facility.location == align) {
                        if (column.designator == cAlign[0]) {
                            col = {
                                type: 'facility',
                                colspan: cAlign.length,
                                facility: facility,
                                column: column
                            }

                            break;
                        }
                    }
                }
            }

            if (facility.column && column.designator == facility.column) {
                col = {
                    type: 'facility',
                    colspan: 1,
                    facility: facility,
                    column: column
                }
            }
        }

        return col;
    }

    inAlignmentGrp(row: any, alignment: any, designator: String): Boolean {
        let inGrp = false;
        if (row.facility) {
            for (let facility of row.facility) {
                // iterate through the alignment object
                for (let align in alignment) {
                    // Assign current location
                    let cAlign = alignment[align];

                    // Check if facility has location then check for first location value
                    if (facility.location && facility.location == align) {
                        inGrp = cAlign.includes(designator);
                        if (inGrp) break;
                    }
                }

                if (inGrp) break;
            }
        }

        return inGrp;
    }
    
    getQueryStringValue (key:string) {  
        return decodeURIComponent(location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
    }  
}