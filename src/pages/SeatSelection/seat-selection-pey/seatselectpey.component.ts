import Vue from "vue";
import Component from "vue-class-component";
import SeatSelection from "../seat-selection/seatselect.component";
import axios from "axios";
import SeatSelectionBaseComponent from "../seat-selection-base/seat-select.base.component";

declare var _: any;

SeatSelection.prototype.mounted = function() {};

@Component
export default class SeatSelectPeyComponent extends SeatSelectionBaseComponent {
    mounted() {
        let jsonName : string = this.getQueryStringValue("json");
        jsonName = jsonName ? jsonName :  "Airbus_A350_900_Premium_Economy_2A1C1I_NoPack";

        axios.get("assets/JSONS/"+jsonName+".json")
            // axios.get("assets/JSONS/Airbus_A380_800_Business_2A_NoPack.json")
        // axios.get("assets/JSONS/Airbus_A380_800_Economy_2A_NoPack.json")
            // axios.get("assets/JSONS/Airbus_A380_800_First_2A1C1I_NoPack.json")
            // axios.get("assets/JSONS/Boeing_777_300ER_Economy_2A_NoPack.json")
            // axios.get("assets/JSONS/Boeing_777_300ER_First_1A_NoPack.json")
            // axios.get("assets/JSONS/Boeing_777_300ER_Business_2A_NoPack.json")
            .then(response => {
                let data = response.data;
                // console.log(data.seatMapDetails);
                // Alter daata for rendering
                for (let i = 0; i < data.seatMapDetails.data.compartment.length; i++) {
                    let compartment = data.seatMapDetails.data.compartment[i];

                    // Add aisle in compartment data
                    let aisleIndexes = [];
                    for (let i = 0; i < compartment.column.length; i++) {
                        let column = compartment.column[i];
                        let nextColumn = compartment.column[i + 1];

                        if (column && nextColumn && column.alignment !== nextColumn.alignment) {
                            aisleIndexes.push(i + 1);
                        }
                    }

                    for (let i in aisleIndexes) {
                        let curI = aisleIndexes[i];
                        compartment.column.splice(curI + parseInt(i), 0, {
                            "alignment": "A",
                            "characteristics": "",
                            "designator": ""
                        });
                    }

                    // Add altered column data
                    for (let i = 0; i < data.seatMapDetails.data.row.length; i++) {
                        let row = data.seatMapDetails.data.row[i];

                        // Set columns array
                        let rowColumns = [];

                        // Check rows in compartment
                        if (row.number >= compartment.startRow && row.number <= compartment.endRow) {

                            // Loop through the columns to retain correct column sequence
                            for (let column of compartment.column) {
                                // Check if aisle
                                if (column.alignment == 'A') {
                                    let col = {
                                        type: 'aisle',
                                        colspan: 1,
                                        column: column
                                    };

                                    rowColumns.push(col);
                                } else {
                                    let col = null;

                                    // Loop through each seat in row
                                    // Check if has seat
                                    if (row.seat) {
                                        // Check for facilities in rows with seats
                                        if (row.facility) {
                                            col = this.getFacility(row, compartment, column);
                                        }

                                        // Check if column is a seat
                                        for (let seat of row.seat) {
                                            // If current seat matches current column, append to columns
                                            if (seat.column == column.designator) {
                                                col = {
                                                    type: 'seat',
                                                    colspan: 1,
                                                    seat: seat,
                                                    column: column
                                                }
                                            }
                                        }

                                        // Check for empty seats column is a blank seat
                                        if (!col && !this.inAlignmentGrp(row, compartment.alignment, column.designator)) {
                                            col = {
                                                type: 'empty',
                                                colspan: 1,
                                                column: column
                                            }
                                        }
                                    } else {
                                        // Check existing facilities on rows with no seats
                                        col = this.getFacility(row, compartment, column);

                                        // Check for empty seats column is a blank seat
                                        if (!col && !this.inAlignmentGrp(row, compartment.alignment, column.designator)) {
                                            col = {
                                                type: 'empty',
                                                colspan: 1,
                                                column: column
                                            }
                                        }
                                    }

                                    if (!col) continue;

                                    rowColumns.push(col);
                                }
                            }

                            // Merge facility per column group before assigning
                            this.mergeFacilities(rowColumns, compartment.alignment);

                            // Assign column info in current row
                            row.column = rowColumns;
                        }
                    }
                }

                let upperDeck = _.filter(data.seatMapDetails.data.compartment, (o: any) => { return o.location == 'U'; });
                let mainDeck = _.filter(data.seatMapDetails.data.compartment, (o: any) => { return o.location == 'M'; });

                this.globalFilteredData.upperDeckCount = upperDeck.length;
                this.globalFilteredData.mainDeckCount = mainDeck.length;
                this.globalFilteredData.activeDeck = mainDeck.length > 0 && upperDeck.length > 0 ? 'active-main' : (upperDeck.length > 0 && mainDeck.length == 0) ? 'active-upper' : (mainDeck.length > 0 && upperDeck.length == 0) ? 'active-main' : 'active-main';

                this.updatePassenger(data);

                this.pageData.seatmapData = data;

                this.changeSelectedFlight();
            });
    }
}