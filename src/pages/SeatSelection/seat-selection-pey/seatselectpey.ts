import Vue from "vue";
import SeatSelectPeyComponent from "./seatselectpey.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: SeatSelectPeyComponent
    },
    beforeCreate: () => {  }
});