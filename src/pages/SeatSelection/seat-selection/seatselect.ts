import Vue from "vue";
import SeatSelection from "./seatselect.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: SeatSelection
    },
    beforeCreate: () => {  }
});