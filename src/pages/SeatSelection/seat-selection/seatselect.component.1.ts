import Vue from "vue";
import Component from 'vue-class-component';
import BspComponent from "../../../components/bsp/bsp.component";
import ProgressBarComponent from "../../../components/progress-bar/progress-bar.component";
import FooterComponent from "../../../components/footer/footer.component";
import SeatmapComponent from "../../../components/seatmap/seatmap.component";
import MultiTabComponent from "../../../components/multi-tab/multi-tab.component";
import PassengerTabComponent from "../../../components/passenger-tab/passenger-tab.component";
import LegendComponent from "../../../components/legend/legend.component";
import SeatSelectionData from "../../../models/seat-selection-data";
import SubmitData, { PassengerData } from "../../../models/submitData";
import "./seatselect.component.scss";
import "../../../assets/styles/main.scss";
import axios from "axios";
import Sticky from 'vue-sticky-directive';
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";


declare var _: any;
declare var pageData: any;
Vue.use(Sticky);

@Component({
    template: require("./seatselect.component.html"),
    components: {
        SeatmapComponent,
        FooterComponent,
        MultiTabComponent,
        PassengerTabComponent,
        LegendComponent,
        BspComponent,
        ProgressBarComponent,
        ComponentLoader
    }
})
export default class SeatSelection extends Vue {
    data() {
        return {
            pageData
        }
    }

    globalFilteredData: SeatSelectionData = new SeatSelectionData();
    jsonToSubmit: SubmitData = new SubmitData;
    filteredData: string = ""

    headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 420
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 0
		}
	]

    created() {
        this.$watch(() => { return pageData.seatmapData.seatMapDetails.data.addonDetails }, (addonDetails) => {
            this.globalFilteredData.addonDetails = addonDetails;
        });
        this.$watch(() => { return pageData.seatmapData.seatMapDetails.allFlightSegmentDetails }, (flightSegmentDetails) => {
            this.globalFilteredData.allFlightSegmentDetails = flightSegmentDetails;
        });
    }
    mounted() {
        // axios.get("assets/JSONS/seatmapData.json")
        // axios.get("assets/JSONS/Seat_Map_A380_Economy_with_Addons_old.json")
            axios.get("assets/JSONS/Seat_Map_A380_Economy_with_Addons _(08-22-18).json")
            // axios.get("assets/JSONS/Seat_Map_A380_Business.json")
            
            .then(response => {
                let data = response.data;
                // console.log(data.seatMapDetails);
                // Alter daata for rendering
                for (let i = 0; i < data.seatMapDetails.data.compartment.length; i++) {
                    let compartment = data.seatMapDetails.data.compartment[i];

                    // Add aisle in compartment data
                    let aisleIndexes = [];
                    for (let i = 0; i < compartment.column.length; i++) {
                        let column = compartment.column[i];
                        let nextColumn = compartment.column[i + 1];

                        if (column && nextColumn && column.alignment !== nextColumn.alignment) {
                            aisleIndexes.push(i + 1);
                        }
                    }

                    for (let i in aisleIndexes) {
                        let curI = aisleIndexes[i];
                        compartment.column.splice(curI + parseInt(i), 0, {
                            "alignment": "A",
                            "characteristics": "",
                            "designator": ""
                        });
                    }

                    // Add altered column data
                    for (let i = 0; i < data.seatMapDetails.data.row.length; i++) {
                        let row = data.seatMapDetails.data.row[i];

                        // Set columns array
                        let rowColumns = [];

                        // Check rows in compartment
                        if (row.number >= compartment.startRow && row.number <= compartment.endRow) {

                            // Loop through the columns to retain correct column sequence
                            for (let column of compartment.column) {
                                // Check if aisle
                                if (column.alignment == 'A') {
                                    let col = {
                                        type: 'aisle',
                                        colspan: 1,
                                        column: column
                                    };

                                    rowColumns.push(col);
                                } else {
                                    let col = null;

                                    // Loop through each seat in row
                                    // Check if has seat
                                    if (row.seat) {
                                        // Check for facilities in rows with seats
                                        if (row.facility) {
                                            col = this.getFacility(row, compartment, column);
                                        }

                                        // Check if column is a seat
                                        for (let seat of row.seat) {
                                            // If current seat matches current column, append to columns
                                            if (seat.column == column.designator) {
                                                col = {
                                                    type: 'seat',
                                                    colspan: 1,
                                                    seat: seat,
                                                    column: column
                                                }
                                            }
                                        }

                                        // Check for empty seats column is a blank seat
                                        if (!col && !this.inAlignmentGrp(row, compartment.alignment, column.designator)) {
                                            col = {
                                                type: 'empty',
                                                colspan: 1,
                                                column: column
                                            }
                                        }
                                    } else {
                                        // Check existing facilities on rows with no seats
                                        col = this.getFacility(row, compartment, column);

                                        // Check for empty seats column is a blank seat
                                        if (!col && !this.inAlignmentGrp(row, compartment.alignment, column.designator)) {
                                            col = {
                                                type: 'empty',
                                                colspan: 1,
                                                column: column
                                            }
                                        }
                                    }

                                    if (!col) continue;

                                    rowColumns.push(col);
                                }
                            }

                            // Merge facility per column group before assigning
                            this.mergeFacilities(rowColumns, compartment.alignment);

                            // Assign column info in current row
                            row.column = rowColumns;
                        }
                    }
                }

                let upperDeck = _.filter(data.seatMapDetails.data.compartment,  (o: any) => { return o.location == 'U'; });
                let mainDeck = _.filter(data.seatMapDetails.data.compartment,  (o: any) => { return o.location == 'M'; });

                this.globalFilteredData.upperDeckCount = upperDeck.length;
                this.globalFilteredData.mainDeckCount = mainDeck.length;
                this.globalFilteredData.activeDeck = mainDeck.length > 0 && upperDeck.length > 0 ? 'active-main' : (upperDeck.length > 0 && mainDeck.length == 0) ? 'active-upper' : (mainDeck.length > 0 && upperDeck.length == 0) ? 'active-main' : 'active-main';

                this.updatePassenger(data);

                pageData.seatmapData = data;

                this.changeSelectedFlight();
            });
    }

    getFlightStatus(departureCityCode: string, arrivalCityCode: string): string {
        let flightStatus: string = "";
        _.each(pageData.seatmapData.seatMapDetails.allFlightSegmentDetails, (flight: any) => {
            if (flight.arrivalCityCode == departureCityCode && flight.departureCityCode == arrivalCityCode) {
                flightStatus = "Departing";
            } else {
                flightStatus = "Returning";
            }
        });
        return flightStatus;
    }

    updatePassenger(data: any) {
        
        _.each(data.seatMapDetails.data.passenger, (passenger: any, index: number) => {
            let passengerData: PassengerData = new PassengerData();
            passenger.seatSelected == undefined ? passenger.seatSelected = "" : null;
            passenger.seat = {
                seatNo: passenger.seatSelected,
                isSelected: true,
                selectedAddon: {}
            };
            passenger.index = index;
            passenger.seatTypeWord = this.getSeatType(passenger.seatType, data.seatMapDetails.dictionary);
            let seatDetails = this.getSeatDetails(passenger.seatType, passenger.seatPricing);
            passenger.seatPrice = seatDetails ? seatDetails.amount : "";
            passenger.currency = seatDetails ? seatDetails.currency : "";

            passengerData.id = passenger.passengerID;
            passengerData.oldSeat = passenger.seatSelected;
            passengerData.oldSeatType = passenger.seatType;
            passengerData.oldSeatPrice = passenger.seatPrice;
            passengerData.oldCurrency = passenger.currency;

            this.jsonToSubmit.passenger.push(passengerData);

            this.globalFilteredData.flight.updatedPassengerData.push(passenger);
        });

        this.globalFilteredData.flight.updatedPassengerData[0].selected = true;
    }

    getSeatType(type: string, dictionary: any): string {
        var seat = "Standard"
        if (type) {
            seat = dictionary.seatType[type].toLowerCase() == "preferred" ? "Extra Legroom" : dictionary.seatType[type];
        }
        return seat;
    }

    getSeatDetails(type: string, pricingData: any): any {
        return _.find(pricingData, (pricing: any) => {
            return pricing.type == type;
        })
    }

    changeSelectedFlight() {
        var self = this;
        _.find(pageData.seatmapData.seatMapDetails.allFlightSegmentDetails, (flight: any) => {
            if (flight.selected) {
                self.globalFilteredData.flight.flightInfo = flight;
                self.globalFilteredData.seat = {};
                return;
            }
        });
    }

    onSelectTab(flightSegment: any) {
        
        console.log(this.globalFilteredData);
        let flight = pageData.seatmapData.seatMapDetails.data.flight;
        flight.otherDetails = pageData.seatmapData.seatMapDetails.dictionary.flight[flight.flight];

        this.jsonToSubmit.arrivalAirportCode = flight.otherDetails.destinationAirportCode;
        this.jsonToSubmit.departureAirportCode = flight.otherDetails.originAirportCode;
        this.jsonToSubmit.departureDate = flight.otherDetails.departureDateTime;
        this.jsonToSubmit.flightID = flight.flightID;
        this.jsonToSubmit.flightNo = flight.otherDetails.flightNumber;

        _.each(this.globalFilteredData.flight.updatedPassengerData, (passenger: any, index: number) => {
            this.jsonToSubmit.passenger[index].newCurrency = passenger.currency;
            this.jsonToSubmit.passenger[index].newSeat = passenger.seatSelected;
            this.jsonToSubmit.passenger[index].newSeatPrice = passenger.seatPrice;
            this.jsonToSubmit.passenger[index].newSeatType = passenger.seatType;
            this.jsonToSubmit.passenger[index].seatAddon = passenger.seat.selectedAddon ? passenger.seat.selectedAddon : {};
        })

        this.filteredData = JSON.stringify(this.jsonToSubmit);
        console.log(this.filteredData);

        let form: any = this.$refs['submitForm'];
        console.log(form);
        form.submit();

        // _.find(pageData.seatmapData.seatMapDetails.allFlightSegmentDetails, (item: any) => {
        //     item.flightNumber == flightSegment.flightNumber ? item.selected = true : item.selected = false;
        // });
        // this.changeSelectedFlight();
        // this.$emit('input', pageData.seatmapData.seatMapDetails);
    }

    getTabCount(): number {
        return pageData.seatmapData.seatMapDetails.allFlightSegmentDetails.length;
    }

    mergeFacilities(rowColumns: any, alignment: any): Object {
        for (let grp in alignment) {
            let colgrp = alignment[grp];

            let facilities = _.filter(rowColumns, (o: any) => {
                return o.type == 'facility' && colgrp.includes(o.facility.column);
            });

            // Combine new column info
            if (facilities.length > 1) {
                let col = {
                    type: 'facility',
                    colspan: facilities.length,
                    facility: facilities[0].facility,
                    column: facilities[0].column
                }

                // get index of first facility matched
                let facIndex = _.findIndex(rowColumns, (o: any) => {
                    return o.type == 'facility' && o.facility.column == facilities[0].facility.column && o.facility.type == facilities[0].facility.type;
                });

                // remove old columns
                let removed = _.remove(rowColumns, (o: any) => {
                    return o.type == 'facility' && colgrp.includes(o.facility.column);
                });

                rowColumns.splice(facIndex, 0, col);

            }
        }

        return rowColumns;
    }

    getFacility(row: any, compartment: any, column: any): any {
        let col = null;
        // iterate through the facilities
        for (let index in row.facility) {
            let facility = row.facility[index];

            if (facility.location) {
                // iterate through the alignment object
                for (let align in compartment.alignment) {
                    // Assign current location
                    let cAlign = compartment.alignment[align];

                    // Check if facility has location then check for first location value
                    if (facility.location == align) {
                        if (column.designator == cAlign[0]) {
                            col = {
                                type: 'facility',
                                colspan: cAlign.length,
                                facility: facility,
                                column: column
                            }

                            break;
                        }
                    }
                }
            }

            if (facility.column && column.designator == facility.column) {
                col = {
                    type: 'facility',
                    colspan: 1,
                    facility: facility,
                    column: column
                }
            }
        }

        return col;
    }

    inAlignmentGrp(row: any, alignment: any, designator: String): Boolean {
        let inGrp = false;
        if (row.facility) {
            for (let facility of row.facility) {
                // iterate through the alignment object
                for (let align in alignment) {
                    // Assign current location
                    let cAlign = alignment[align];

                    // Check if facility has location then check for first location value
                    if (facility.location && facility.location == align) {
                        inGrp = cAlign.includes(designator);
                        if (inGrp) break;
                    }
                }

                if (inGrp) break;
            }
        }

        return inGrp;
    }
}
