import Component from "vue-class-component";
import PassengerCountDesktopComponent from "../../../../../components/passenger-count-base/desktop/passenger-count/passenger-count.desktop.component";
import './passenger-count.mobile.component.scss';

@Component({
    template: require("./passenger-count.mobile.component.html")
})
export default class PassengerCountMobileComponent extends PassengerCountDesktopComponent {

}