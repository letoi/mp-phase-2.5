import { Vue, Component } from 'vue-property-decorator';
import MastheadComponent from "../../../components/masthead/masthead.component";
import FooterComponent from "../../../components/footer/footer.component";
import LightBoxComponent from "../../../components/light-box/light-box.component";
import "./home.component.scss";
import DisableAutocomplete from 'vue-disable-autocomplete';
import VTooltip from 'v-tooltip';
import numberWithCommas from "../../../filters/number-with-commas";
import { dateMonthYearFormat, dateCompleteMonthYearFormat } from "../../../filters/date-format";
import UserSummaryPanel from "../../../models/user-data";
import HomeWidgetDesktopComponent from "./home-widget/desktop/home-widget.desktop.component";
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HomeWidgetMobileComponent from "./home-widget/mobile/home-widget.mobile.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";
import AsideMobileComponent from "./aside/mobile/aside.mobile.component";
import HomeSectionsComponent from "../home-sections-baseline1/home-sections-baseline1.component";

declare var _: any;
declare var pageData: any;

Vue.use(DisableAutocomplete);
Vue.use(VTooltip, {
	popover: {
		defaultPopperOptions: {
			modifiers: {
				preventOverflow: { padding: 0 },
				defaultPlacement: 'bottom'
			},
		},
	}
});

@Component({
	template: require("./home.component.html"),
	components: {
		MastheadComponent,
		FooterComponent,
		LightBoxComponent,
		ComponentLoader,
		HomeSectionsComponent
	},
	filters: {
		numberWithCommas: numberWithCommas,
		dateFormat: dateMonthYearFormat,
		completeDateFormat: dateCompleteMonthYearFormat
	}
})
export default class HomeComponent extends Vue {
	userSummaryPanelData: UserSummaryPanel = new UserSummaryPanel();

	checkedInPax: number = 0;
	defaultTab: number = 1;
	detailSelection: string = "";
	toOpenCollapse: string = "";
	toOpenCollapseContainer: string = "";
	components: any = [];
	headerComponent: any = [];
	asideComponents: any = [];

	data() {
		return {
			pageData
		}
	}

	async created() {
		if (pageData.pnrList) {
			this.checkedInPax = pageData.pnrList.reduce((prev: any, current: any) => {
				return (prev.isCheckin ? 1 : 0) + (current.isCheckin ? 1 : 0);
			}, 0);
		}

		this.defaultTab = this.checkedInPax > 0 ? 3 : 1;
		
		this.userSummaryPanelData.isLogin = pageData.userData.userType != 'guest';
		this.userSummaryPanelData.userData = pageData.userData;
		this.userSummaryPanelData.userType = pageData.userData.userType;

		this.asideComponents = [
			{
				component: AsideMobileComponent,
				props: {
					userSummaryPanelData: this.userSummaryPanelData,
					checkedInPax: this.checkedInPax,
					defaultTab: this.defaultTab
				},
				maxWidth: 420
			}
		];

		this.components = [
			{
				component: HomeWidgetDesktopComponent,
				props: {
					userSummaryPanelData: this.userSummaryPanelData,
					checkedInPax: this.checkedInPax,
					defaultTab: this.defaultTab
				},
				minWidth: 420
			},
			{
				component: HomeWidgetMobileComponent,
				minWidth: 0
			}
		];

		this.headerComponent = [
			{
				component: HeaderDesktopComponent,
				props: {
					headerdata: pageData.headerData.firstLevelNavigationContainer,
					userdata: this.userSummaryPanelData.userData,
					country: this.userSummaryPanelData.country,
					loggedin: this.userSummaryPanelData.isLogin,
					checkedInPax: this.checkedInPax,
					logoLink: '#main'
				},
				minWidth: 768
			},
			{
				component: HeaderMobileComponent,
				props: {
					headerdata: pageData.headerData.firstLevelNavigationContainer,
					userdata: this.userSummaryPanelData.userData,
					country: this.userSummaryPanelData.country,
					loggedin: this.userSummaryPanelData.isLogin,
					checkedInPax: this.checkedInPax,
					logoLink: '#main'
				},
				minWidth: 0
			}
		]
	}

	get percentageValue() {
		let total = parseFloat(pageData.userData.ppsValue.currentValue) + parseFloat(pageData.userData.ppsValue.requalifyValue);
		total = (parseFloat(pageData.userData.ppsValue.currentValue) / total) * 100;
		return Math.round(total);
	}

	showLogin() {
		this.$root.$emit('loginModal', '1');
	}

	showMoreDetails(toOpen: string) {
		if (this.toOpenCollapse == toOpen) {
			this.toOpenCollapse = "";
			setTimeout(() => {
				this.toOpenCollapseContainer = "";
			}, 500);
		} else {
			this.toOpenCollapse = toOpen;
			this.toOpenCollapseContainer = toOpen;
		}
	}
}
