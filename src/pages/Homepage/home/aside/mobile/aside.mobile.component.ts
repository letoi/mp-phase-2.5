import { Component } from 'vue-property-decorator';
import "../../../../../extenders/element-extender";
import "./aside.mobile.component.scss";
import HomeWidgetBaseComponent from "../../home-widget/home-widget.base.component";
import BookingMobileComponent from '../../booking/mobile/booking.mobile.component';

@Component({
    template: require("./aside.mobile.component.html"),
    components: {
        BookingMobileComponent
    }
})
export default class AsideMobileComponent extends HomeWidgetBaseComponent {
    activeView: string = '';
    toggleContainers: Array<HTMLElement | null> = [];

    mounted() {
        this.toggleContainers = [
            this.$root.$el.querySelector(".header_wrapper"),
            this.$root.$el.querySelector(".contents_wrapper"),
            this.$root.$el.querySelector(".home-baseline1-component")
        ];
        
        this.$root.$on("open:lightbox", (tab: string) => {
            this.changeActiveTab(tab);
        });
    }

    changeActiveTab(tab: string) {
        this.onToggleContainers(!tab);
        this.activeView = tab;
    }

    onToggleContainers(state: boolean) {
        this.toggleContainers.forEach((el: Element | null) => {
            
            if (state) {
                el!.removeAttr("hidden");
            } else {
                el!.attr("hidden", "hidden");
            }
        });
    }
}