import Vue from "vue";
import HomeComponent from "./home.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: HomeComponent
    },
    beforeCreate: () => { }
});