import { Component, Vue } from 'vue-property-decorator';
import AutocompleteComponent from '../../../../components/autocomplete/autocomplete.component';
import axios, {AxiosResponse as Response} from 'axios';
import { filterCIBOrigin, filterCIBDestination, filterORBOrigin } from '../../../../mixins/booking-filters';
import { FlightData } from '../../../../models/airport';
import { Constants } from '../../../../models/constants';
import { mixins } from 'vue-class-component';
import RecentSearch from '../../../../mixins/recent-search';
import { recentSearchDate } from '../../../../filters/date-format';

class AutocompleteJSON {
    origin: Array<any> = [];
    destination: Array<any> = [];
}
@Component({
    components: {
        'v-autocomplete-booking': AutocompleteComponent
    },
    filters: {
        recentSearchDate
    }
})
export default class BookingBaseComponent extends mixins(RecentSearch, Vue) {
    BOOK_FLIGHTS_JSON_URL = '/assets/JSONS/CIBAirportListJSON.json';
    REDEEM_FLIGHTS_JSON_URL = '/assets/JSONS/ORBAirportListJSON.json';
    bookTripCountries: AutocompleteJSON = new AutocompleteJSON();
    redeemTripCountries: AutocompleteJSON = new AutocompleteJSON();
    bookFlightData: FlightData = new FlightData();
    redeemFlightData: FlightData = new FlightData();
    bookFlightRecentSearches: Array<any> = [];
    redeemFlightRecentSearches: Array<any> = [];
    cabins: Array<any> = [];
    flightSelection: string = 'bookFlight';

    mounted() {
        this.bookFlightData.cabinDescription = this.redeemFlightData.cabinDescription = Constants.cabinClass[0];
        this.cabins = Constants.cabinClass;
        axios.get(this.BOOK_FLIGHTS_JSON_URL)
            .then((response: Response) => {
                let data = response.data;
                this.renderToCIB(data);
            });
            this.getLatestRecentSearch();
    }

    loadNewJSON(bookType: string) {
        if (bookType == 'redeem' && !this.redeemTripCountries.origin.length) {
            axios.get(this.REDEEM_FLIGHTS_JSON_URL)
                .then((response: Response) => {
                    let data = response.data;
                    this.renderToORB(data);
                });
        }
    }

    renderToCIB(data: any) {
        this.bookTripCountries.origin = filterCIBOrigin(data.CIBAirportList);
        this.bookTripCountries.destination = filterCIBDestination(data.CIBAirportList);
    }

    renderToORB(data: any) {
        this.redeemTripCountries.origin = filterORBOrigin(data.ORBAirportList);
        this.redeemTripCountries.destination = filterORBOrigin(data.ORBAirportList);
    }

    getLatestRecentSearch() {
        this.bookFlightRecentSearches = this.getFromLocalStorage('bookFlight').search_history.reverse();
        this.redeemFlightRecentSearches = this.getFromLocalStorage('redeemFlight').search_history.reverse();
    }

    formSubmit(type: string, data: FlightData) {
        this.saveToLocalStorage(type, data);
        this.getLatestRecentSearch();
        this.resetForm(type);
    }

    resetForm(type: string) {
        switch(type) {
            case 'bookFlight': 
                this.bookFlightData = new FlightData();
                this.bookFlightData.cabinDescription = Constants.cabinClass[0];
                break;
            case 'redeemFlight': 
                this.redeemFlightData = new FlightData();
                this.redeemFlightData.cabinDescription = Constants.cabinClass[0];
                break;
        }
    }

    selectRecentSearch(type: any, recentSearch: any) {
        switch(type) {
            case 'bookFlight':
                this.bookFlightData = this.parseToFlightData(this.bookTripCountries, recentSearch);
                break;
            case 'redeemFlight':
                this.redeemFlightData = this.parseToFlightData(this.redeemTripCountries, recentSearch);
                break;
        }
    }

    removeRecentSearch$(type: string, createdAt: number, index: number) {
        this.removeRecentSearch(type, createdAt, index);
        this.getLatestRecentSearch();
    }
}