import { Component, Watch } from 'vue-property-decorator';
import BookingBaseComponent from '../booking.base.component';
import { dateParser } from '../../../../../mixins/utilities';
import LightBoxComponent from '../../../../../components/light-box/light-box.component';
import VDatepickerMobileComponent from '../../../../../components/v-datepicker/mobile/v-datepicker.mobile.component';
import "./booking.mobile.component.scss";
import PassengerCountMobileComponent from '../../passenger-count/mobile/passenger-count.mobile.component';
import CabinClassMobileComponent from '../../cabin-class/mobile/cabin-class.mobile.component';

@Component({
    template: require('./booking.mobile.component.html'),
    components: {
        LightBoxComponent,
        'v-datepicker': VDatepickerMobileComponent,
        'v-passengercount': PassengerCountMobileComponent,
        'v-cabinclass': CabinClassMobileComponent
    }
})
export default class BookingMobileComponent extends BookingBaseComponent {
    openLightbox: string = '';
    tabIndex: number = 1;

    showLightBox(id: string, event: any) {
        event.target.blur();
        this.openLightbox = id;
    }

    closeModal() {
        this.openLightbox = '';
    }

    dateParser(date: Date) {
        return date ? dateParser(date) : "";
    }

    getPassengerCabinClassLabel(model: any) {
        let total = 0;
        total = model.passengerCount.adult + model.passengerCount.children + model.passengerCount.infant;
        return total ? `${total} passenger${total > 1 ? 's' : ''} in ${model.cabinDescription.cabinDescription}` : "";
    }

    switchTabs(tabSelected: number) {
        this.tabIndex = tabSelected;
    }

    @Watch('tabIndex')
    onTabIndexChange(value: number) {
        if (value == 2) {
            this.loadNewJSON('redeem');
        }
    }
}