import Vue from "vue";
import Component from "vue-class-component";
import UserSummaryPanel from "../../../../models/user-data";
import { FlightStatusAirportRootData, FlightStatusQuery, FlightStatusAirportData, FlightData } from "../../../../models/airport";
import * as moment from "moment";
import axios from "axios";
import { parseAirportData, filterCIBOrigin, filterCIBDestination } from "../../../../mixins/booking-filters";
import { validate, validateAll, RequiredModel } from "../../../../directive/validation.directive";
import TabViewComponent from "../../../../components/tabview/tab-view.component";
import ManageBookingComponent from "../../../../components/manage-booking/manage-booking.component";
import AutocompleteComponent from "../../../../components/autocomplete/autocomplete.component";
import VDatepickerComponent from "../../../../components/v-datepicker/v-datepicker.component";
import BookingComponent from "../../../../components/booking/booking.component";
import { toDateObject } from "../../../../mixins/utilities";

declare var pageData: any;
declare var _: any;
const $HomeWidgetComponent = Vue.extend({
    props: {
        userSummaryPanelData: UserSummaryPanel,
        checkedInPax: Number,
        defaultTab: Number
    },
    directives: {
		requiredModel: RequiredModel
	},
    data() {
        return {
            pageData,
            bookingReferenceSQMI: "",
			last_familyNameSQMI: "",
			bookingReferenceEticket: "",
			last_familyNameEticket: "",
			flightOrigin: "",
			flightDestination: "",
			departDate: "",
			returnDate: "",
			checkedInSelection: "bookingReference"
        }
    }
});

@Component({
    components: {
        BookingComponent,
        TabViewComponent,
        ManageBookingComponent,
        'v-datepicker': VDatepickerComponent,
		'v-autocomplete-booking': AutocompleteComponent,
		'v-autocomplete': AutocompleteComponent,
    }
})
export default class HomeWidgetBaseComponent extends $HomeWidgetComponent {
    // Flight Status Requirements
	FLIGHT_STATUS_AIRPORT_LIST_JSON: string = "/assets/JSONS/flightStatusairportListJSON.json";
	FLIGHT_STATUS_AIRPORT_LIST_ZH_CN_JSON: string = "/assets/JSONS/flightStatusAirportListJSON_zh_CN.json";
	FLIGHT_STATUS_TYPE_ROUTE: string = "route";
	FLIGHT_STATUS_TYPE_FLIGHT_NUMBER: string = "flightNumber";

	flightStatusAirportListRootData: FlightStatusAirportRootData = new FlightStatusAirportRootData();
	isFlightStatusDeparting: boolean = true;
	isFlightStatusArriving: boolean = false;
	flightStatusType: string = this.FLIGHT_STATUS_TYPE_ROUTE;

	flightStatusQueryData: FlightStatusQuery = new FlightStatusQuery();
	originCountriesBook: FlightStatusAirportData[] = [];
	destinationCountriesBook: FlightStatusAirportData[] = [];
	bookFlightJSON: any;
	flightPossibleDates: any[] = []
	flightStatusArriveDate: any = {
		label: "Arrive Date",
		value: moment().toDate()
	};
	flightStatusDepartDate: any = {
		label: "Depart Date",
		value: moment().toDate()
	};
	checkInBookingRadio: string = "sq";
	showManualBooking: boolean = false;
	bookFlightData: FlightData = new FlightData();
	bookFlightJSONURL: string = "/assets/JSONS/CIBAirportListJSON.json";
    flightNumber: string = "";
    
    get flightStatusReadableArriveDate(): string {
		return this.getReadableDate(this.flightStatusArriveDate.value);
	}

	get flightStatusReadableDepartDate(): string {
		return this.getReadableDate(this.flightStatusDepartDate.value);
    }

    getReadableDate(dateToread: Date): string {
		return dateToread != null ? moment(dateToread).format("ddd - D MMM YYYY") : "";
	}
    
    mounted() {
		let dateNow = moment().toDate();
		this.flightStatusArriveDate.value = dateNow;
		this.flightStatusArriveDate.value = dateNow;

		this.flightPossibleDates = [
			moment(dateNow).subtract(2, 'days').toDate(),
			moment(dateNow).subtract(1, 'days').toDate(),
			dateNow,
			moment(dateNow).add(1, 'days'),
			moment(dateNow).add(2, 'days')
		];

		this.onRequest(this.bookFlightJSONURL, "CIBAirportList")
		.then((response: any) => {
			this.bookFlightJSON = response;
			this.renderToFrom(response, "CIBAirportList");
			this.renderCIB(response);
		});

		axios.get("assets/JSONS/fareDealsHomePage.json")
			.then(response => {
				let data = response.data;
				pageData.fareDealsData = data;

				this.userSummaryPanelData.isLogin = pageData.userData.userType != 'guest';
				this.userSummaryPanelData.userData = pageData.userData;
			});
		// Flight Status Requirements
		axios.get(this.FLIGHT_STATUS_AIRPORT_LIST_JSON)
			.then(response => {
				this.flightStatusAirportListRootData = response.data as FlightStatusAirportRootData;
				this.bookFlightJSON = this.flightStatusAirportListRootData;
				this.renderToFrom(this.flightStatusAirportListRootData, "airportList");
				this.renderCIB(this.flightStatusAirportListRootData);
			});
	}

	async onRequest(url: string, key: string) {
        let response = await axios.get(url);
        return response.data;
    }

    toDateObject(dateString: string) {
		let date = new Date(dateString);
		return toDateObject(date);
	}

	// Flight Status Requirements
	resetBasicFlightStatusQueryData() {
		if (this.flightStatusQueryData) {
			this.flightStatusQueryData.schedule = {};
			this.flightStatusQueryData.from = new FlightStatusAirportData();
			this.flightStatusQueryData.to = new FlightStatusAirportData();
		}
	}

	onFlightStatusDeparting($clickEvent: Event) {
		this.isFlightStatusDeparting = true;
		this.isFlightStatusArriving = false;
		$clickEvent.preventDefault();
	}

	onFlightStatusTypeChanged() {
		this.resetBasicFlightStatusQueryData();
	}

	onFlightStatusArriving($clickEvent: Event) {
		this.isFlightStatusArriving = true;
		this.isFlightStatusDeparting = false;

		$clickEvent.preventDefault();
	}

	renderToFrom(response: any, key: string) {
		let findDefaultSelected = _.find(response[key], { airportCode: response.defaultSelectedAirport });
		this.flightStatusQueryData.from = parseAirportData(findDefaultSelected);
	}

	renderCIB(response: any) {
		this.originCountriesBook = filterCIBOrigin(response.airportList);
		this.destinationCountriesBook = filterCIBDestination(response.airportList);
	}

	onValidationBlur(event: any) {
		validate(event.target);
	}

	checkForm(event: any) {
		if (validateAll(event)) {
			event.target.submit();
			return true;
		} else {
			return false;	
		}
	}
}