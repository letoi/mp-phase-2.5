import { Vue, Component } from 'vue-property-decorator';
import PassengerCountMobileComponent from "../../passenger-count/mobile/passenger-count.mobile.component";
import "./home-widget.mobile.component.scss";

@Component({
    template: require("./home-widget.mobile.component.html"),
    components: {
        'passenger-count': PassengerCountMobileComponent
    }
})
export default class HomeWidgetMobileComponent extends Vue {
    openLightbox(id: string) {
        this.$root.$emit("open:lightbox", id);
    }
}