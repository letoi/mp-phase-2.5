import Vue from "vue";
import Component from "vue-class-component";
import HomeWidgetBaseComponent from "../home-widget.base.component";
import "../../../../../extenders/element-extender";

@Component({
    template: require("./home-widget.desktop.component.html")
})
export default class HomeWidgetDesktopComponent extends HomeWidgetBaseComponent {
    mounted() {
        this.$nextTick(() => {
            this.$root.$el.querySelector(".header_wrapper")!.removeAttr("hidden");
            this.$root.$el.querySelector(".contents_wrapper")!.removeAttr("hidden");
            this.$root.$el.querySelector(".home-baseline1-component")!.removeAttr("hidden");
        });
    }
}