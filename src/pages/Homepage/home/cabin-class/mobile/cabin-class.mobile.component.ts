import { Component, Vue, Prop } from 'vue-property-decorator';
import './cabin-class.mobile.component.scss';

@Component({
    template: require('./cabin-class.mobile.component.html')
})
export default class CabinClassMobileComponent extends Vue {
    @Prop({default: () => []}) cabins!: Array<any>;
    @Prop(Object) value!: object;
    selected: object = {};

    mounted() {
        this.selected = this.value;
    }

    selectedCabin(cabin: any) {
        this.selected = cabin;
        this.$emit('input', this.selected);
    }
}