import Vue from 'vue';
import HomeSectionComponent from './home-sections-baseline1.component';

new Vue({
  el: '#app',
  components: {
    AppContent: HomeSectionComponent
  }
});
