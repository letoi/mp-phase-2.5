import Vue from 'vue';
import Component from 'vue-class-component';
import axios from 'axios';
import FooterComponent from "../../../components/footer/footer.component";
import FareDealBaseline1Component from '../../../components/fare-deal-baseline1/fare-deal-baseline1.component';
import TripCardComponent from '../../../components/trip-card/trip-card.component';
import PromotionsComponent from '../../../components/promotions/promotions.component';
import SignUpComponent from '../../../components/sign-up/sign-up.component';
import PopularLinksComponent from '../../../components/popular-links/popular-links.component';
import NewsBaseline1Component from '../../../components/news-baseline1/news-baseline1.component';
import ExperienceBaseline1Component from '../../../components/experience-baseline1/experience-baseline1.component';
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";

import './home-sections-baseline1.component.scss';
import '../../../assets/styles/main.scss';

declare var pageData: any;

@Component({
	template: require('./home-sections-baseline1.component.html'),
	components: {
		FareDealBaseline1Component,
		TripCardComponent,
		PromotionsComponent,
		SignUpComponent,
		PopularLinksComponent,
		NewsBaseline1Component,
		FooterComponent,
		ExperienceBaseline1Component,
		ComponentLoader
	}
})

export default class HomeSectionsComponent extends Vue {

	headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 420
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 0
		}
	]

	data() {
		return {
			pageData
		};
	}

	async created() {
		const response = await axios.get('assets/JSONS/fareDealsHomePage.json');

		pageData.fareDealsData = response.data.promos;
	}
}
