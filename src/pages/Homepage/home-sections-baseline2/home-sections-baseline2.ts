import Vue from 'vue';
import HomeSectionBaseline2Component from './home-sections-baseline2.component';

new Vue({
  el: '#app',
  components: {
    AppContent: HomeSectionBaseline2Component
  }
});
