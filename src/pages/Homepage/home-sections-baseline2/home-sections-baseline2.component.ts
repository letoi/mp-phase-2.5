import Vue from 'vue';
import Component from 'vue-class-component';
import axios from 'axios';
import FooterComponent from "../../../components/footer/footer.component";
import FareDealBaseline2Component from '../../../components/fare-deal-baseline2/fare-deal-baseline2.component';
import TripCardComponent from '../../../components/trip-card/trip-card.component';
import PromotionsComponent from '../../../components/promotions/promotions.component';
import SignUpComponent from '../../../components/sign-up/sign-up.component';
import PopularLinksComponent from '../../../components/popular-links/popular-links.component';
import NewsBaseline2Component from '../../../components/news-baseline2/news-baseline2.component';
import ExperienceBaseline2Component from '../../../components/experience-baseline2/experience-baseline2.component';
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";

import './home-sections-baseline2.component.scss';
import '../../../assets/styles/main.scss';

declare var pageData: any;

@Component({
	template: require('./home-sections-baseline2.component.html'),
	components: {
		FareDealBaseline2Component,
		TripCardComponent,
		PromotionsComponent,
		SignUpComponent,
		PopularLinksComponent,
		NewsBaseline2Component,
		FooterComponent,
		ExperienceBaseline2Component,
		ComponentLoader
	}
})

export default class HomeSectionsComponent extends Vue {

	headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 420
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer
			},
			minWidth: 0
		}
	]

	data() {
		return {
			pageData
		};
	}

	async created() {
		const response = await axios.get('assets/JSONS/fareDealsHomePage.json');

		pageData.fareDealsData = response.data.promos;
	}
}
