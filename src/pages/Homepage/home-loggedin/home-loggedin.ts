import Vue from "vue";
import HomeComponentLoggedIn from "./home-loggedin.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: HomeComponentLoggedIn
    },
    beforeCreate: () => { }
});