import Vue from "vue";
import Component from 'vue-class-component';
import DisableAutocomplete from 'vue-disable-autocomplete';
import VTooltip from 'v-tooltip';
import HomeComponent from "../home/home.component";

declare var _: any;
declare var pageData: any;

Vue.use(DisableAutocomplete);
Vue.use(VTooltip, {
	popover: {
		defaultPopperOptions: {
			modifiers: {
				preventOverflow: { padding: 0 },
				defaultPlacement: 'bottom'
			},
		},
	}
});

@Component
export default class HomeComponentLoggedIn extends HomeComponent {
}
