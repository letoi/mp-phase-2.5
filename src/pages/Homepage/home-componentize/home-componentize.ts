import Vue from "vue";
import HomeComponentizeComponent from "./home-componentize.component";

new Vue({
    el: '#app',
    components: {
        AppContent: HomeComponentizeComponent
    },
    beforeCreate: () => { }
});
