import Vue from "vue";
import Component from 'vue-class-component';
import "../home/home.component.scss";
import ComponentLoader from "../../../components/component-loader/component-loader.component";

@Component({
    template: `<div class="component">
        <h1>Hello Desktop</h1>
    </div>`
})
class DesktopComponent extends Vue { }

@Component({
    template: `<div class="component">
        <h1>Hello Mobile {{ data }}</h1>
    </div>`,
    props: {
        data: String
    }
})
class MobileComponent extends Vue { }

declare var _: any;
declare var pageData: any;

@Component({
	template: require("./home-componentize.component.html"),
	components: {
		ComponentLoader
	}
})
export default class HomeComponentizeComponent extends Vue {
	components: any = [
        {
            component: DesktopComponent,
            minWidth: 425
        },
        {
            component: MobileComponent,
            props: {
                data: "This is a sample data"
            },
            minWidth: 0
        }
    ];
}
