import Vue from "vue";
import Component from 'vue-class-component';
import BookingComponent from "../../../components/booking/booking.component";
import FooterComponent from "../../../components/footer/footer.component";
import LightBoxComponent from "../../../components/light-box/light-box.component";
import VDatepickerComponent from '../../../components/v-datepicker/v-datepicker.component';
import "../../../assets/styles/main.scss";
import "./hangar.component.scss";
import axios from "axios";

declare var _: any;
declare var pageData: any;

@Component({
    template: require("./hangar.component.html"),
    components: {
        BookingComponent,
        FooterComponent,
        LightBoxComponent,
        'v-datepicker': VDatepickerComponent
    }
})
export default class HangarComponent extends Vue {
    data() {
        return {
            pageData
        }
    }

    showEmergencyModal: string = "";
    mounted (){  }

    async created (){
        await axios.get("assets/JSONS/fareDealsHomePage.json")
        .then(response => {
            let data = response.data;
            console.log(data.promos);
            
            pageData.fareDealsData = data;
            console.log('fareDealsData:', pageData.fareDealsData);
        });
    }

    openModal(){
        this.showEmergencyModal = "6";
    }

    closeModal(){
        this.showEmergencyModal = "";
    }
}