import Vue from "vue";
import HangarComponent from "./hangar.component";


new Vue({
    el: '#app',
    components: {
        AppContent: HangarComponent
    },
    beforeCreate: () => { }
});