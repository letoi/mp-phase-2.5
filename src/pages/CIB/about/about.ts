import Vue from "vue";
import AboutComponent from "./about.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: AboutComponent
    },
    beforeCreate: () => { }
});