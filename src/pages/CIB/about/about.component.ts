import Vue from "vue";
import Component from "vue-class-component";

@Component({
    template: require("./about.component.html")
})
export default class AboutComponent extends Vue {

}