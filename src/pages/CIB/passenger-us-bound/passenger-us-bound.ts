import Vue from "vue";
import PassengerUsBoundComponent from "./passenger-us-bound.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: PassengerUsBoundComponent
    },
    beforeCreate: () => { }
});