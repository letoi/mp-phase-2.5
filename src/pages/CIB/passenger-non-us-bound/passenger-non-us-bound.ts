import Vue from "vue";
import PassengerNonUsBoundComponent from "./passenger-non-us-bound.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: PassengerNonUsBoundComponent
    },
    beforeCreate: () => { }
});