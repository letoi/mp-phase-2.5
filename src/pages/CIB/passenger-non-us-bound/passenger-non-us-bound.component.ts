import Vue from "vue";
import Component from "vue-class-component";
import HeaderComponent from "../../../components/header/header.component";
import ProgressBarComponent from "../../../components/progress-bar/progress-bar.component";
import BspComponent from "../../../components/bsp/bsp.component"
import InlineKfLoginComponent from "../../../components/CIB/inline-kf-login/inline-kf-login.component";
import StickyPaxTabsComponent from "../../../components/CIB/sticky-pax-tabs/sticky-pax-tabs.component";
import FooterComponent from "../../../components/footer/footer.component";
import "./passenger-non-us-bound.component.scss";
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";
import UserData from "../../../models/user-data";
import ApisInformationComponent from "../../../components/CIB/apis-information/apis-information.component";
import axios from "axios";
import VTooltip from 'v-tooltip';
import AutocompleteComponent from "../../../components/autocomplete/autocomplete.component";


declare var _: any;
declare var pageData: any;

Vue.use(VTooltip, {
	popover: {
		defaultPopperOptions: {
			modifiers: {
				preventOverflow: { padding: 0 },
				defaultPlacement: 'bottom'
			},
		},
	}
});

@Component({
    template: require("./passenger-non-us-bound.component.html"),
    components: {
		HeaderComponent,
		ProgressBarComponent,
		BspComponent,
		InlineKfLoginComponent,
		StickyPaxTabsComponent,
		FooterComponent,
		ComponentLoader,
		HeaderDesktopComponent,
		ApisInformationComponent,
		'v-autocomplete': AutocompleteComponent
    }
})
export default class PassengerNonUsBoundComponent extends Vue {

	jsonData : any = {};
	userData: UserData = new UserData();
	userType: string = 'guest';
	personTitles: any = {};
	royalty: any = {};
	countryList : any = {};
	stateList : any = {};
	paxDetails: any = {};

	data() {
		return {
			pageData
		};
	}

	headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer,
				userdata: this.userData.userData,
				country: this.userData.country,
				loggedin: this.userData.isLogin
			},
			minWidth: 768
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer,
				userdata: this.userData.userData,
				country: this.userData.country,
				loggedin: this.userData.isLogin
			},
			minWidth: 0
		}
	]

	created(){
		this.userData.isLogin = pageData.userData.userType != 'guest';
        this.userData.userData = pageData.userData;
        this.userType = pageData.userData.userType;
	}

	mounted(){

		axios.all([axios.get("assets/JSONS/PAX-Multipax.json"),axios.get("assets/JSONS/TitleJson.json"),axios.get("assets/JSONS/royaltyResponse.json"),axios.get("assets/JSONS/countryResponse.json"),axios.get("assets/JSONS/stateResponse.json")])
		.then(([passenger, title, royalty, countryList, state])=>{
			this.jsonData = passenger.data;
			this.jsonData.activePassengerIndex = "0";
			this.personTitles = title.data;
			this.royalty = royalty.data;
			this.countryList = countryList.data;
			this.stateList = state.data;
			this.paxDetails = passenger.data.passengers;
			//console.log(this.personTitles);
			// console.log(this.jsonData);
			// console.log(this.paxDetails);
			// console.log(this.royalty);
			// console.log(this.countryList);
			// console.log(this.stateList);
		})
	}

	getDatePart(partIndex: number, date: String){
		let part: string = date.split(" ")[partIndex];
		if(partIndex == 1){
			part = part.substring(0, part.length - 1);
		}else if(partIndex == 0){
			part = this.getMonthFromString(part).toString();
		}
		
		return part;
	}

	getMonthFromString(mon:string){

		var d = Date.parse(mon + "1, 2012");
		if(!isNaN(d)){
		   let number = new Date(d).getMonth() + 1;
		   return number >= 10? number : "0"+number;
 		}
		return -1;
	  }
	 

}