import Vue from "vue";
import PaxStudentPromoComponent from "./pax-student-promo.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: PaxStudentPromoComponent
    },
    beforeCreate: () => { }
});