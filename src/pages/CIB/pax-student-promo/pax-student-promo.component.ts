import Vue from "vue";
import Component from "vue-class-component";
import HeaderComponent from "../../../components/header/header.component";
import ProgressBarComponent from "../../../components/progress-bar/progress-bar.component";
import BspComponent from "../../../components/bsp/bsp.component"
import InlineKfLoginComponent from "../../../components/CIB/inline-kf-login/inline-kf-login.component";
import StickyPaxTabsComponent from "../../../components/CIB/sticky-pax-tabs/sticky-pax-tabs.component";
import ApisInformationComponent from "../../../components/CIB/apis-information/apis-information.component";
import FooterComponent from "../../../components/footer/footer.component";
import "./pax-student-promo.component.scss";


declare var _: any;
declare var pageData: any;

@Component({
    template: require("./pax-student-promo.component.html"),
    components: {
		HeaderComponent,
		ProgressBarComponent,
		BspComponent,
		InlineKfLoginComponent,
		StickyPaxTabsComponent,
		ApisInformationComponent,
		FooterComponent,
		
    }
})
export default class PaxStudentPromoComponent extends Vue {
	data() {
		return {
			pageData
		};
	}
}