import Vue from "vue";
import FareCalendar from "./fare-calendar.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: FareCalendar
    },
    beforeCreate: () => { }
});