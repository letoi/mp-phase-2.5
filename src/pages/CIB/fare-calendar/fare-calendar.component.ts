import Vue from "vue";
import Component from "vue-class-component";
import ComponentLoader from "../../../components/component-loader/component-loader.component";
import HeaderDesktopComponent from "../../../components/header/desktop/header.desktop.component";
import HeaderMobileComponent from "../../../components/header/mobile/header.mobile.component";
import FooterComponent from "../../../components/footer/footer.component";
import ProgressBarComponent from "../../../components/progress-bar/progress-bar.component";
import "./fare-calendar.component.scss";
import VTooltip from 'v-tooltip';
import Axios from "axios";
import * as moment from "moment";
import UserData from "../../../models/user-data";

declare var _: any;
declare var pageData: any;

Vue.use(VTooltip, {
	popover: {
		defaultPopperOptions: {
			modifiers: {
				preventOverflow: { padding: 0 },
				defaultPlacement: 'bottom'
			},
		},
	}
});

@Component({
	template: require("./fare-calendar.component.html"),
	components: {
		FooterComponent,
		ProgressBarComponent,
		ComponentLoader
	}
})
export default class FareCalendar extends Vue {
	data() {
		return {
			pageData
		};
	}

	userData: UserData = new UserData();
    userType: string = 'guest';

	calendar = Object;
	fareDetails = Object;
	selectedReturnDate = "";
	calendarReturnDate = new Array();
	buttonNextActive = false;

	headerComponent: any = [
		{
			component: HeaderDesktopComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer,
				userdata: this.userData.userData,
				country: this.userData.country,
				loggedin: this.userData.isLogin
			},
			minWidth: 420
		},
		{
			component: HeaderMobileComponent,
			props: {
				headerdata: pageData.headerData.firstLevelNavigationContainer,
				userdata: this.userData.userData,
				country: this.userData.country,
				loggedin: this.userData.isLogin
			},
			minWidth: 0
		}
	]

	mounted() {
		Axios.get("assets/JSONS/fareCalendar.json").then(response => {
			let fareCalendar = response.data.response.fareCalendar;
			let returnDate = new Array();

			this.calendar = fareCalendar.map(function(fare: any) {
				fare.departureDate = moment(fare.departureDate).format(
					"DD MMM (ddd)"
				);
				fare.isActive = false;

				for (let index = 0; index < fare.return.length; index++) {
					returnDate.push(moment(fare.return[index].returnDate).format("DD MMM (ddd)"));

					if (fare.return[index] !== undefined) {
						fare.return[index].returnDate = moment(
							fare.return[index].returnDate
						).format("DD MMM (ddd)");

						fare.return[index]["isActive"] = false;
					}
				}

				return fare;
			});

			this.fareDetails = response.data.response;
			this.calendarReturnDate = _.uniq(returnDate);
		});
	}

	created() {
		this.userData.isLogin = pageData.userData.userType != 'guest';
        this.userData.userData = pageData.userData;
		this.userType = pageData.userData.userType;
    }

	calendarOnClick(returnDate: any, calendarDate: any) {
		_.each(this.calendar, (calendar: any) => {
			calendar.isActive = false;
			_.each(calendar.return, (ret: any) => {
				ret.isActive = false;
			});
		});

		returnDate.isActive = !returnDate.isActive;
		calendarDate.isActive = true;
		this.selectedReturnDate = returnDate.returnDate;
		this.buttonNextActive = true;
	}

	checkReturnDate(retDate: any) {
		if (retDate === this.selectedReturnDate) {
			return "active";
		}

		return "";
	}
}
