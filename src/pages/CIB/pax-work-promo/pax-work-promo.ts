import Vue from "vue";
import PaxWorkPromoComponent from "./pax-work-promo.component";
import { StyleLoader } from "../../../mixins/style-loader";

StyleLoader.attachLoader();
new Vue({
    el: '#app',
    components: {
        AppContent: PaxWorkPromoComponent
    },
    beforeCreate: () => { }
});