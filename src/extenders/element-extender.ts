interface Element {
	hasClass(cls: string): boolean;
	addClass(cls: string): void;
	removeClass(cls: string): void;
	removeElement(): Element;
	attr(attr: string, val?: string): void | string;
	removeAttr(attr: string): void;
	unwrap(): void;
	insertAfter(ele: Element | string, isVueElement?: boolean): void;
	append(ele: Element | string, isVueElement?: boolean): void;
	blur(): void;
	focus(): void;
}
