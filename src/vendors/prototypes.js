Element.prototype.hasClass = function (cls) {
    return !!this.className.match(new RegExp('(\\s|^)' + cls + '(\\s|$)'));
};

Element.prototype.addClass = function (cls) {
    if (!this.hasClass(cls)) this.className = (this.className + " " + cls).trim();
};

Element.prototype.removeClass = function (cls) {
    if (this.hasClass(cls)) {
        var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
        this.className = this.className.replace(reg, ' ').trim();
    }
};

Element.prototype.removeElement = function () {
    this.parentNode.removeChild(this);
};

Element.prototype.attr = function (attr, val) {
    if (val) {
        this.setAttribute(attr, val);
    } else {
        return this.getAttribute(attr);
    }
};

Element.prototype.removeAttr = function(attr) {
    this.removeAttribute(attr);
};

Element.prototype.insertAfter = function (ele, isVueElement) {
    var t = document.createElement('template');
    t.innerHTML = ele;
    var content = (isVueElement) ? ele : t.content.cloneNode(true);
    this.parentNode.insertBefore(content, this.nextSibling);
};

Element.prototype.append = function (ele, isVueElement) {
    var t = document.createElement('template');
    t.innerHTML = ele;
    var content = (isVueElement) ? ele : t.content.cloneNode(true);
    this.appendChild(content);
};

Element.prototype.unwrap = function() {
    var parent = this.parentNode;
    while(this.firstChild) {
        parent.insertBefore(this.firstChild, this);
    }
    this.removeElement();
};