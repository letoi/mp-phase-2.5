const path = require('path')
const webpack = require('webpack')
const HTMLLoader = require('html-webpack-plugin')
const pageLoader = require('./page.config')
const loadModulesRunner = require('./runner.config.json');
const vendorModulesRunner = require('./runner.config.json').vendors;
const loadModulesBuild = require('./build.config.json');
const vendorModulesBuild = require('./build.config.json').vendors;
const SassPlugin = require('sass-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const getAllHTMLFiles = (files, mode) => {
  return files.map((file) => {
    return new HTMLLoader({
      template: `${file.layout}`,
      filename: `${file.filename}`,
      chunks: [file.moduleName],
      templateParameters: {
        embeddedJson: file.embeddedJson,
        pageTitle: file.pageTitle || "Singapore Airlines",
        mode: mode
      }
    });
  });
};

const htmls = pageLoader.getModuleHTML(loadModulesRunner);
const scripts = pageLoader.getModuleScript(loadModulesRunner);

const config = {
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    filename: 'script/[name].js'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'scss': 'vue-style-loader!css-loader!sass-loader',
            'sass': 'vue-style-loader!css-loader!sass-loader?indentedSyntax',
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
        }
      },
      {
        test: /\.(png|jpg|gif|svg|woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
      {
        test: /\.(html)$/,
        use: {
          loader: 'html-loader',
          options: {
            attrs: [':data-src']
          }
        }
      },
      {
        test: /\.css$/,
        use: ['css-loader']
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader"
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: false
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: false
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json', '.css'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    host:'localhost',
    port: 8080
  },
  performance: {
    hints: false
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new CopyWebpackPlugin([
      { from: "./src/assets/images/", to: "assets/images/" },
      { from: "./src/assets/JSONS/**/*", to: "assets/JSONS/", flatten: true },
      { from: "./src/assets/fonts/*", to: "assets/fonts/", flatten: true }
    ])
  ],
  devtool: '#eval-source-map'
}

module.exports = env => {
  // config.plugins.push(...getAllHTMLFiles(htmls, env.NODE_ENV));
  if (env.NODE_ENV === 'development') {
    let runnerHTML = pageLoader.getModuleHTML(loadModulesRunner);
    let runnerScripts = pageLoader.getModuleScript(loadModulesRunner);
    config.mode = 'development',
    config.entry = {
      ...runnerScripts,
      vendor: vendorModulesRunner
    };
    config.plugins.push(...getAllHTMLFiles(runnerHTML, env.NODE_ENV));
  } else if (env.NODE_ENV === 'production') {
    let buildHTML = pageLoader.getModuleHTML(loadModulesBuild);
    let buildScripts = pageLoader.getModuleScript(loadModulesBuild);
    config.entry = {
      ...buildScripts,
      vendor: vendorModulesBuild
    };
    config.plugins.push(...getAllHTMLFiles(buildHTML, env.NODE_ENV));
    config.devtool = '#source-map'
    config.optimization = {
      minimizer: [
        new UglifyJsPlugin({
          sourceMap: true
        })
      ]
    }
    // http://vue-loader.vuejs.org/en/workflow/production.html
    config.mode = "production";
    config.plugins.push(
      new SassPlugin({"./src/assets/styles/main.scss": "assets/style/bundle.min.css"}, env.NODE_ENV, {
        sourceMap: true,
        sass: { outputStyle: 'compressed' }
      })
    );
    config.plugins.push(
      new UglifyJsPlugin({
        sourceMap: true
      })
    );
  }

  return config;
}