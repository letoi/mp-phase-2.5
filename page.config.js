let defaultLayout = "./src/layouts/default.ejs";
let pageUrl = "./src/pages/"

function concatenateAllModules(config) {
    var pages = config.pageFlow;
    var moduleName = [];
    config.loadModules.forEach((pageFlow) => {
        var pageLinks = pages[pageFlow].map((page) => {
            return pageUrl + pageFlow + "/" + page;
        });
        moduleName.push(...pageLinks);
    });
    return moduleName;
}

module.exports = {
    getModuleScript: (config) => {
        var entryModule = {};
        var moduleName = concatenateAllModules(config);
        moduleName.forEach((url) => {
            var moduleLoad = require(url + "/module.json");
            if (moduleLoad.scripts) {
                entryModule[moduleLoad.moduleName] = moduleLoad.scripts;
            }
        });
        return entryModule;
    },
    getModuleHTML: (config) => {
        var moduleName = concatenateAllModules(config);
        return moduleName.map((url) => {
                var moduleLoad = require(url + "/module.json");
                if (!moduleLoad.scripts) {
                    return 0;
                }

                return {
                    filename: moduleLoad.filename,
                    layout: moduleLoad.layout || defaultLayout,
                    moduleName: moduleLoad.moduleName,
                    pageTitle: moduleLoad.pageTitle,
                    embeddedJson: moduleLoad.embeddedJson
                };
            }).filter((modules) => {
                if (modules) {
                    return modules;
                }
            });
    }
};